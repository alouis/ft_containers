# Ft_containers
Some containers of the STL as described [here](http://www.cplusplus.com/reference/stl/) and [there](https://en.cppreference.com/w/cpp/container)

## Containers
 * [Vector](http://www.cplusplus.com/reference/vector/vector/)
 * [Stack](http://www.cplusplus.com/reference/stack/stack/)
 * [Map](http://www.cplusplus.com/reference/map/)

## Ressources
 ### General overview
 * [C++ Core Guidelines](https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#Ri-explicit)
 * [The C++ Standard Template Library (STL)](https://www.geeksforgeeks.org/the-c-standard-template-library-stl/)
 * [Liste des algorithmes](https://r0d.developpez.com/articles/algos-stl-fr/#LIII)

 ### Containers
 * [How to write an STL compatible container](https://medium.com/@vgasparyan1995/how-to-write-an-stl-compatible-container-fc5b994462c6#26af)
 * [How default arguments work](https://www.programiz.com/cpp-programming/default-argument)
 * [Lots of drawing](https://www.programmersought.com/article/73958385388/)
 * [Binary trees explained](https://www.codesdope.com/course/data-structures-red-black-trees/)

 ### Templates
 * [Templates](https://isocpp.org/wiki/faq/templates)
 * [Class template](https://en.cppreference.com/w/cpp/language/class_template)
 * [Template specialization](https://www.geeksforgeeks.org/template-specialization-c/)
 * [Template specialization and overloading (Programmer help)](https://programmer.help/blogs/c-templates-chapter-16-specialization-and-overloading.html)

 ### Iterators
  * [STL iterator basic operations](https://www.cs.helsinki.fi/u/tpkarkka/alglib/k06/lectures/iterators.html#example-simple-list)
  * [Iterators' operations possible implementations](https://en.cppreference.com/w/cpp/iterator)
  * [What is an iterator_trait](https://www.oreilly.com/library/view/c-in-a/059600298X/re673.html)
  * [Const correctness](https://isocpp.org/wiki/faq/const-correctness)
  * [Overloading const-ness only pointer and reference parameters](https://www.geeksforgeeks.org/function-overloading-and-const-functions/)
  * [Operations of random access iterators aritmetic](https://www.boost.org/sgi/stl/RandomAccessIterator.html#2)

 ### Specific algorithms
 * [What is an Allocator ?](https://medium.com/@vgasparyan1995/what-is-an-allocator-c8df15a93ed)
 * [Does std::stack exposes iterators](https://stackoverflow.com/questions/525365/does-stdstack-expose-iterators)
 * [About size_t and ptrdiff_t](https://pvs-studio.com/en/blog/posts/cpp/a0050/)
 * [SFINAE source code](https://en.wikipedia.org/wiki/Substitution_failure_is_not_an_error)
 * [How to use enable_if](https://eli.thegreenplace.net/2014/sfinae-and-enable_if/)

 ### Keywords and expressions
 * [Proper use of typedef](https://stackoverflow.com/questions/18385418/c-meaning-of-a-statement-combining-typedef-and-typename)
 * [Typedef, typename, dependancies, dependant names](https://stackoverflow.com/questions/610245/where-and-why-do-i-have-to-put-the-template-and-typename-keywords)
 * [Dependant names](https://en.cppreference.com/w/cpp/language/dependent_name)
 * [explicit keyword](https://www.geeksforgeeks.org/g-fact-93/)
 * [inline specifier](https://en.cppreference.com/w/cpp/language/inline)

 ### Algorithm complexity
  * [Amortized time](https://mortoray.com/2014/08/11/what-is-amortized-time/)
  * [Time complexity: examples](https://www.geeksforgeeks.org/understanding-time-complexity-simple-examples/)
