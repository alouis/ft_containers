/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Reverse_iterator.hpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/25 15:02:42 by alouis            #+#    #+#             */
/*   Updated: 2021/08/23 17:33:17 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef REVERSE_ITERATOR_HPP
# define REVERSE_ITERATOR_HPP

namespace ft
{
    template <class Iter>
    class const_reverse_iterator;

    template <class Iter, class ConstIter> 
    class reverse_iterator
    {
        public:
            typedef             Iter                                            iterator_type;
		    typedef             ConstIter                                       const_iterator_type;
            typedef typename    ft::Iterator_traits<Iter>::iterator_category    iterator_category;
            typedef typename    ft::Iterator_traits<Iter>::value_type           value_type;
            typedef typename    ft::Iterator_traits<Iter>::difference_type      difference_type;
            typedef typename    ft::Iterator_traits<Iter>::pointer              pointer;
            typedef typename    ft::Iterator_traits<Iter>::reference            reference;

            reverse_iterator() : _current(iterator_type()) {};

            explicit reverse_iterator (iterator_type it) : _current(it) {};
            
            reverse_iterator (const reverse_iterator<iterator_type, const_iterator_type>& rev_it) : _current(rev_it._current) {};

            reverse_iterator& operator=( const reverse_iterator& other ) {
                _current = other._current;
                return (*this);
            };
            
    		operator const_reverse_iterator<const_iterator_type>() const { 
                return const_reverse_iterator<const_iterator_type>(const_iterator_type(_current)); 
            };
            
            iterator_type base() const { return (_current); };

            reference operator*() {
                iterator_type    tmp(_current);

                tmp--;
                return (*tmp);
            };

            const reference operator*() const {
                iterator_type    tmp(_current);

                tmp--;
                return (*tmp);
            };

            pointer operator->() {
                iterator_type    tmp(_current);

                tmp--;
                return (&*tmp);
            };
            
            const pointer operator->() const {
                iterator_type    tmp(_current);

                tmp--;
                return (&*tmp);
            };

            reverse_iterator operator+ (difference_type n) const {
                reverse_iterator    tmp(*this);

                tmp._current -= n;
                return (tmp);
            };

            reverse_iterator operator-( difference_type n ) const {
                reverse_iterator    tmp(*this);

                tmp._current += n;
                return (tmp);
            };
            
            reverse_iterator& operator++() {
                _current--;
                
                return (*this);
            };

            reverse_iterator  operator++(int) {
                reverse_iterator    tmp(*this);

                ++(*this);
                return (tmp);
            };

            reverse_iterator& operator--() {
                _current++;

                return (*this);
            };

            reverse_iterator  operator--(int) {
                reverse_iterator    tmp(*this);

                --(*this);
                return (tmp);
            };

            reverse_iterator& operator+=( difference_type n ) {
                _current = _current - n;

                return (*this);
            };
            reverse_iterator& operator-=( difference_type n ) {
                _current = _current + n;

                return (*this);
            };

            reference operator[] (difference_type n) {
                return (_current - n - 1);
            };

            const reference operator[] (difference_type n) const {
                return (_current - n - 1);
            };

            difference_type operator-(reverse_iterator other) {
                return -(_current - other._current);
            };

            friend
            reverse_iterator operator+ ( typename reverse_iterator::difference_type n,
                const reverse_iterator &rev_it) {
                    return reverse_iterator(rev_it + n);
            };

            friend
            reverse_iterator::difference_type operator- (const reverse_iterator& lhs,
                const reverse_iterator& rhs) {
                    return (rhs._current - lhs._current);
            };

            template <class Iterator, class ConstIterator>
            friend bool operator==(const reverse_iterator<Iterator, ConstIterator> &lhs,
                                    const reverse_iterator<Iterator, ConstIterator> &rhs);

            template <class Iterator, class ConstIterator>
            friend bool operator<(const reverse_iterator<Iterator, ConstIterator> &lhs,
                                    const reverse_iterator<Iterator, ConstIterator> &rhs);

            template <class Iterator, class ConstIterator>
            friend bool operator==(const const_reverse_iterator<ConstIterator> &lhs,
                                    const reverse_iterator<Iterator, ConstIterator> &rhs);

        private:
            iterator_type       _current;      
    };
    
    template <class Iterator, class ConstIterator>
    bool operator==( const reverse_iterator<Iterator, ConstIterator>& lhs, 
                            const reverse_iterator<Iterator, ConstIterator>& rhs ) {
        return (lhs._current == rhs._current);
    };

    template <class Iterator, class ConstIterator>
	bool operator!=( const reverse_iterator<Iterator, ConstIterator>& lhs, 
                    const reverse_iterator<Iterator, ConstIterator>& rhs ) {
        return !(lhs == rhs);
    };

    template <class Iterator, class ConstIterator>
	bool operator<( const reverse_iterator<Iterator, ConstIterator>& lhs, 
                    const reverse_iterator<Iterator, ConstIterator>& rhs ) {
        return (lhs._current > rhs._current);
    };

    template <class Iterator, class ConstIterator>
	bool operator<=( const reverse_iterator<Iterator, ConstIterator>& lhs, 
                        const reverse_iterator<Iterator, ConstIterator>& rhs ) {
        return !(lhs > rhs);
    };

    template <class Iterator, class ConstIterator>
	bool operator>( const reverse_iterator<Iterator, ConstIterator>& lhs, 
                    const reverse_iterator<Iterator, ConstIterator>& rhs ) {
        return (rhs < lhs);
    };

    template <class Iterator, class ConstIterator>
	bool operator>=( const reverse_iterator<Iterator, ConstIterator>& lhs, 
                        const reverse_iterator<Iterator, ConstIterator>& rhs ) {
        return !(lhs < rhs);
    };


    template <class Iter>
    class const_reverse_iterator
    {
        public:
            typedef             Iter                                            iterator_type;
            typedef typename    ft::Iterator_traits<Iter>::iterator_category    iterator_category;
            typedef typename    ft::Iterator_traits<Iter>::value_type           value_type;
            typedef typename    ft::Iterator_traits<Iter>::difference_type      difference_type;
            typedef typename    ft::Iterator_traits<Iter>::pointer              pointer;
            typedef typename    ft::Iterator_traits<Iter>::reference            reference;

            const_reverse_iterator() { _current = iterator_type(); };

            explicit const_reverse_iterator (iterator_type it) { _current = it; };

            const_reverse_iterator (const const_reverse_iterator<iterator_type>& rev_it) { _current = rev_it._current; };

            const_reverse_iterator& operator=( const const_reverse_iterator& other ) { 
                _current = other._current;
                return (*this);
            };
            
            iterator_type base() const { return (_current); };

            const reference operator*() const {
                iterator_type    tmp(_current);

                tmp--;
                return (*tmp);
            };
            
            const pointer operator->() const {
                iterator_type    tmp(_current);

                tmp--;
                return (&*tmp);
            };

            const_reverse_iterator operator+ (difference_type n) const {
                const_reverse_iterator    tmp(*this);

                tmp._current -= n;
                return (tmp);
            };

            const_reverse_iterator operator-( difference_type n ) const {
                const_reverse_iterator    tmp(*this);

                tmp._current += n;
                return (tmp);
            };
            
            const_reverse_iterator& operator++() {
                _current--;
                
                return (*this);
            };

            const_reverse_iterator  operator++(int) {
                const_reverse_iterator    tmp(*this);

                ++(*this);
                return (tmp);
            };

            const_reverse_iterator& operator--() {
                _current++;

                return (*this);
            };

            const_reverse_iterator  operator--(int) {
                const_reverse_iterator    tmp(*this);

                --(*this);
                return (tmp);
            };

            const_reverse_iterator& operator+=( difference_type n ) {
                _current = _current - n;

                return (*this);
            };
            const_reverse_iterator& operator-=( difference_type n ) {
                _current = _current + n;

                return (*this);
            };

            const reference operator[] (difference_type n) const {
                return (_current - n - 1);
            };

            difference_type operator-(const_reverse_iterator other) {
                return -(_current - other._current);
            };

            friend
            const_reverse_iterator operator+ ( typename const_reverse_iterator::difference_type n,
                const_reverse_iterator &rev_it) {
                    return const_reverse_iterator(rev_it + n);
            };

            friend
            const_reverse_iterator::difference_type operator- (const_reverse_iterator& lhs,
                const_reverse_iterator& rhs) {
                    return (rhs._current - lhs._current);
            };

            template <class Iterator>
            friend bool operator==(const const_reverse_iterator<Iterator> &lhs,
                                const const_reverse_iterator<Iterator> &rhs);

            template <class Iterator>
            friend bool operator<(const const_reverse_iterator<Iterator> &lhs,
                                const const_reverse_iterator<Iterator> &rhs);

            template <class Iterator, class ConstIterator>
            friend bool operator==(const const_reverse_iterator<ConstIterator> &lhs,
                                const reverse_iterator<Iterator, ConstIterator> &rhs);
                                        
        private:
            iterator_type   _current;
    };

    template <class Iterator>
	bool operator==(const const_reverse_iterator<Iterator> &lhs,
					const const_reverse_iterator<Iterator> &rhs) {
		return (lhs._current == rhs._current);
	};

	template <class Iterator>
	bool operator!=(const const_reverse_iterator<Iterator> &lhs,
					const const_reverse_iterator<Iterator> &rhs) {
		return !(lhs == rhs);
	};

	template <class Iterator>
	bool operator<(const const_reverse_iterator<Iterator> &lhs,
				   const const_reverse_iterator<Iterator> &rhs) {
		return (lhs._current > rhs._current);
	};

	template <class Iterator>
	bool operator<=(const const_reverse_iterator<Iterator> &lhs,
					const const_reverse_iterator<Iterator> &rhs) {
		return !(lhs > rhs);
	};

	template <class Iterator>
	bool operator>(const const_reverse_iterator<Iterator> &lhs,
				   const const_reverse_iterator<Iterator> &rhs) {
		return (rhs < lhs);
	};

	template <class Iterator>
	bool operator>=(const const_reverse_iterator<Iterator> &lhs,
					const const_reverse_iterator<Iterator> &rhs) {
		return !(lhs < rhs);
	};

	template <class Iterator, class ConstIterator>
	bool operator ==(const const_reverse_iterator<ConstIterator> &lhs,
					const reverse_iterator<Iterator, ConstIterator> &rhs) {
		return lhs._current == rhs._current;
	};

	template <class Iterator, class ConstIterator>
	bool operator !=(const const_reverse_iterator<ConstIterator> &lhs,
					const reverse_iterator<Iterator, ConstIterator> &rhs) {
		return !(lhs == rhs);
	};

	template <class Iterator, class ConstIterator>
	bool operator <(const const_reverse_iterator<ConstIterator> &lhs,
					const reverse_iterator<Iterator, ConstIterator> &rhs) {
		return (lhs._current > rhs._current);
	};

	template <class Iterator, class ConstIterator>
	bool operator >=(const const_reverse_iterator<ConstIterator> &lhs,
					const reverse_iterator<Iterator, ConstIterator> &rhs) {
		return !(lhs < rhs);
	};

	template <class Iterator, class ConstIterator>
	bool operator >(const const_reverse_iterator<ConstIterator> &lhs,
					const reverse_iterator<Iterator, ConstIterator> &rhs) {
		return (rhs < lhs);
	};

	template <class Iterator, class ConstIterator>
	bool operator <=(const const_reverse_iterator<ConstIterator> &lhs,
					const reverse_iterator<Iterator, ConstIterator> &rhs) {
		return !(lhs > rhs);
	};
};

#endif