/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Random_access_iterator.hpp                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/09 11:43:05 by alouis            #+#    #+#             */
/*   Updated: 2021/08/23 09:37:06 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RANDOM_ACCESS_ITERATOR_HPP
# define RANDOM_ACCESS_ITERATOR_HPP

# include "Iterator.hpp"

namespace ft
{
    template <class T>
    class random_access_iterator : public ft::baseIterator<ft::random_access_iterator_tag, T>
    {
        private:
            typedef ft::baseIterator<ft::random_access_iterator_tag, T> base_iterator;
            
        public:
            typedef typename    base_iterator::difference_type   difference_type;
            typedef typename    base_iterator::value_type        value_type;
            typedef typename    base_iterator::pointer           pointer;
            typedef typename    base_iterator::reference         reference;
            typedef typename    base_iterator::iterator_category iterator_category;
            
            random_access_iterator(void) : _ptr(NULL) {};
            random_access_iterator(const pointer ptr) : _ptr(ptr) {};
            random_access_iterator& operator=(const random_access_iterator<T> &other) {
                _ptr = other._ptr;
                return (*this);
            };

            // from compiler perspective const T and T are two totally different types so it needs explicit conversion: const iterator(iterator) kinda
            operator random_access_iterator<const T>() const {
                return (random_access_iterator<const T>(_ptr));
            };

            ~random_access_iterator(void) {};
            
            random_access_iterator&   operator++()                                    // pre-incrementation (++it)
            {
                ++_ptr;
                return (*this); 
            };

            random_access_iterator    operator++(int)                                 // post-incrementation (it++)
            {
                random_access_iterator tmp(*this);
                
                operator++(); 
                return tmp;
            };

            random_access_iterator&   operator--()                                    // pre-decrementation (--it)
            {
                --_ptr;
                return (*this); 
            };
 
            random_access_iterator    operator--(int)                                 // post-decrementation (it--)
            {
                random_access_iterator tmp(*this);
                
                operator--(); 
                return tmp;
            };

            reference   operator*() const { return (*_ptr); };

            pointer     operator->() const { return &(operator*()); };

            bool    operator==(const random_access_iterator<T> &other) const {
                return (_ptr == other._ptr);
            };
            
            bool    operator!=(const random_access_iterator<T> &other) const {
                return (_ptr != other._ptr);
            };
            
            bool    operator<(const random_access_iterator<T> &other) const {
                return (_ptr < other._ptr);
            };

            bool    operator>(const random_access_iterator<T> &other) const {
                return !(*this < other);
            };

            bool    operator<=(const random_access_iterator<T> &other) const {
                return !(*this > other);
            };

            bool    operator>=(const random_access_iterator<T> &other) const {
                return !(*this < other);
            };

            random_access_iterator& operator+=(random_access_iterator<T>::difference_type n) {
                while (n > 0)
                {
                    ++(*this);
                    n--;;
                }
                while (n < 0)
                {
                    --(*this);
                    n++;
                }
                return (*this);
            };

            random_access_iterator  operator+(const random_access_iterator<T>::difference_type n) { 
                random_access_iterator tmp(*this);
                 
                return tmp += n;
            };
            
            random_access_iterator& operator-=(const random_access_iterator<T>::difference_type n) {
                operator+=(-n);
                return (*this);
            };

            random_access_iterator  operator-(const random_access_iterator<T>::difference_type n) { 
                random_access_iterator tmp(*this);
                 
                return tmp -= n;
            };

            value_type&  operator[](difference_type n) { return *(_ptr + n); };

            const value_type&  operator[](difference_type n) const { return *(_ptr + n); };

            friend
            random_access_iterator<T>::difference_type distance(const random_access_iterator<T> &left, const random_access_iterator<T> &right) {
                return (left._ptr - right._ptr);
            };

            friend
            random_access_iterator<T>::difference_type operator-(const random_access_iterator<T>& left, const random_access_iterator<T>& right) {
                return (distance(left, right));
            }


        private:
            pointer _ptr;
    };
;}

#endif