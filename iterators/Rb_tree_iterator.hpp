/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Rb_tree_iterator.hpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/05 18:38:21 by alouis            #+#    #+#             */
/*   Updated: 2021/08/18 16:59:32 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RB_TREE_ITERATOR_HPP
# define RB_TREE_ITERATOR_HPP

# include "Iterator.hpp"
# include "../utils/rb_tree.hpp"

namespace ft
{
    template <class Value, class NodePointer>
    class rb_tree_iterator : public ft::baseIterator<ft::bidirectional_iterator_tag, Value>
    {
        private:
            typedef ft::baseIterator<ft::bidirectional_iterator_tag, Value> base_iterator;
            typedef NodePointer                                             node_ptr;

        public:
            typedef typename    base_iterator::difference_type   difference_type;
            typedef typename    base_iterator::value_type        value_type;
            typedef typename    base_iterator::pointer           pointer;
            typedef typename    base_iterator::reference         reference;
            typedef typename    base_iterator::iterator_category iterator_category;

            rb_tree_iterator() : _node(NULL) {};
            rb_tree_iterator(node_ptr node) : _node(node) {};
            rb_tree_iterator(const rb_tree_iterator &other) : _node(other._node) {};

            rb_tree_iterator& operator= (const rb_tree_iterator &other) {
                _node = other._node;
                return (*this);
            };
            
            // from compiler perspective const T and T are two totally different types so it needs explicit conversion: const iterator(iterator) kinda
            operator rb_tree_iterator<const Value, NodePointer>() const {
                return (rb_tree_iterator<const Value, NodePointer>(_node));
            };

            ~rb_tree_iterator() {};

            rb_tree_iterator& operator++() {
                increment();
                return (*this);
            };

            rb_tree_iterator operator++(int) {
                rb_tree_iterator  tmp(*this);

                increment();
                return (tmp);
            };

            rb_tree_iterator& operator--() {
                decrement();
                return (*this);
            };

            rb_tree_iterator operator--(int) {
                rb_tree_iterator  tmp(*this);

                decrement();
                return (tmp);
            };

            bool operator== (const rb_tree_iterator<Value, NodePointer> &other) const {
                return (_node == other._node);
            };

            bool operator!= (const rb_tree_iterator<Value, NodePointer> &other) const {
                return (_node != other._node);
            };

            reference operator* () const {
                return (_node->getValue());
            };
            
            pointer operator-> () const {
                return &(operator*());
            };

            node_ptr getNode() {
                return _node;
            };

        private:
            node_ptr    _node;

            void    increment() {
                if (_node->_right)
                {
                    _node = _node->_right;
                    while (_node->_left)
                        _node = _node->_left;
                }
                else
                {
                    node_ptr parent = _node->_parent;

                    while (_node == parent->_right)
                    {
                        _node = parent;
                        parent = _node->_parent;
                    }
                    if (_node->_right != parent)
                        _node = parent;
                }
            };

            void    decrement() {
                if (_node->_parent && _node->_parent->_parent && _node->_parent->_parent == _node && _node->_color == 0)
                    _node = _node->_right;
                else if (_node->_left)
                {
                    _node = _node->_left;
                    while (_node->_right)
                        _node = _node->_right;
                }
                else
                {
                    node_ptr parent = _node->_parent;

                    while (parent->_left == _node)
                    {
                        _node = parent;
                        parent = _node->_parent;
                    }
                    _node = parent;
                }
            };

        template <class, class, class, class, class>
		friend class rb_tree;

        template <class>
		friend class reverse_rb_tree_iterator;
    };

    template <class Iter> 
    class reverse_rb_tree_iterator
    {
        public:
            typedef             Iter                                            iterator_type;
            typedef typename    ft::Iterator_traits<Iter>::iterator_category    iterator_category;
            typedef typename    ft::Iterator_traits<Iter>::value_type           value_type;
            typedef typename    ft::Iterator_traits<Iter>::difference_type      difference_type;
            typedef typename    ft::Iterator_traits<Iter>::pointer              pointer;
            typedef typename    ft::Iterator_traits<Iter>::reference            reference;

            reverse_rb_tree_iterator() : _current(iterator_type()) {};

            explicit reverse_rb_tree_iterator (iterator_type it) : _current(--it) {};

            reverse_rb_tree_iterator (const reverse_rb_tree_iterator<Iter>& rev_it) : _current(rev_it._current) {};

            reverse_rb_tree_iterator& operator=( const reverse_rb_tree_iterator& other ) {
                _current = other._current;
                return (*this);
            };
            
            // template <typename T>
            // operator reverse_rb_tree_iterator<ft::rb_tree_iterator<const T> >() const {
            //     return (reverse_rb_tree_iterator<ft::rb_tree_iterator<const T> >(_current));
            // };

            iterator_type base() const { return (_current); };

            reference operator*() const {
                iterator_type    tmp(_current);

                return (*tmp);
            };

            pointer operator->() const {
                iterator_type    tmp(_current);

                return (&*tmp);
            };
            
            reverse_rb_tree_iterator& operator++() {
                _current--;
                
                return (*this);
            };

            reverse_rb_tree_iterator  operator++(int) {
                reverse_rb_tree_iterator    tmp(*this);

                ++(*this);
                return (tmp);
            };

            reverse_rb_tree_iterator& operator--() {
                if (_current._node->is_header == false)
                    _current++;
                else
                    _current = _current._node->_left;
                return (*this);
            };

            reverse_rb_tree_iterator  operator--(int) {
                reverse_rb_tree_iterator    tmp(*this);

                --(*this);
                return (tmp);
            };

            friend
            bool operator==( const reverse_rb_tree_iterator& lhs, const reverse_rb_tree_iterator& rhs ) {
                return (lhs._current == rhs._current);
            };

            friend
            bool operator!=( const reverse_rb_tree_iterator& lhs, const reverse_rb_tree_iterator& rhs ) {
                return (lhs._current != rhs._current);
            };
            
        private:
            iterator_type    _current;
        
        template <class, class, class, class, class>
		friend class rb_tree;
    };
};

#endif