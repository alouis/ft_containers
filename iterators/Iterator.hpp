/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Iterator.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/06 14:23:06 by alouis            #+#    #+#             */
/*   Updated: 2021/08/07 14:50:58 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ITERATOR_HPP
# define ITERATOR_HPP

# include <memory>
# include <cstddef>

namespace ft
{  
    //empty structs to identify which type of iterator is needed (tag dispatching method)
    struct bidirectional_iterator_tag
	{
	}; 

    struct random_access_iterator_tag
	{
	};

    // "base class to ease the definition of required types for simple iterators"
    template <class Category, class T, class Distance = ptrdiff_t,
          class Pointer = T*, class Reference = T&>
    struct baseIterator {
        typedef T         value_type;
        typedef Distance  difference_type;
        typedef Pointer   pointer;
        typedef Reference reference;
        typedef Category  iterator_category;
    };

    // "iterator_traits: template class that provides uniform interface to the properties of an iterator
    // This makes it possible to implement algorithms only in terms of iterators."
    template <class Iterator>
    class Iterator_traits
    {
        public:
            typedef typename    Iterator::difference_type   difference_type;        //ptrdiff_t
            typedef typename    Iterator::value_type        value_type;             //T
            typedef typename    Iterator::pointer           pointer;                //T*
            typedef typename    Iterator::reference         reference;              //T&
            typedef typename    Iterator::iterator_category iterator_category;      //ft::random_access_iterator_tag
    };

    // "It is also specialized for pointers (T*) and pointers to const (const T*)"
    template <class T> 
    class Iterator_traits<T*>
    {
        public:
            typedef             ptrdiff_t                       difference_type;
            typedef             T                               value_type;
            typedef             T*                              pointer;
            typedef             T&                              reference;
            typedef             ft::random_access_iterator_tag iterator_category;
    };
    
     
    template <class T> 
    class Iterator_traits<const T*>
    {
        public:
            typedef             ptrdiff_t                       difference_type;
            typedef             T                               value_type;
            typedef             const T*                        const_pointer;          // /!\ OR pointer;
            typedef             const T&                        const_reference;        // /!\ OR reference;
            typedef             ft::random_access_iterator_tag iterator_category;
    };

    //to resolve error: type 'int' cannot be used prior to '::' because it has no members -> silencing them all 
	template<> class Iterator_traits<int>{};
    template<> class Iterator_traits<char>{};
    template<> class Iterator_traits<double>{};
    template<> class Iterator_traits<std::string>{};
    template<> class Iterator_traits<unsigned int>{};
    template<> class Iterator_traits<long>{};
    template<> class Iterator_traits<long long>{};
    template<> class Iterator_traits<short int>{};
    template<> class Iterator_traits<bool>{};
    template<> class Iterator_traits<unsigned long>{};
    template<> class Iterator_traits<unsigned long long>{};


    // calculating distance using tag dispatching so algorithms are specific to iterator's type (random access substracts while bidirectionnal iterates) 
    template <class InputIterator>
    typename Iterator_traits<InputIterator>::difference_type
    distance (InputIterator first, InputIterator last) {
        return (distance(first, last, typename Iterator_traits<InputIterator>::iterator_category()));
    };

    template<class InputIterator>
    typename Iterator_traits<InputIterator>::difference_type
    distance (InputIterator first, InputIterator last, ft::random_access_iterator_tag) {
        return (last - first);
    };

    // can't use this one with map's iterator though, iterating through rb trees requires authorization their iterators don't have. Would work with a list type container.
    template<class InputIterator>
    typename Iterator_traits<InputIterator>::difference_type
    distance (InputIterator first, InputIterator last, ft::bidirectional_iterator_tag) {
        typename Iterator_traits<InputIterator>::difference_type n;

        n = 0;
        while (first != last)
        {
            n++;
            first++;
        }
        return (n);
    };
}; 

#endif