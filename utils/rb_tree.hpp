/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rb_tree.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/30 16:31:31 by alouis            #+#    #+#             */
/*   Updated: 2021/08/23 14:05:00 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RB_TREE_HPP
# define RB_TREE_HPP
# include "../iterators/Rb_tree_iterator.hpp"
# include "../utils/pair.hpp"

namespace ft
{
    enum Color { RED, BLACK };
    
    template<class Value>
    class rb_tree_node
    {
        public:
            typedef     Value           value_type;

            rb_tree_node() : _value(value_type()), _parent(NULL), _left(NULL), _right(NULL), _color(RED) {};

            rb_tree_node(const value_type &v, rb_tree_node *p = NULL, rb_tree_node *l = NULL, rb_tree_node *r = NULL) : _value(v), _parent(p), _left(l), _right(r), _color(RED) {};
            
            rb_tree_node(const rb_tree_node &other) {
                *this = other;
            };

            rb_tree_node& operator= (const rb_tree_node &other) {
                if (this != &other)
                {
                    _value = other._value;
                    _parent = other._parent;
                    _left = other._left;
                    _right = other._right;
                    _color = other._color;
                }
                return (*this);
            };

            ~rb_tree_node() {};

            static rb_tree_node* leftmost(rb_tree_node *x)
            {
                if (x == NULL)
                    return (NULL);
                while (x->_left != NULL) 
                    x = x->_left;
                return x;
            };

            static rb_tree_node* rightmost(rb_tree_node *x)
            {
                if (x == NULL)
                    return (NULL);
                while (x->_right != NULL) 
                    x = x->_right;
                return x;
            };

            const value_type& getValue() const {
                return (_value);
            };

            value_type& getValue() {
                return (_value);
            };

        private:
            value_type      _value;
            rb_tree_node    *_parent;
            rb_tree_node    *_left;
            rb_tree_node    *_right;
            Color           _color;
            bool            is_header;

        //friend of rb_tree and rb_tree_iterator so that they can access its private data
        template <class, class, class, class, class>
		friend class rb_tree;

		template <class, class>
		friend class rb_tree_iterator;

		template <class>
		friend class reverse_rb_tree_iterator;
    };

    template <class Key, class Value, class KeyOfValue, class Compare, class Alloc = std::allocator<Value> >
    class rb_tree
    {
        public:
            typedef             Key                                                         key_type;
            typedef             Value                                                       value_type;
            typedef             Alloc                                                       allocator_type;
            typedef typename    allocator_type::size_type                                   size_type;
            typedef             rb_tree_node<Value>                                         Node;
            typedef             rb_tree_node<Value>*                                        NodePointer;
            typedef typename    std::allocator<Node>                                        node_allocator;
            typedef typename    ft::rb_tree_iterator<Value, NodePointer>                    iterator;
            typedef typename    ft::rb_tree_iterator<Value const, NodePointer>              const_iterator;
            typedef typename    ft::reverse_rb_tree_iterator<iterator>                      reverse_iterator;
            typedef typename    ft::reverse_rb_tree_iterator<const_iterator>                const_reverse_iterator;

            rb_tree(const allocator_type& alloc = allocator_type()) :
                _alloc(alloc), 
                _node_alloc(node_allocator()),
                _node_count(0) {
                    empty_initialize();
            };

            rb_tree(const rb_tree &other) : 
                _alloc(other.get_allocator()), 
                _node_alloc(other._node_alloc),
                key_compare(other.key_compare), 
                _node_count(0) {
                    if (other._node_count == 0)
                        empty_initialize();
                    else
                        *this = other;
            };

            ~rb_tree() {
                clear();
                _node_alloc.deallocate(_header, 1);
            };
            
            rb_tree&    operator= (const rb_tree &other) {
                const_iterator    first(other.begin());
                const_iterator    last(other.end());
                
                clear();
                while (first != last)
                {
                    insert_unique(*first);
                    first++;
                }
                return *this;
            };

            iterator begin() {
                return (leftmost());
            };

            const_iterator begin() const {
                return (leftmost());
            };

            iterator end() {
                return (_header);
            };

            const_iterator end() const {
                return (_header);
            };

            reverse_iterator rbegin() {
                return reverse_iterator(_header);
            };

            const_reverse_iterator rbegin() const {
                return const_reverse_iterator(_header);
            };
            
            reverse_iterator rend() {
                return reverse_iterator(leftmost());
            };
            
            const_reverse_iterator rend() const {
                return const_reverse_iterator(leftmost());
            };

            bool empty() const {
                return !(_node_count);
            };

            size_type size() const {
                return (_node_count);
            };
            
            size_type max_size() const {
                return (_node_alloc.max_size());
            };

            pair<iterator,bool> insert_unique (const value_type& val) {     //used for map
                NodePointer x;
                NodePointer y;
                iterator j;

                y = _header;
                x = root();
                while (x != NULL)   // Starting from the root node, go down to find the appropriate insertion point (left if encountering less than or equal to and right if big)
                {
                    y = x;
                    if (ft::is_equal<key_type>()(KeyOfValue()(val), KeyOfValue()(x->_value)))   //key of val already exists in tree
                    {
                        j = y;
                        return pair<iterator, bool>(j, false);
                    }
                    else
                        x = (key_compare(KeyOfValue()(val), KeyOfValue()(x->_value))) ? x->_left : x->_right;
                }
                j = y;
                if (j == begin())
                    return ft::pair<iterator, bool>(_insert(x, y, val), true);
                else
                    --j;
                if (key_compare(KeyOfValue()(j._node->_value), KeyOfValue()(val)))  // Less than the new value, insert on the right when it is small
		            return pair<iterator, bool>(_insert(x, y, val), true);
	            return pair<iterator, bool>(j, false);             // Otherwise the new value must be the same as the key value in the tree
            };
            
            pair<iterator,bool> insert_unique_set (const value_type& val) {     //used for map
                NodePointer x;
                NodePointer y;
                iterator j;

                y = _header;
                x = root();
                while (x != NULL)   // Starting from the root node, go down to find the appropriate insertion point (left if encountering less than or equal to and right if big)
                {
                    y = x;
                    if (ft::is_equal<key_type>()(val, x->_value))   //key of val already exists in tree
                    {
                        j = y;
                        return pair<iterator, bool>(j, false);
                    }
                    else
                        x = (key_compare(val, x->_value)) ? x->_left : x->_right;
                }
                j = y;
                if (j == begin())
                    return ft::pair<iterator, bool>(_insert_set(x, y, val), true);
                else
                    --j;
                if (key_compare(*j, val))  // Less than the new value, insert on the right when it is small
		            return pair<iterator, bool>(_insert_set(x, y, val), true);
	            return pair<iterator, bool>(j, false);             // Otherwise the new value must be the same as the key value in the tree
            };

            void insert_range (iterator first, iterator last) {
                while (first != last)
                {
                    if (find(first->first) == end())
                        insert_unique(*first);
                    first++;
                }
            };

            void insert_range_set (iterator first, iterator last) {
                while (first != last)
                {
                    if (find_set(*first) == end())
                        insert_unique_set(*first);
                    first++;
                }
            };

            iterator find (const key_type& k) {
                iterator    f(begin());

                while (f != end())
                {
                    if (ft::is_equal<key_type>()(k, KeyOfValue()(f._node->_value)))
                        return f;
                    f++;
                }
                return (end());
            };

            const_iterator find (const key_type& k) const {
                const_iterator    f(begin());

                while (f != end())
                {
                    if (ft::is_equal<key_type>()(k, KeyOfValue()(f._node->_value)))
                        return f;
                    f++;
                }
                return (end());
            };

            iterator find_set (const key_type& k) {
                iterator    f(begin());

                while (f != end())
                {
                    if (ft::is_equal<key_type>()(k, f._node->_value))
                        return f;
                    f++;
                }
                return (end());
            };
            
            const_iterator find_set (const key_type& k) const {
                const_iterator    f(begin());

                while (f != end())
                {
                    if (ft::is_equal<key_type>()(k, f._node->_value))
                        return f;
                    f++;
                }
                return (end());
            };
            //REAL FIND, COULDNT MAKE IT WORK
            // iterator find(const Key& k) {
            //     link_type y = header;
            //     link_type x = root();
                
            //     while (x != 0) {
            //         if (!key_compare(key(x), k))
            //         // x key value is greater than k, go left when encountering a large value
            //         y = x, x = left(x);
            //         else
            //         // The x key value is greater than k, go to the right when it encounters a small value
            //         x = right(x);
            //     }
            //     iterator j = iterator(y);
            //     return (j == end() || key_compare(k, key(j.node))) ? end() : j;
            // };

            iterator lower_bound (const key_type& k) {
                iterator first(begin());
                iterator last(end());

                while (first != last)
                {
                    if (!key_compare(KeyOfValue()(first._node->_value), k))
                        return first;
                    first++;
                }
                return (end());
            };

            const_iterator lower_bound (const key_type& k) const {
                const_iterator first(begin());
                const_iterator last(end());

                while (first != last)
                {
                    if (!key_compare(KeyOfValue()(first._node->_value), k))
                        return first;
                    first++;
                }
                return (end());
            };

            iterator upper_bound (const key_type& k) {
                iterator first(begin());
                iterator last(end());

                while (first != last)
                {
                    if (key_compare(KeyOfValue()(first._node->_value), k) || ft::is_equal<key_type>()(KeyOfValue()(first._node->_value), k))
                        first++;
                    else
                        return first;
                }
                return (end());
            };

            const_iterator upper_bound (const key_type& k) const {
                const_iterator first(begin());
                const_iterator last(end());

                while (first != last)
                {
                    if (key_compare(KeyOfValue()(first._node->_value), k) || ft::is_equal<key_type>()(KeyOfValue()(first._node->_value), k))
                        first++;
                    else
                        return first;
                }
                return (end());
            };

            iterator lower_bound_set (const key_type& k) {
                iterator first(begin());
                iterator last(end());

                while (first != last)
                {
                    if (!key_compare(*first, k))
                        return first;
                    first++;
                }
                return (end());
            };
            
            const_iterator lower_bound_set (const key_type& k) const {
                const_iterator first(begin());
                const_iterator last(end());

                while (first != last)
                {
                    if (!key_compare(*first, k))
                        return first;
                    first++;
                }
                return (end());
            };

            iterator upper_bound_set (const key_type& k) {
                iterator first(begin());
                iterator last(end());

                while (first != last)
                {
                    if (key_compare(*first, k) || ft::is_equal<key_type>()(*first, k))
                        first++;
                    else
                        return first;
                }
                return (end());
            };

            const_iterator upper_bound_set (const key_type& k) const {
                const_iterator first(begin());
                const_iterator last(end());

                while (first != last)
                {
                    if (key_compare(*first, k) || ft::is_equal<key_type>()(*first, k))
                        first++;
                    else
                        return first;
                }
                return (end());
            };
            
            void erase (iterator position) {
                NodePointer x = position._node;

                if (_node_count == 0)
                    return ;
                if (_node_count == 1)
                    clear();
                else if (x->_right == NULL && x->_left == NULL)
                    erase_leave(x);
                else if ((x->_right && x->_left == NULL) || (x->_right == NULL && x->_left))
                    erase_node_with_one_child(x);
                else
                    erase_node_with_two_children(position);
                return ;
            };
            
            void erase (iterator first, iterator last) {
                if (first == begin() && last == end())
                    clear();
                else
                {
                    iterator tmp = first;
                    
                    while (first != last)
                    {
                        first++;
                        erase(tmp);
                        tmp = first;
                    }
                }
            };

            void    clear() {
               if (_node_count != 0)
               {
                    erase_recursive(root());
                    root() = NULL;
                    leftmost() = _header;
                    rightmost() = _header;
               }
            };

            allocator_type get_allocator() const { return (_alloc); };

            void    print_tree() {
                std::cout << "======" << std::endl;
                // std::cout << "header" << std::endl;
                // std::cout << "self: " << _header << std::endl;
                // std::cout << "parent: " << _header->_parent << std::endl;
                // std::cout << "left: " << _header->_left << std::endl;
                // std::cout << "right: " << _header->_right << std::endl;

                if (root()) {
                    std::cout << "root" << std::endl;
                   std::cout << "root() = " << KeyOfValue()(root()->getValue()) << "\nrightmost() = " << KeyOfValue()(rightmost()->getValue()) << "\nleftmost() = " << KeyOfValue()(leftmost()->getValue()) << std::endl;
                } else
                    std::cout << "This is an empty tree\n";
                std::cout << "ELEMENTS:\n";
                for (iterator first = begin(); first != end(); first++)
                {
                    std::cout << "--" << std::endl;
                    NodePointer pnode = first.getNode();
                    std::cout << "[" << first->first << "] = " << first->second << std::endl;
                    std::cout << "self: " << pnode << " [" << KeyOfValue()(pnode->getValue()) << "] " << std::endl;
                    std::cout << "parent: " << pnode->_parent << std::endl;
                    std::cout << "left: " << pnode->_left;
                    if (pnode->_left)
                        std::cout << " [" << KeyOfValue()(pnode->_left->getValue()) << "] " << std::endl;
                    else
                        std::cout << std::endl;
                    std::cout << "right: " << pnode->_right;
                    if (pnode->_right)
                        std::cout << " [" << KeyOfValue()(pnode->_right->getValue()) << "] " << std::endl;
                    else
                        std::cout << std::endl;
                }
                std::cout << "=======" << std::endl;
            };

        private:
            allocator_type  _alloc;
            node_allocator  _node_alloc;
            
            NodePointer     _header;
            size_type       _node_count;
            Compare         key_compare;

            NodePointer&   root() const {
                return (_header->_parent);
            };

            NodePointer&   leftmost() const {
                return (_header->_left);
            };

            NodePointer&   rightmost() const {
                return (_header->_right);
            };
            
            void    empty_initialize() {
                _header = _node_alloc.allocate(1);
                _header->_color = RED;
                _header->_parent = NULL;
                _header->_left = _header;
                _header->_right = _header;
                _header->is_header = true;
            };

            NodePointer create_node(const value_type& val) {
                NodePointer new_node;

                new_node = _node_alloc.allocate(1);
                _alloc.construct(&new_node->_value, val);
                return (new_node);
            };

            void        destroy_node(NodePointer node) {
                if (node != NULL)
                {
                    _alloc.destroy(&(node->_value));
                    _node_alloc.deallocate(node, 1);
                }
            };
            
            iterator _insert(NodePointer x, NodePointer y, const value_type& val) {
                NodePointer z;

                z = create_node(val);
                if (y == _header || x != 0 || key_compare(KeyOfValue()(val), KeyOfValue()(y->_value)))
                {
                    y->_left = z;                //so z is leftmost if y is header
                    if (y == _header)
                    {
                        root() = z;
                        rightmost() = z;
                    }
                    else if (y == leftmost())
                        leftmost() = z;
                }
                else
                {
                    y->_right = z;
                    if (y == rightmost())
                        rightmost() = z;
                }
                z->_parent = y;
                z->_left = NULL;
                z->_right = NULL;
                z->is_header = false;
                rb_tree_rebalance(z, root());
                _node_count += 1;
                
                return (z);
            };

            iterator _insert_set(NodePointer x, NodePointer y, const value_type& val) {
                NodePointer z;

                z = create_node(val);
                if (y == _header || x != 0 || key_compare(val, y->_value))
                {
                    y->_left = z;                //so z is leftmost if y is header
                    if (y == _header)
                    {
                        root() = z;
                        rightmost() = z;
                    }
                    else if (y == leftmost())
                        leftmost() = z;
                }
                else
                {
                    y->_right = z;
                    if (y == rightmost())
                        rightmost() = z;
                }
                z->_parent = y;
                z->_left = NULL;
                z->_right = NULL;
                z->is_header = false;
                rb_tree_rebalance(z, root());
                _node_count += 1;
                
                return (z);
            };

            void    rotate_left(NodePointer x) {
                //x is the pivot point, rotate the right node of x to the left, and replace x with y
                NodePointer y;

                y = x->_right;
                x->_right = y->_left;

                //Set the parent node
                if (y->_left != NULL)
                    y->_left->_parent = x;
                y->_parent = x->_parent;

                if (x == root())
                    root() = y;
                else if (x == x->_parent->_left)
                    x->_parent->_left = y;
                else
                    x->_parent->_right = y;

                //Father and son upside down
                y->_left = x;
                x->_parent = y;
            };

            void    rotate_right(NodePointer x) {
                //x is the pivot point, rotate the left node of x to the right, and replace x with y
                NodePointer y;
                
                y = x->_left;
                x->_left = y->_right;

                if (y->_right != NULL)
                    y->_right->_parent = x;
                y->_parent = x->_parent;

                if (x == root())
                    root() = y;
                else if (x == x->_parent->_left)
                    x->_parent->_left = y;
                else
                    x->_parent->_right = y;

                //Father and son upside down
                y->_right = x;
                x->_parent = y;
            };

            // Parameter one is the new node, parameter two is root
            void    rb_tree_rebalance(NodePointer x, NodePointer& root) {
                NodePointer y;

                x->_color = RED;// new node must be red
                while (x != root && x->_parent->_color == RED) // If the parent node of the inserted node is red, it must be adjusted
                {
                    if (x->_parent == x->_parent->_parent->_left) // The parent node is the left child node of the grandparent node
                    {
                        y = x->_parent->_parent->_right;	// y is the uncle node
                        if (y && y->_color == RED) // Case 1: Both the parent node and the uncle node are red
                        {
                            x->_parent->_color = BLACK;		        // make the parent node black
                            y->_color = BLACK;						// make the uncle node black
                            x->_parent->_parent->_color = RED;      // Make the grandfather node red
                            x = x->_parent->_parent;
                        }
                        else            // No uncle node or uncle node is black
                        {
                            if (x == x->_parent->_right) // Case 2: The new node is the right child node of the parent node
                            {
                                x = x->_parent;
                                rotate_left(x);		// Rotate left to its parent node
                            }
                            x->_parent->_color = BLACK;
                            x->_parent->_parent->_color = RED;
                            rotate_right(x->_parent->_parent);
                        }
                    }
                    else // The parent node is the right child node of the grandparent node
                    {
                        y = x->_parent->_parent->_left;	// y is the uncle node
                        if (y && y->_color == RED) // Case 1: Both the parent node and the uncle node are red
                        {
                            x->_parent->_color = BLACK;		    // make the parent node black
                            y->_color = BLACK;					// make the uncle node black
                            x->_parent->_parent->_color = RED;   // Make the grandfather node red
                            x = x->_parent->_parent;		                // ready to check up
                        }
                        else
                        {	
                            if (x == x->_parent->_left) // Case 2: The new node is the left child node of the parent node
                            {
                                x = x->_parent;
                                rotate_right(x);		// Rotate right to its parent node
                            }
                            x->_parent->_color = BLACK;
                            x->_parent->_parent->_color = RED;
                            rotate_left(x->_parent->_parent);
                        }
                    }
                }
                root->_color = BLACK;
            };

            void    erase_recursive(NodePointer x) {
                if (x->_left)
                    erase_recursive(x->_left);
                if (x->_right)
                    erase_recursive(x->_right);
                destroy_node(x);
                _node_count -= 1;
            };

            void    erase_leave(NodePointer x) {
                // update header's pointer if needed
                if (x == rightmost())
                    rightmost() = x->_parent;
                else if (x == leftmost())
                    leftmost() = x->_parent;
                // x's parent points to NULL
                if (x == x->_parent->_right)
                    x->_parent->_right = NULL;
                else if (x == x->_parent->_left)
                    x->_parent->_left = NULL;
                destroy_node(x);
                _node_count -= 1;
                rb_tree_delete_rebalance(root());
            };

            void    erase_node_with_one_child(NodePointer x) {
                NodePointer y = (x->_right) ? x->_right : x->_left;

                // update header's pointer if needed
                if (x == rightmost())
                    rightmost() = (x->_left->_right == NULL) ? x->_left : x->_left->_right;
                else if (x == leftmost())
                    leftmost() = (x->_right->_left == NULL) ? x->_right : x->_right->_left; // because tree isn't well balanced and sometimes when deleting from start right subtree doesn't exist anymore
                // set NULL where y was
                if (x->_right == y)
                    x->_right = NULL;
                else if (x->_left == y)
                    x->_left = NULL;
                // y->_parent points to x parent
                y->_parent = x->_parent;
                // x's parent points to y
                if (x->_parent->_right == x)
                    x->_parent->_right = y;
                else if (x->_parent->_left == x)
                    x->_parent->_left = y;
                else if (x == root())
                    _header->_parent = y;
                destroy_node(x);
                _node_count -= 1;
                rb_tree_delete_rebalance(y);
            };

            void    erase_node_with_two_children(iterator position) {
                NodePointer toBeDeleted = position._node;
                // replace tobedeleted with its predecessor in terms of key value
                position--;
                NodePointer prev = position._node;
                
                // set either NULL either y child where y used to be
                if (prev->_parent != toBeDeleted)
                {
                    prev->_parent->_right = prev->_left;   // y must be on the right of its parent if its parent is not x and if it has a child it must be on its left otherwise the right child would be it(x)--
                    if (prev->_left)
                        prev->_left->_parent = prev->_parent;
                    prev->_left = toBeDeleted->_left;
                    prev->_left->_parent = prev;
                }
                if (toBeDeleted->_parent->_right == toBeDeleted)
                    toBeDeleted->_parent->_right = prev;
                else if (toBeDeleted->_parent->_left == toBeDeleted)
                    toBeDeleted->_parent->_left = prev;
                else if (toBeDeleted == root())
                    root() = prev;
                prev->_parent = toBeDeleted->_parent;
                prev->_right = toBeDeleted->_right;
                prev->_right->_parent = prev;
                destroy_node(toBeDeleted);
                _node_count -= 1;
                rb_tree_delete_rebalance(prev);
            };

            void   rb_tree_delete_rebalance(NodePointer x) {
                while (x != root() && x->_color == BLACK) 
                {
                    if (x == x->_parent->_left || x->_parent->_left == NULL)
                    {
                        NodePointer w = x->_parent->_right;
                    
                        if (w->_color == RED) 
                        {
                            w->_color = BLACK;
                            x->_parent->_color = RED;
                            rotate_left(x->_parent);
                            w = (x->_parent->_right != NULL) ? x->_parent->_right : w->_parent->_left;
                        }
                        if ((w->_left == NULL || w->_left->_color == BLACK) && (w->_right == NULL || w->_right->_color == BLACK))
                        {
                            w->_color = RED;
                            x = x->_parent;
                        }
                        else 
                        {
                            if (w->_right == NULL || w->_right->_color == BLACK) 
                            {
                                if (w->_left)
                                    w->_left->_color = BLACK;
                                w->_color = RED;
                                rotate_right(w);
                                w = (x->_parent->_right != NULL) ? x->_parent->_right : w->_parent->_left;
                            }
                            w->_color = x->_parent->_color;
                            x->_parent->_color = BLACK;
                            if (w->_right)
                                w->_right->_color = BLACK;
                            rotate_left(x->_parent);
                            x = root();
                        }
                    }
                    else 
                    {
                        NodePointer w = x->_parent->_left;

                        if (w->_color == RED) 
                        {
                            w->_color = BLACK;
                            x->_parent->_color = RED;
                            rotate_right(x->_parent);
                            w = (x->_parent->_left != NULL) ? x->_parent->_left : x->_parent->_right;
                        }
                        if ((w->_right == NULL || w->_right->_color == BLACK) && (w->_left == NULL || w->_left->_color == BLACK))
                        {
                            w->_color = RED;
                            x = x->_parent;
                        }
                        else 
                        {
                            if (w->_left == NULL || w->_left->_color == BLACK) 
                            {
                                if (w->_right)
                                    w->_right->_color = BLACK;
                                w->_color = RED;
                                rotate_left(w);
                                w = (x->_parent->_left != NULL) ? x->_parent->_left : x->_parent->_right;
                            }
                            w->_color = x->_parent->_color;
                            x->_parent->_color = BLACK;
                            if (w->_left)
                                w->_left->_color = BLACK;
                            rotate_right(x->_parent);
                            x = root();
                        }
                    }
                }
                x->_color = BLACK;
            };
    };
};

#endif