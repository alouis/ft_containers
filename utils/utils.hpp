/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <user42@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/30 17:01:52 by alouis            #+#    #+#             */
/*   Updated: 2021/08/17 10:03:44 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTILS_HPP
# define UTILS_HPP

namespace ft
{
    //function object that returns first type of a pair
    template <class Pair>
    struct  select1st : std::unary_function<Pair, typename Pair::first_type>
    {
        typedef             Pair                argument_type;
        typedef typename    Pair::first_type    result_type;

        const typename  Pair::first_type&
        operator()(const Pair& p) const { return (p.first); };
    };

    template <class T>
    struct is_integral;

    template <class T, T v>
	struct integral_constant
	{
		typedef T value_type;
		typedef integral_constant<T, v> type;
		static const value_type value = v;
		operator value_type() { return v; }
	};

	typedef integral_constant<bool, true> true_type;
	typedef integral_constant<bool, false> false_type;

	// remove cont and volatile to examine if type is integral or not
	template <class T>
	struct remove_cv
	{
		typedef T type;
	};

	template <class T>
	struct remove_cv<const T>
	{
		typedef T type;
	};

	template <class T>
	struct remove_cv<volatile T>
	{
		typedef T type;
	};
    
	template <class T>
	struct remove_cv<const volatile T>
	{
		typedef T type;
	};

	// implementation of is_integral for all types that are integral
	template <typename>
	struct is_integral_base : ft::false_type
	{
	};

	template <>
	struct is_integral_base<bool> : ft::true_type
	{
	};

	template <>
	struct is_integral_base<char> : ft::true_type
	{
	};

	template <>
	struct is_integral_base<wchar_t> : ft::true_type
	{
	};

	template <>
	struct is_integral_base<signed char> : ft::true_type
	{
	};

	template <>
	struct is_integral_base<short int> : ft::true_type
	{
	};

	template <>
	struct is_integral_base<int> : ft::true_type
	{
	};

	template <>
	struct is_integral_base<long int> : ft::true_type
	{
	};

	template <>
	struct is_integral_base<long long int> : ft::true_type
	{
	};

	template <>
	struct is_integral_base<unsigned char> : ft::true_type
	{
	};

	template <>
	struct is_integral_base<unsigned short int> : ft::true_type
	{
	};
	template <>
	struct is_integral_base<unsigned int> : ft::true_type
	{
	};

	template <>
	struct is_integral_base<unsigned long int> : ft::true_type
	{
	};

	template <>
	struct is_integral_base<unsigned long long int> : ft::true_type
	{
	};

	template<class T>
	struct is_integral : is_integral_base<typename ft::remove_cv<T>::type > {};
};

#endif