/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/06 14:50:03 by alouis            #+#    #+#             */
/*   Updated: 2021/08/23 17:27:47 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vector.hpp"
#include <vector>
#include <sstream>

template<class Container>
void print_container(Container c)
{
	std::cout << "=== Container ===" << std::endl;
	if (c.size() < 30)
	{
		for (typename Container::iterator it = c.begin(); it != c.end(); it++)
			std::cout << *it << std::endl;
	}
	std::cout << "size: " <<c.size() << std::endl;
	std::cout << "======" << std::endl;
}

namespace std
{
    template <typename T>
    std::string to_string(T val)
    {
        std::stringstream stream;
        stream << val;
        return stream.str();
    }
}
void test_vector_m()
{
	std::cout << "Start" << std::endl;
	std::vector<int> sv1(6), sv2(6);
	std::vector<int>::iterator it1, it2;
	it1 = sv1.begin();
	it2 = sv2.begin();
	std::cout << it1[2] << " - " << it2[2] << " : " << (it1 == it2) << std::endl;
	std::cout << "diff: " << sv1.end() - sv1.begin() << " - " << sv1.rend() - sv1.rbegin() << std::endl;

	std::cout << "========= 1. ========" << std::endl;

	ft::vector<int> v1;
	ft::vector<int> v2(6, 8);
	v2[1] = 1;
	v2[2] = 7;
	v2[3] = 8;
	const ft::vector<int> v3(v2);
	std::cout << v2[3] << " - " << v2.at(3) << std::endl;
	std::cout << v2[2] << std::endl;
	std::cout << v3[2] << std::endl;
	std::cout << (v1 == v2) << std::endl;
	try
	{
		v2.at(10);
	}
	catch (std::out_of_range &e)
	{
		std::cout << e.what() << std::endl;
	}
	try
	{
		v3.at(10);
	}
	catch (std::out_of_range &e)
	{
		std::cout << e.what() << std::endl;
	}
	std::cout << v1.max_size() << std::endl;

	std::cout << "========= 2. =========" << std::endl;
	ft::vector<int>::iterator vt1;
	for (vt1 = v2.begin(); vt1 != v2.end(); ++vt1)
		std::cout << *vt1 << std::endl;

	std::cout << "diff: " << v2.end() - v2.begin() << " - " << v2.rend() - v2.rbegin() << std::endl;
    
	std::cout << "========= 4. =========" << std::endl;
	std::vector<int>::const_iterator cit;
	const ft::vector<int> cv(6);
	cit = sv2.begin();
	cit++;
	std::cout << cit[1] << std::endl;
	ft::vector<int>::const_iterator ft_cit(cv.begin());
	ft_cit = v2.begin();
	ft_cit++;
	std::cout << ft_cit[1] << std::endl;

	std::cout << "========= 5. =========" << std::endl;
	ft::vector<int> v4(v2.begin(), v2.end());
	for (ft::vector<int>::iterator it = v4.begin(); it != v4.end(); it++)
		std::cout << *it << std::endl;
	
	std::cout << "======== TESTS =========" << std::endl;
	std::cout << "Constructor" << std::endl;
	{
		NAMESPACE::vector<int> t1;
		for (int i = 0; i < 20; i++)
			t1.push_back(i);
		NAMESPACE::vector<int> t2;
		t2 = t1;

		NAMESPACE::vector<int> t3(t2.begin(), t2.end());
		std::cout << t1.size() << " " << t2.size() << " " << t3.size() << " " << t1.capacity() << " " << t2.capacity() << " " << t3.capacity() << std::endl;
		print_container(t1);
		print_container(t2);
		print_container(t3);
	}

	NAMESPACE::vector<int> vect;
	std::cout << "get_allocator()" << std::endl;
	std::cout << vect.get_allocator().max_size() << std::endl;
	for (int i = 0; i < 20; i++)
		vect.push_back(i);
	NAMESPACE::vector<int>::iterator iter = vect.begin();
	NAMESPACE::vector<int>::reverse_iterator riter = vect.rbegin();

	std::cout << "Iter" << std::endl;
	for (; iter != vect.end(); iter++)
		*iter = *iter + (iter - vect.begin());
	std::cout << (vect.begin() < vect.end()) << std::endl;
	std::cout << (vect.begin() <= vect.end()) << std::endl;
	std::cout << (vect.begin() >= vect.end()) << std::endl;
	std::cout << (vect.begin() > vect.end()) << std::endl;
	std::cout << (vect.begin() == vect.end()) << std::endl;
	std::cout << (vect.begin() != vect.end()) << std::endl;
	std::cout << (vect.begin() + 6)[2] << std::endl;
	std::cout << (vect.end() - 3)[1] << std::endl;
	std::cout << (vect.end() - vect.begin()) << std::endl;

	std::cout << "Riter" << std::endl;
	for (; riter != vect.rend(); riter++)
		std::cout << *riter << std::endl;
	std::cout << (vect.rbegin() < vect.rend()) << std::endl;
	std::cout << (vect.rbegin() <= vect.rend()) << std::endl;
	std::cout << (vect.rbegin() >= vect.rend()) << std::endl;
	std::cout << (vect.rbegin() > vect.rend()) << std::endl;
	std::cout << (vect.rbegin() == vect.rend()) << std::endl;
	std::cout << (vect.rbegin() != vect.rend()) << std::endl;


	std::cout << "front()" << std::endl;
	{
		std::cout << vect.front() << std::endl;

		const int i = vect.front();
		std::cout << i << std::endl;
	}
	std::cout << "back()" << std::endl;
	{
		std::cout << vect.back() << std::endl;

		const int i = vect.back();
		std::cout << i << std::endl;
	}
	std::cout << "[] and at()" << std::endl;
	{
		std::cout << vect[4] << vect.at(4) << std::endl;

		const int i = vect[3];
		const int j = vect.at(3);
		try
		{
			vect.at(1000);
		}
		catch (std::out_of_range &e)
		{
			std::cout << "exception" << std::endl;
		}
		std::cout << i << j << std::endl;
	}
	std::cout << "empty()" << std::endl;
	{
		std::cout << vect.empty() << std::endl;
		NAMESPACE::vector<int> e;
		std::cout << e.empty() << std::endl;
		std::cout << vect.capacity() << vect.size() << std::endl;
	}
	std::cout << "reserve()" << std::endl;
	{
		vect.reserve(100);
		std::cout << vect.capacity() << vect.size() << std::endl;
	}
	std::cout << "resize()" << std::endl;
	{
		vect.resize(3);
		std::cout << vect.capacity() << vect.size() << std::endl;
		print_container(vect);
	}
	std::cout << "Maxsize" << std::endl;
	{
		std::cout << vect.max_size() << std::endl;
	}
	std::cout << "push_back()" << std::endl;
	{
		vect.push_back(100);
		vect.push_back(-8);
		print_container(vect);
	}
	std::cout << "pop_back()" << std::endl;
	{
		vect.pop_back();
		print_container(vect);
	}
	std::cout << "clear" << std::endl;
	//from cplusplus
	{
		NAMESPACE::vector<int> myvector;
		myvector.push_back(100);
		myvector.push_back(200);
		myvector.push_back(300);

		std::cout << "myvector contains:";
		for (unsigned i = 0; i < myvector.size(); i++)
			std::cout << ' ' << myvector[i];
		std::cout << '\n';

		myvector.clear();
		myvector.push_back(1101);
		myvector.push_back(2202);

		std::cout << "myvector contains:";
		for (unsigned i = 0; i < myvector.size(); i++)
			std::cout << ' ' << myvector[i];
		std::cout << '\n';
	}

	std::cout << "assign" << std::endl;
	{
		vect.assign(2, 50);
		print_container(vect);
		vect.assign(200, 6);
		print_container(vect);
		NAMESPACE::vector<int> count(25);
		for (NAMESPACE::vector<int>::iterator it = count.begin(); it != count.end(); it++)
			*it = (it - count.begin());
		vect.assign(vect.begin(), vect.end());
		print_container(vect);
		print_container(count);
	}
	//cplusplus.com
	{
		NAMESPACE::vector<int> first;
		NAMESPACE::vector<int> second;
		first.assign(7, 100); // 7 ints with a value of 100
		NAMESPACE::vector<int>::iterator it;
		it = first.begin() + 1;
		second.assign(it, first.end() - 1); // the 5 central values of first
		std::cout << "Size of first: " << int(first.size()) << '\n';
		std::cout << "Size of second: " << int(second.size()) << '\n';
	}

	std::cout << "Erase" << std::endl;
	{
		vect.clear();
		for (int i = 0; i < 10; i++)
			vect.push_back(i);
		print_container(vect);
		std::cout << "-" << std::endl;
		NAMESPACE::vector<int>::iterator it = vect.erase(vect.begin() + 2);
		std::cout << *it << std::endl;
		print_container(vect);
		it = vect.erase(vect.end() - 1);
		std::cout << *it << std::endl;
		print_container(vect);
	}
	{
		std::cout << "Erase2" << std::endl;
		vect.clear();
		for (int i = 0; i < 20; i++)
			vect.push_back(i);
		NAMESPACE::vector<int>::iterator it = vect.erase(vect.begin() + 2, vect.begin() + 8);
		std::cout << *it << std::endl;
		print_container(vect);
		vect.clear();
		for (int i = 0; i < 20; i++)
			vect.push_back(i);
		it = vect.erase(vect.begin() + 6, vect.end());
		std::cout << *it << std::endl;
		print_container(vect);
	}
	std::cout << "Swap" << std::endl;
	{
		NAMESPACE::vector<int> foo(3, 100); // three ints with a value of 100
		NAMESPACE::vector<int> bar(5, 200); // five ints with a value of 200

		foo.swap(bar);

		std::cout << "foo contains:";
		for (unsigned i = 0; i < foo.size(); i++)
			std::cout << ' ' << foo[i];
		std::cout << '\n';

		std::cout << "bar contains:";
		for (unsigned i = 0; i < bar.size(); i++)
			std::cout << ' ' << bar[i];
		std::cout << '\n';

		swap(bar, foo);
		print_container(bar);
		print_container(foo);
	}

	NAMESPACE::vector<int>::reverse_iterator rit = vect.rend() - 1;
	std::cout << *rit << std::endl;

	std::cout << "Insert" << std::endl;
	{
		//from cppreference
		NAMESPACE::vector<int> myvector(3, 100);
		NAMESPACE::vector<int>::iterator it;
		print_container(myvector);
		std::cout << "insert1" << std::endl;
		it = myvector.begin();
		it = myvector.insert(it, 200);
		print_container(myvector);

		std::cout << "insert2" << std::endl;
		it = myvector.begin();
		myvector.insert(it, 2, 300);
		print_container(myvector);

		// "it" no longer valid, get a new one:
		std::cout << "insert3" << std::endl;
		it = myvector.begin();
		NAMESPACE::vector<int> anothervector(2, 400);
		myvector.insert(it + 2, anothervector.begin(), anothervector.end());

		std::cout << "myvector contains:";
		for (it = myvector.begin(); it < myvector.end(); it++)
			std::cout << ' ' << *it;
		std::cout << '\n';
	}

	std::cout << "compare" << std::endl;
	{
		NAMESPACE::vector<int> v1(2, 10);
		NAMESPACE::vector<int> v2(2, 10);

		std::cout << (v1 == v2) << std::endl;
		std::cout << (v1 != v2) << std::endl;
		std::cout << (v1 >= v2) << std::endl;
		std::cout << (v1 <= v2) << std::endl;
		std::cout << (v1 < v2) << std::endl;
		std::cout << (v1 > v2) << std::endl;
	}
	{
		NAMESPACE::vector<int> v1(2, 10);
		NAMESPACE::vector<int> v2(3, 10);

		std::cout << (v1 == v2) << std::endl;
		std::cout << (v1 != v2) << std::endl;
		std::cout << (v1 >= v2) << std::endl;
		std::cout << (v1 <= v2) << std::endl;
		std::cout << (v1 < v2) << std::endl;
		std::cout << (v1 > v2) << std::endl;
	}
	{
		NAMESPACE::vector<int> v1(2, 9);
		NAMESPACE::vector<int> v2(2, 10);

		std::cout << (v1 == v2) << std::endl;
		std::cout << (v1 != v2) << std::endl;
		std::cout << (v1 >= v2) << std::endl;
		std::cout << (v1 <= v2) << std::endl;
		std::cout << (v1 < v2) << std::endl;
		std::cout << (v1 > v2) << std::endl;
	}
	{
		NAMESPACE::vector<int> v1;
		NAMESPACE::vector<int> v2(v1);

		std::cout << (v1 == v2) << std::endl;
		std::cout << (v1 != v2) << std::endl;
		std::cout << (v1 >= v2) << std::endl;
		std::cout << (v1 <= v2) << std::endl;
		std::cout << (v1 < v2) << std::endl;
		std::cout << (v1 > v2) << std::endl;
	}
	{
		NAMESPACE::vector<int> v1;
		NAMESPACE::vector<int> v2;

		std::cout << (v1 == v2) << std::endl;
		std::cout << (v1 != v2) << std::endl;
		std::cout << (v1 >= v2) << std::endl;
		std::cout << (v1 <= v2) << std::endl;
		std::cout << (v1 < v2) << std::endl;
		std::cout << (v1 > v2) << std::endl;
	}
	std::cout << "=============WITH STRING =================" << std::endl;
	{
		std::cout << "Start" << std::endl;
		std::vector<std::string> sv1(6), sv2(6);
		std::vector<std::string>::iterator it1, it2;
		it1 = sv1.begin();
		it2 = sv2.begin();
		std::cout << it1[2] << " - " << it2[2] << " : " << (it1 == it2) << std::endl;
		std::cout << "diff: " << sv1.end() - sv1.begin() << " - " << sv1.rend() - sv1.rbegin() << std::endl;

		std::cout << "========= 1. ========" << std::endl;

		ft::vector<std::string> v1;
		ft::vector<std::string> v2(6, "hello");
		v2[1] = "one";
		v2[2] = 100;
		v2[3] = std::string("tri");
		const ft::vector<std::string> v3(v2);
		std::cout << v2[3] << " - " << v2.at(3) << std::endl;
		std::cout << v2[2] << std::endl;
		std::cout << v3[2] << std::endl;
		std::cout << (v1 == v2) << std::endl;
		try
		{
			v2.at(10);
		}
		catch (std::out_of_range &e)
		{
			std::cout << e.what() << std::endl;
		}
		try
		{
			v3.at(10);
		}
		catch (std::out_of_range &e)
		{
			std::cout << e.what() << std::endl;
		}
		std::cout << v1.max_size() << std::endl;
		std::cout << "========= 2. =========" << std::endl;
		ft::vector<std::string>::iterator vt1;
		for (vt1 = v2.begin(); vt1 != v2.end(); ++vt1)
			std::cout << *vt1 << std::endl;

		std::cout << "diff: " << v2.end() - v2.begin() << " - " << v2.rend() - v2.rbegin() << std::endl;
        
		std::cout << "========= 4. =========" << std::endl;
		std::vector<std::string>::const_iterator cit;
		const ft::vector<std::string> cv(6);
		cit = sv2.begin();
		cit++;
		std::cout << cit[1] << std::endl;
		ft::vector<std::string>::const_iterator ft_cit(cv.begin());
		ft_cit = v2.begin();
		ft_cit++;
		std::cout << ft_cit[1] << std::endl;

		std::cout << "========= 5. =========" << std::endl;
		for (ft::vector<std::string>::iterator it = v2.begin(); it != v2.end(); it++)
			std::cout << *it << std::endl;
		ft::vector<std::string> v4(v2.begin(), v2.end());
		std::cout << "go" << std::endl;
		for (ft::vector<std::string>::iterator it = v4.begin(); it != v4.end(); it++)
			std::cout << *it << std::endl;
	}
	{

		std::cout << "Constructor" << std::endl;
		{
			NAMESPACE::vector<std::string> t1;
		}
		{
			NAMESPACE::vector<std::string> t1;
			for (int i = 0; i < 20; i++)
				t1.push_back("yo" + std::to_string(i));
			NAMESPACE::vector<std::string> t2;
			t2 = t1;

			NAMESPACE::vector<std::string> t3(t2.begin(), t2.end());
			std::cout << t1.size() << t2.size() << t3.size() << t1.capacity() << t2.capacity() << t3.capacity() << std::endl;
			print_container(t1);
			print_container(t2);
			print_container(t3);
		}

		NAMESPACE::vector<std::string> vect;
		std::cout << "get_allocator()" << std::endl;
		std::cout << vect.get_allocator().max_size() << std::endl;
		for (int i = 0; i < 20; i++)
			vect.push_back("yo" + std::to_string(i));
		NAMESPACE::vector<std::string>::iterator iter = vect.begin();

		std::cout << "Iter" << std::endl;
		for (; iter != vect.end(); iter++)
			*iter = *iter + std::to_string(iter - vect.begin());
		std::cout << (vect.begin() < vect.end()) << std::endl;
		std::cout << (vect.begin() <= vect.end()) << std::endl;
		std::cout << (vect.begin() >= vect.end()) << std::endl;
		std::cout << (vect.begin() > vect.end()) << std::endl;
		std::cout << (vect.begin() == vect.end()) << std::endl;
		std::cout << (vect.begin() != vect.end()) << std::endl;
		std::cout << (vect.begin() + 6)[2] << std::endl;
		std::cout << (vect.end() - 3)[1] << std::endl;
		std::cout << (vect.end() - vect.begin()) << std::endl;
		
		NAMESPACE::vector<std::string>::reverse_iterator riter = vect.rbegin();
		std::cout << "Riter" << std::endl;
		for (; riter != vect.rend(); riter++)
			std::cout << *riter << std::endl;
		std::cout << (vect.rbegin() < vect.rend()) << std::endl;
		std::cout << (vect.rbegin() <= vect.rend()) << std::endl;
		std::cout << (vect.rbegin() >= vect.rend()) << std::endl;
		std::cout << (vect.rbegin() > vect.rend()) << std::endl;
		std::cout << (vect.rbegin() == vect.rend()) << std::endl;
		std::cout << (vect.rbegin() != vect.rend()) << std::endl;
		std::cout << (vect.rend() - vect.rbegin()) << std::endl;

		std::cout << "front()" << std::endl;
		{
			std::cout << vect.front() << std::endl;

			const std::string i = vect.front();
			std::cout << i << std::endl;
		}
		std::cout << "back()" << std::endl;
		{
			std::cout << vect.back() << std::endl;

			const std::string i = vect.back();
			std::cout << i << std::endl;
		}
		std::cout << "[] and at()" << std::endl;
		{
			std::cout << vect[4] << vect.at(4) << std::endl;

			const std::string i = vect[3];
			const std::string j = vect.at(3);
			try
			{
				vect.at(1000);
			}
			catch (std::out_of_range &e)
			{
				std::cout << "exception" << std::endl;
			}
			std::cout << i << j << std::endl;
		}
		std::cout << "empty()" << std::endl;
		{
			std::cout << vect.empty() << std::endl;
			NAMESPACE::vector<std::string> e;
			std::cout << e.empty() << std::endl;
			std::cout << vect.capacity() << vect.size() << std::endl;
		}
		std::cout << "reserve()" << std::endl;
		{
			vect.reserve(100);
			std::cout << vect.capacity() << vect.size() << std::endl;
		}
		std::cout << "resize()" << std::endl;
		{
			vect.resize(3);
			std::cout << vect.capacity() << vect.size() << std::endl;
			print_container(vect);
		}
		std::cout << "Maxsize" << std::endl;
		{
			std::cout << vect.max_size() << std::endl;
		}
		std::cout << "push_back()" << std::endl;
		{
			vect.push_back(std::to_string(100));
			vect.push_back(std::to_string(-8));
			print_container(vect);
		}
		std::cout << "pop_back()" << std::endl;
		{
			vect.pop_back();
			print_container(vect);
		}
		std::cout << "clear" << std::endl;
		//from cplusplus
		{
			NAMESPACE::vector<std::string> myvector;
			myvector.push_back(std::to_string(100));
			myvector.push_back(std::to_string(200));
			myvector.push_back(std::to_string(300));

			std::cout << "myvector contains:";
			for (unsigned i = 0; i < myvector.size(); i++)
				std::cout << ' ' << myvector[i];
			std::cout << '\n';

			myvector.clear();
			myvector.push_back(std::to_string(1101));
			myvector.push_back(std::to_string(2202));

			std::cout << "myvector contains:";
			for (unsigned i = 0; i < myvector.size(); i++)
				std::cout << ' ' << myvector[i];
			std::cout << '\n';
		}
		std::cout << "assign" << std::endl;
		{
			NAMESPACE::vector<std::string> vect0;;
			vect.assign(2, "haha");
			print_container(vect);
			vect.assign(200, "yo");
			print_container(vect);
			NAMESPACE::vector<std::string> count(25);
			for (NAMESPACE::vector<std::string>::iterator it = count.begin(); it != count.end(); it++)
				*it = (it - count.begin());
			std::cout << "aie" << std::endl;
			vect0.assign(vect.begin(), vect.end());
			print_container(vect0);
			print_container(count);
		}
		{
			NAMESPACE::vector<std::string> first;
			NAMESPACE::vector<std::string> second;
			first.assign(7, std::to_string(100)); // 7 ints with a value of 100
			NAMESPACE::vector<std::string>::iterator it;
			it = first.begin() + 1;
			second.assign(it, first.end() - 1); // the 5 central values of first
			std::cout << "Size of first: " << int(first.size()) << '\n';
			std::cout << "Size of second: " << int(second.size()) << '\n';
		}

		std::cout << "Erase" << std::endl;
		{
			vect.clear();
			for (int i = 0; i < 10; i++)
				vect.push_back("yay" + std::to_string(i));
			print_container(vect);
			std::cout << "-" << std::endl;
			NAMESPACE::vector<std::string>::iterator it = vect.erase(vect.begin() + 2);
			std::cout << *it << std::endl;
			print_container(vect);
			it = vect.erase(vect.end() - 1);
			std::cout << *it << std::endl;
			print_container(vect);
		}
		{
			std::cout << "Erase2" << std::endl;
			vect.clear();
			for (int i = 0; i < 20; i++)
				vect.push_back("yay" + std::to_string(i));
			NAMESPACE::vector<std::string>::iterator it = vect.erase(vect.begin() + 2, vect.begin() + 8);
			std::cout << *it << std::endl;
			print_container(vect);
			vect.clear();
			for (int i = 0; i < 20; i++)
				vect.push_back("yay" + std::to_string(i));
			it = vect.erase(vect.begin() + 6, vect.end());
			std::cout << *it << std::endl;
			print_container(vect);
		}
		std::cout << "Swap" << std::endl;
		{
			NAMESPACE::vector<std::string> foo(3, "100"); // three ints with a value of 100
			NAMESPACE::vector<std::string> bar(5, "200"); // five ints with a value of 200

			foo.swap(bar);

			std::cout << "foo contains:";
			for (unsigned i = 0; i < foo.size(); i++)
				std::cout << ' ' << foo[i];
			std::cout << '\n';

			std::cout << "bar contains:";
			for (unsigned i = 0; i < bar.size(); i++)
				std::cout << ' ' << bar[i];
			std::cout << '\n';

			swap(bar, foo);
			print_container(bar);
			print_container(foo);
		}
		NAMESPACE::vector<std::string>::reverse_iterator rit = vect.rend() - 1;
		std::cout << *rit << std::endl;

		std::cout << "Insert" << std::endl;
		{
			//from cppreference
			NAMESPACE::vector<std::string> myvector(3, "100");
			NAMESPACE::vector<std::string>::iterator it;
			print_container(myvector);
			std::cout << "insert1" << std::endl;
			it = myvector.begin();
			it = myvector.insert(it, "200");
			print_container(myvector);

			std::cout << "insert2" << std::endl;
			it = myvector.begin();
			myvector.insert(it, 2, "300");
			print_container(myvector);

			// "it" no longer valid, get a new one:
			std::cout << "insert3" << std::endl;
			it = myvector.begin();
			NAMESPACE::vector<std::string> anothervector(2, "400");
			myvector.insert(it + 2, anothervector.begin(), anothervector.end());

			std::cout << "operator ->" << std::endl;
			std::cout <<  myvector.begin()->size() << std::endl;
		}
		std::cout << "compare" << std::endl;
		{
		NAMESPACE::vector<std::string> v1(2, "10");
		NAMESPACE::vector<std::string> v2(2, "10");

		std::cout << (v1 == v2) << std::endl;
		std::cout << (v1 != v2) << std::endl;
		std::cout << (v1 >= v2) << std::endl;
		std::cout << (v1 <= v2) << std::endl;
		std::cout << (v1 < v2) << std::endl;
		std::cout << (v1 > v2) << std::endl;
		}
		{
		NAMESPACE::vector<std::string> v1(2, "10");
		NAMESPACE::vector<std::string> v2(3, "10");

		std::cout << (v1 == v2) << std::endl;
		std::cout << (v1 != v2) << std::endl;
		std::cout << (v1 >= v2) << std::endl;
		std::cout << (v1 <= v2) << std::endl;
		std::cout << (v1 < v2) << std::endl;
		std::cout << (v1 > v2) << std::endl;
		}
		{
		NAMESPACE::vector<std::string> v1(2, "9");
		NAMESPACE::vector<std::string> v2(2, "10");

		std::cout << (v1 == v2) << std::endl;
		std::cout << (v1 != v2) << std::endl;
		std::cout << (v1 >= v2) << std::endl;
		std::cout << (v1 <= v2) << std::endl;
		std::cout << (v1 < v2) << std::endl;
		std::cout << (v1 > v2) << std::endl;
		}
		{
		NAMESPACE::vector<std::string> v1;
		NAMESPACE::vector<std::string> v2(v1);

		std::cout << (v1 == v2) << std::endl;
		std::cout << (v1 != v2) << std::endl;
		std::cout << (v1 >= v2) << std::endl;
		std::cout << (v1 <= v2) << std::endl;
		std::cout << (v1 < v2) << std::endl;
		std::cout << (v1 > v2) << std::endl;
		}
		{
		NAMESPACE::vector<std::string> v1;
		NAMESPACE::vector<std::string> v2;

		std::cout << (v1 == v2) << std::endl;
		std::cout << (v1 != v2) << std::endl;
		std::cout << (v1 >= v2) << std::endl;
		std::cout << (v1 <= v2) << std::endl;
		std::cout << (v1 < v2) << std::endl;
		std::cout << (v1 > v2) << std::endl;
		}
	}
	{
		std::cout << "======== TESTS CONST =========" << std::endl;
		NAMESPACE::vector<int> t;
		for (int i = 0; i < 20; i++)
			t.push_back(i);

		std::cout << "t: " << t.capacity() << std::endl;
		const NAMESPACE::vector<int> vect(t);

		std::cout << (vect.begin() < vect.end()) << std::endl;
		std::cout << (vect.begin() <= vect.end()) << std::endl;
		std::cout << (vect.begin() >= vect.end()) << std::endl;
		std::cout << (vect.begin() > vect.end()) << std::endl;
		std::cout << (vect.begin() == vect.end()) << std::endl;
		std::cout << (vect.begin() != vect.end()) << std::endl;
		std::cout << (vect.begin() + 6)[2] << std::endl;
		std::cout << (vect.end() - 3)[1] << std::endl;
		std::cout << (vect.end() - vect.begin()) << std::endl;
		
		std::cout << "Riter" << std::endl;
		NAMESPACE::vector<const int>::const_reverse_iterator riter;
		for (riter = vect.rbegin(); riter != vect.rend(); riter++)
			std::cout << *riter << std::endl;
		riter = vect.rend();
		riter -= 3;
		std::cout << *riter << std::endl;
		riter += 1;
		std::cout << *riter << std::endl;
		std::cout << (vect.rbegin() < vect.rend()) << std::endl;
		std::cout << (vect.rbegin() <= vect.rend()) << std::endl;
		std::cout << (vect.rbegin() >= vect.rend()) << std::endl;
		std::cout << (vect.rbegin() > vect.rend()) << std::endl;
		std::cout << (vect.rbegin() == vect.rend()) << std::endl;
		std::cout << (vect.rbegin() != vect.rend()) << std::endl;

		std::cout << (vect.rend() - vect.rbegin()) << std::endl;
		
		std::cout << "front()" << std::endl;
		{
			std::cout << vect.front() << std::endl;

			const int i = vect.front();
			std::cout << i << std::endl;
		}
		std::cout << "back()" << std::endl;
		{
			std::cout << vect.back() << std::endl;

			const int i = vect.back();
			std::cout << i << std::endl;
		}
		std::cout << "[] and at()" << std::endl;
		{
			std::cout << vect[4] << vect.at(4) << std::endl;

			const int i = vect[3];
			const int j = vect.at(3);
			try
			{
				vect.at(1000);
			}
			catch (std::out_of_range &e)
			{
				std::cout << "exception" << std::endl;
			}
			std::cout << i << j << std::endl;
			print_container(vect);
		}
		std::cout << "empty()" << std::endl;
		{
			std::cout << vect.empty() << std::endl;
			NAMESPACE::vector<int> e;
			std::cout << e.empty() << std::endl;
			std::cout << vect.capacity() << vect.size() << std::endl;
		}

		std::cout << "Maxsize" << std::endl;
		{
			std::cout << vect.max_size() << std::endl;
		}
		std::cout << "compare" << std::endl;
		{
			const NAMESPACE::vector<int> v1(2, 10);
			const NAMESPACE::vector<int> v2(2, 10);

			std::cout << (v1 == v2) << std::endl;
			std::cout << (v1 != v2) << std::endl;
			std::cout << (v1 >= v2) << std::endl;
			std::cout << (v1 <= v2) << std::endl;
			std::cout << (v1 < v2) << std::endl;
			std::cout << (v1 > v2) << std::endl;
		}
		{
			const NAMESPACE::vector<int> v1(2, 10);
			const NAMESPACE::vector<int> v2(3, 10);

			std::cout << (v1 == v2) << std::endl;
			std::cout << (v1 != v2) << std::endl;
			std::cout << (v1 >= v2) << std::endl;
			std::cout << (v1 <= v2) << std::endl;
			std::cout << (v1 < v2) << std::endl;
			std::cout << (v1 > v2) << std::endl;
		}
		{
			const NAMESPACE::vector<int> v1(2, 9);
			const NAMESPACE::vector<int> v2(2, 10);

			std::cout << (v1 == v2) << std::endl;
			std::cout << (v1 != v2) << std::endl;
			std::cout << (v1 >= v2) << std::endl;
			std::cout << (v1 <= v2) << std::endl;
			std::cout << (v1 < v2) << std::endl;
			std::cout << (v1 > v2) << std::endl;
		}
		{
			const NAMESPACE::vector<int> v1;
			const NAMESPACE::vector<int> v2(v1);

			std::cout << (v1 == v2) << std::endl;
			std::cout << (v1 != v2) << std::endl;
			std::cout << (v1 >= v2) << std::endl;
			std::cout << (v1 <= v2) << std::endl;
			std::cout << (v1 < v2) << std::endl;
			std::cout << (v1 > v2) << std::endl;
		}
		{
			const NAMESPACE::vector<int> v1;
			const NAMESPACE::vector<int> v2;

			std::cout << (v1 == v2) << std::endl;
			std::cout << (v1 != v2) << std::endl;
			std::cout << (v1 >= v2) << std::endl;
			std::cout << (v1 <= v2) << std::endl;
			std::cout << (v1 < v2) << std::endl;
			std::cout << (v1 > v2) << std::endl;
		}

		{
			const NAMESPACE::vector<int> v1(2, 9);
			NAMESPACE::vector<int> v2(2, 10);

			std::cout << (v1 == v2) << std::endl;
			std::cout << (v1 != v2) << std::endl;
			std::cout << (v1 >= v2) << std::endl;
			std::cout << (v1 <= v2) << std::endl;
			std::cout << (v1 < v2) << std::endl;
			std::cout << (v1 > v2) << std::endl;
		}
	}
}

int    test_vector_a(void)
{
    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||          Constructors          ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
     
    NAMESPACE::vector<int>            first;
    NAMESPACE::vector<int>            second (4,100);
    NAMESPACE::vector<int>            third (second.begin(),second.end());
    NAMESPACE::vector<int>            fourth (third);
    NAMESPACE::vector<int> const      constVector(5, 27);
    NAMESPACE::vector<double>         dbl(1, 890.00);
    NAMESPACE::vector<float>          flt(4, 3.5673);
    NAMESPACE::vector<std::string>    str(5, "Hello world!");
    NAMESPACE::vector<std::string>    str2;
    NAMESPACE::vector<char>           chr(10, 'c');
    
    NAMESPACE::vector<int>::const_iterator citer(second.begin());
    
    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||           operator=()          ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    NAMESPACE::vector<int> foo (3,0);
    NAMESPACE::vector<int> bar (5,0);

    bar = foo;
    foo = NAMESPACE::vector<int>();

    str2 = str;

    std::cout << "Size of foo: " << int(foo.size()) << '\n';
    std::cout << "Size of bar: " << int(bar.size()) << '\n';
    std::cout << "Size of str2: " << int(str2.size()) << " /!\\ IN PROGRESS /!\\" << '\n';


    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||        begin() - end()         ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;   
    std::cout << "The contents of first are:\n";
    for (NAMESPACE::vector<int>::iterator it = first.begin(); it != first.end(); ++it)
        std::cout << ' ' << *it;
    std::cout << '\n';

    std::cout << "The contents of second are:\n";
    for (NAMESPACE::vector<int>::iterator it = second.begin(); it != second.end(); ++it)
        std::cout << ' ' << *it;
    std::cout << '\n';

    std::cout << "The contents of third are:\n";
    for (NAMESPACE::vector<int>::iterator it = third.begin(); it != third.end(); ++it)
        std::cout << ' ' << *it;
    std::cout << '\n';

    std::cout << "The contents of fourth are:\n";
    for (NAMESPACE::vector<int>::iterator it = fourth.begin(); it != fourth.end(); ++it)
        std::cout << ' ' << *it;
    std::cout << '\n';

    std::cout << "The contents of dbl are:\n";
    for (NAMESPACE::vector<double>::iterator it = dbl.begin(); it != dbl.end(); ++it)
        std::cout << ' ' << *it;
    std::cout << '\n';

    std::cout << "The contents of flt are:\n";
    for (NAMESPACE::vector<float>::iterator it = flt.begin(); it != flt.end(); ++it)
        std::cout << ' ' << *it;
    std::cout << '\n';

    std::cout << "The contents of str are:\n";
    for (NAMESPACE::vector<std::string>::iterator it = str.begin(); it != str.end(); ++it)
        std::cout << ' ' << *it;
    std::cout << '\n';

    std::cout << "The contents of str2 are:\n";
    for (NAMESPACE::vector<std::string>::iterator it = str2.begin(); it != str2.end(); ++it)
        std::cout << ' ' << *it;
    std::cout << '\n';

    std::cout << "The contents of chr are:\n";
    for (NAMESPACE::vector<char>::iterator it = chr.begin(); it != chr.end(); ++it)
        std::cout << ' ' << *it;
    std::cout << '\n';

    std::cout << "The contents of constVector are:\n";
    for (NAMESPACE::vector<int>::const_iterator it = constVector.begin(); it != constVector.end(); ++it)
        std::cout << ' ' << *it;
    std::cout << '\n';      

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||       size() - max_size()      ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    std::cout << "1st size: " << first.size() << "\nmax_size: " << first.max_size() << std::endl;
    std::cout << "2nd size: " << second.size() << "\nmax_size: " << second.max_size() << std::endl;
    std::cout << "3rd size: " << third.size() << "\nmax_size: " << third.max_size() << std::endl;
    std::cout << "4th size: " << fourth.size() << "\nmax_size: " << fourth.max_size() << std::endl;
    std::cout << "const size: " << constVector.size() << "\nmax_size: " << constVector.max_size() << std::endl;
    std::cout << "dbl size: " << dbl.size() << "\nmax_size: " << dbl.max_size() << std::endl;
    std::cout << "flt size: " << flt.size() << "\nmax_size: " << flt.max_size() << std::endl;
    std::cout << "str size: " << str.size() << "\nmax_size: " << str.max_size() << std::endl;

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||             iterator           ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    NAMESPACE::vector<int>::const_iterator      cit;
    NAMESPACE::vector<int>::iterator            it;
    
    cit = second.begin(); // /!\ should work, viable overloaded '=' needed
    it = second.begin();
    std::cout << *cit << " and " << *it << std::endl;
    cit = constVector.begin();
    // it = constVector.begin(); => error: no viable overloaded '='
    std::cout << *cit << " and " << *it << std::endl;
    cit = it;
    std::cout << *cit << " and " << *it << std::endl;

    NAMESPACE::vector<std::string>::const_iterator      str_cit;
    NAMESPACE::vector<std::string>::iterator            str_it;
    NAMESPACE::vector<std::string>  const               constStr(3, "const");
    str_cit = str.begin(); // /!\ should work, viable overloaded '=' needed
    str_it = str.begin();
    std::cout << *str_cit << " and " << *str_it << std::endl;
    str_cit = constStr.begin();
    // it = constVector.begin(); => error: no viable overloaded '='
    std::cout << *str_cit << " and " << *str_it << std::endl;
    str_cit = str_it;
    std::cout << *str_cit << " and " << *str_it << std::endl;
    
    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||           pop_back()           ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    std::cout << "Before poping back some elements of vector:\n";
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << "After popping back some elements of vector:\n";
    for (unsigned long i = (second.size() / 2); i <= second.size(); ++i)
        second.pop_back();
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << std::endl;

    
    std::cout << "Before poping back some elements of string vector:\n";
    std::cout << "size = " << str.size() << " - capacity = " << str.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = str.begin(); iter != str.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << "After popping back some elements of vector:\n";
    for (unsigned long i = (str.size() / 2); i <= str.size(); ++i)
        str.pop_back();
    std::cout << "size = " << str.size() << " - capacity = " << str.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = str.begin(); iter != str.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||            resize()            ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    std::cout << "Before requesting a change of size:\n";
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << "After requesting a change of size to 1:\n";
    second.resize(1);
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << "After requesting a change of size to 20:\n";
    second.resize(20);
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << "After requesting a change of size to 21:\n";
    second.resize(21);
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << std::endl;

    std::cout << "Before requesting a change of size:\n";
    std::cout << "size = " << str.size() << " - capacity = " << str.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = str.begin(); iter != str.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    str.resize(1);
    std::cout << "After requesting a change of size to 1:\n";
    std::cout << "size = " << str.size() << " - capacity = " << str.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = str.begin(); iter != str.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    str.resize(20);
    std::cout << "After requesting a change of size to 20:\n";
    std::cout << "size = " << str.size() << " - capacity = " << str.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = str.begin(); iter != str.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    str.resize(21);
    std::cout << "After requesting a change of size to 21:\n";
    std::cout << "size = " << str.size() << " - capacity = " << str.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = str.begin(); iter != str.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||          push_back()           ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    std::cout << "Before pushing back new value:\n";
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << "After pushing back new value:\n";
    second.push_back(27);
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << "After pushing back new value:\n";
    second.push_back(33);
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << std::endl;

    std::cout << "Before pushing back new string:\n";
    std::cout << "size = " << str.size() << " - capacity = " << str.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = str.begin(); iter != str.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << "After pushing back new string:\n";
    str.push_back("friends");
    std::cout << "size = " << str.size() << " - capacity = " << str.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = str.begin(); iter != str.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << "After pushing back new string:\n";
    str.push_back("and blroks");
    std::cout << "size = " << str.size() << " - capacity = " << str.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = str.begin(); iter != str.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << std::endl;

    NAMESPACE::vector<std::string>  emptyStr3;
    std::cout << "Before pushing back new string in empty vector:\n";
    std::cout << "size = " << emptyStr3.size() << " - capacity = " << emptyStr3.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = emptyStr3.begin(); iter != emptyStr3.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    emptyStr3.push_back("friends");
    std::cout << "After pushing back new string:\n";
    std::cout << "size = " << emptyStr3.size() << " - capacity = " << emptyStr3.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = emptyStr3.begin(); iter != emptyStr3.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << std::endl;
    
    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||           reserve()            ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    std::cout << "Before requesting a change of capacity:\n";
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << "After requesting a change of capacity to 65:\n";
    second.reserve(65);
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << "After requesting a change of capacity to 100:\n";
    second.reserve(100);
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << "Trying requesting more capacity than NAMESPACE::vector::max_size:\n";
    try {
        second.reserve(second.max_size() + 1);
    }
    catch (std::length_error& err) {
        std::cout << err.what() << std::endl;
    }
    std::cout << std::endl;

    std::cout << "Before requesting a change of capacity:\n";
    std::cout << "size = " << str.size() << " - capacity = " << str.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = str.begin(); iter != str.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << "After requesting a change of capacity to 65:\n";
    str.reserve(65);
    std::cout << "size = " << str.size() << " - capacity = " << str.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = str.begin(); iter != str.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << "After requesting a change of capacity to 100:\n";
    str.reserve(100);
    std::cout << "size = " << str.size() << " - capacity = " << str.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = str.begin(); iter != str.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << "Trying requesting more capacity than NAMESPACE::vector::max_size:\n";
    try {
        str.reserve(str.max_size() + 1);
    }
    catch (std::length_error& err) {
        std::cout << err.what() << std::endl;
    }

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||            assign()            ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    std::cout << "Before assigning new content:\n";
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    second.assign(6, 100);
    std::cout << "After assigning 6 int of value 100:\n";
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    second.assign(99, 1);
    std::cout << "After assigning 99 int of value 1:\n";
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    second.assign(110, 1);
    std::cout << "After assigning 110 int of value 1:\n";
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    second.assign(2, 1);
    std::cout << "After assigning 2 int of value 1:\n";
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << std::endl;
    
    second.push_back(10);
    second.push_back(5);
    second.push_back(2);
    std::cout << "* assign(InputIterator first, InputIterator last)" << std::endl;
    std::cout << "Before range built:\n";
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    NAMESPACE::vector<int>    copyBuilt(second.begin(), second.end());
    std::cout << "After range built:\n";
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << "Before assigning range built vector's content:\n";
    std::cout << "size = " << third.size() << " - capacity = " << third.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = third.begin(); iter != third.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    third.assign(copyBuilt.begin(), copyBuilt.end());
    std::cout << "After assigning range built vector's content:\n";
    std::cout << "size = " << third.size() << " - capacity = " << third.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = third.begin(); iter != third.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||       iterator arithmetic      ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    it = copyBuilt.begin();
    std::cout << "i += n\n";
    it += 4;
    std::cout << *it << std::endl;
    it += -2;
    std::cout << *it << std::endl;
    std::cout << std::endl;
    std::cout << "i + n\n";
    it = it + 2;
    std::cout << *it << std::endl;
    it = it + (-2);
    std::cout << *it << std::endl;
    it = copyBuilt.end();
    std::cout << "i -= n\n";
    it -= 3;
    std::cout << *it << std::endl;
    it -= -2;
    std::cout << *it << std::endl;
    std::cout << std::endl;
    std::cout << "i - n\n";
    it = it - 3;
    std::cout << *it << std::endl;
    it = it - (-2);
    std::cout << *it << std::endl;
    
    cit = copyBuilt.begin();
    std::cout << "i - j\n";
    std::cout << it - cit << std::endl;
    
    // NAMESPACE::vector<double>::iterator   dit;
    // dit = dbl.begin();
    // std::cout << std::endl << "Value - iterator address - value address" << std::endl;
    // std::cout << *dit << " - " << &dit << " - " << &*dit << std::endl;
 


    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||            insert()            ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    std::cout << "* insert(iterator position, val)" << std::endl;
    std::cout << "Before insert at the end, middle and begining:\n";
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    it = copyBuilt.end();
    copyBuilt.insert(it, 1, 88);
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    it = copyBuilt.end();
    it -= 3;
    copyBuilt.insert(it, 1, 88);
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    it = copyBuilt.begin();
    copyBuilt.insert(it, 1, 88);
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << std::endl;

    NAMESPACE::vector<std::string>::iterator  itStr;
    NAMESPACE::vector<std::string>            str3(str.begin(), str.end());
    std::cout << "Before insert at the end, middle and begining:\n";
    std::cout << "size = " << str3.size() << " - capacity = " << str3.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = str3.begin(); iter != str3.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    itStr = str3.end();
    str3.insert(itStr, 3, "!");
    std::cout << "size = " << str3.size() << " - capacity = " << str3.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = str3.begin(); iter != str3.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    itStr = str3.end();
    itStr -= 4;
    str3.insert(itStr, 1, "-middle-");
    std::cout << "size = " << str3.size() << " - capacity = " << str3.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = str3.begin(); iter != str3.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    itStr = str3.begin();
    str3.insert(itStr, 1, "-begining-");
    std::cout << "size = " << str3.size() << " - capacity = " << str3.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = str3.begin(); iter != str3.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << std::endl;

    
    std::cout << "* insert(iterator position, n, val)" << std::endl;
    std::cout << "Before insert at the end, middle and begining:\n";
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    it = copyBuilt.end();
    it = copyBuilt.insert(it, 1);
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl << "return " << *it << std::endl;
    it = copyBuilt.end();
    it -= 3;
    it = copyBuilt.insert(it, 1);
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl << "return " << *it << std::endl;
    it = copyBuilt.begin();
    it = copyBuilt.insert(it, 1);
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl << "return " << *it << std::endl;
    std::cout << std::endl;
    
    copyBuilt.reserve(50);
    copyBuilt.clear();
    for (int i = 0; i < 6; i++)
        copyBuilt.push_back(i);
    std::cout << "Before insert at the end, middle and begining:\n";
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    it = copyBuilt.end();
    it = copyBuilt.insert(it, 9007);
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl << "return " << *it << std::endl;
    it = copyBuilt.end();
    it -= 3;
    it = copyBuilt.insert(it, 9007);
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl << "return " << *it << std::endl;
    it = copyBuilt.begin();
    it = copyBuilt.insert(it, 9007);
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl << "return " << *it << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    
    copyBuilt.clear();
    for (int i = 0; i < 6; i++)
        copyBuilt.push_back(i);
    std::cout << "Before insert at the end, middle and begining:\n";
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    it = copyBuilt.end();
    copyBuilt.insert(it, 3, 9);
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl ;
    it = copyBuilt.end();
    it -= 5;
    copyBuilt.insert(it, 3, 9);
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    it = copyBuilt.begin();
    copyBuilt.insert(it, 3, 9);
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl ;
    std::cout << std::endl;
    

    std::cout << "* insert(iterator position, iterator first, iterator last)" << std::endl;
    std::cout << "Before insert (626, 627, 628) at the end, middle and begining of big vector:\n";
    first.clear();
    for (int i = 626; i < 629; i++)
        first.push_back(i);
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    it = copyBuilt.end();
    copyBuilt.insert(it, first.begin(), first.end());
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    it = copyBuilt.end();
    it -= 7;
    copyBuilt.insert(it, first.begin(), first.end());
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    it = copyBuilt.begin();
    copyBuilt.insert(it, first.begin(), first.end());
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << std::endl;


    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||             erase()            ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    std::cout << "* erase(iterator position)" << std::endl;
    std::cout << "Before" << std::endl;
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    it = second.erase(second.end() - 1);
    std::cout << "After erasing at the end (return " << *it << "):" << std::endl;
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    it = second.erase(second.end() - 3);
    std::cout << "After erasing in the middle (return " << *it << "):" << std::endl;
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    it = second.erase(second.begin());
    std::cout << "After erasing in the begining (return " << *it << "):" << std::endl;
    std::cout << "size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    std::cout << std::endl;
    
    std::cout << "* erase(iterator first, last)" << std::endl;
    std::cout << "Before erasing at the end, in the middle and begining" << std::endl;
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    it = copyBuilt.erase(copyBuilt.end() - 6, copyBuilt.end());
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl << "return: " << *it << std::endl;
    it = copyBuilt.erase(copyBuilt.end() - 4, copyBuilt.end() - 1);
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl << "return: " << *it << std::endl;
    it = copyBuilt.erase(copyBuilt.end() - 5, copyBuilt.end() - 2);
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl << "return: " << *it << std::endl;
    it = copyBuilt.erase(copyBuilt.begin(), copyBuilt.begin() + 3);
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl << "return: " << *it << std::endl;
    it = copyBuilt.erase(copyBuilt.begin(), copyBuilt.begin() + 3);
    std::cout << "size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl << "return: " << *it << std::endl;


    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||              swap()            ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    std::cout << "BEFORE swaping:" << std::endl;
    std::cout << "1. size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl << "AND" << std::endl;
    std::cout << "2. size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    copyBuilt.swap(second);
    std::cout << "AFTER swaping:" << std::endl;
    std::cout << "1. size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl << "AND" << std::endl;
    std::cout << "2. size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    // std::cout << std::endl << "ITERATOR VALIDITY: ";
    // for (; cpyBuiltIT != second.end(); cpyBuiltIT++)
    //     std::cout << *cpyBuiltIT << " ";
    std::cout << std::endl;
    std::cout << std::endl;
    
    NAMESPACE::vector<std::string>  emptyStr;
    std::cout << "BEFORE swaping:" << std::endl;
    std::cout << "1. size = " << emptyStr.size() << " - capacity = " << emptyStr.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = emptyStr.begin(); iter != emptyStr.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl << "AND" << std::endl;
    std::cout << "2. size = " << str2.size() << " - capacity = " << str2.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = str2.begin(); iter != str2.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    emptyStr.swap(str2);
    std::cout << "AFTER swaping:" << std::endl;
    std::cout << "1. size = " << emptyStr.size() << " - capacity = " << emptyStr.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = emptyStr.begin(); iter != emptyStr.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl << "AND" << std::endl;
    std::cout << "2. size = " << str2.size() << " - capacity = " << str2.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = str2.begin(); iter != str2.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||              swap()            ||" << std::endl;
    std::cout << "||             overload           ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    std::cout << "BEFORE swaping:" << std::endl;
    std::cout << "1. size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl << "AND" << std::endl;
    std::cout << "2. size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    swap(copyBuilt, second);
    std::cout << "AFTER swaping:" << std::endl;
    std::cout << "1. size = " << copyBuilt.size() << " - capacity = " << copyBuilt.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = copyBuilt.begin(); iter != copyBuilt.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl << "AND" << std::endl;
    std::cout << "2. size = " << second.size() << " - capacity = " << second.capacity() << " - content = ";
    for (NAMESPACE::vector<int>::iterator iter = second.begin(); iter != second.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    
    NAMESPACE::vector<std::string>  emptyStr2;
    std::cout << "BEFORE swaping:" << std::endl;
    std::cout << "1. size = " << emptyStr2.size() << " - capacity = " << emptyStr2.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = emptyStr2.begin(); iter != emptyStr2.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl << "AND" << std::endl;
    std::cout << "2. size = " << str.size() << " - capacity = " << str.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = str.begin(); iter != str.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    swap(emptyStr2, str);
    std::cout << "AFTER swaping:" << std::endl;
    std::cout << "1. size = " << emptyStr2.size() << " - capacity = " << emptyStr2.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = emptyStr2.begin(); iter != emptyStr2.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl << "AND" << std::endl;
    std::cout << "2. size = " << str.size() << " - capacity = " << str.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = str.begin(); iter != str.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||        reverse_iterator        ||" << std::endl;
    std::cout << "||           arithmetic           ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    std::cout << "With vector: ";
    for (NAMESPACE::vector<int>::iterator it = copyBuilt.begin(); it != copyBuilt.end(); it++)
        std::cout << *it << " ";
    std::cout << std::endl;
    it = copyBuilt.begin();
    NAMESPACE::vector<int>::reverse_iterator    rit(it);
    it = copyBuilt.end();
    NAMESPACE::vector<int>::reverse_iterator    rit3(it);

    std::cout << "i--\n";
    rit--;
    std::cout << *rit << std::endl; 
    std::cout << std::endl;

    std::cout << "--i\n";
    --rit;
    std::cout << *rit << std::endl; 
    std::cout << std::endl;

    std::cout << "i -= n\n";
    rit -= 3;
    std::cout << *rit << std::endl;
    rit -= -2;
    std::cout << *rit << std::endl;
    std::cout << std::endl;

    std::cout << "i - n\n";
    rit = rit - 3;
    std::cout << *rit << std::endl;
    rit = rit - (-3);
    std::cout << *rit << std::endl;
    std::cout << std::endl;
    
    std::cout << "i++\n";
    rit3++;
    std::cout << *rit3 << std::endl; 
    std::cout << std::endl;

    std::cout << "++i\n";
    ++rit3;
    std::cout << *rit3 << std::endl; 
    std::cout << std::endl;

    std::cout << "i += n\n";
    rit3 += 3;
    std::cout << *rit3 << std::endl;
    rit3 += -4;
    std::cout << *rit3 << std::endl;
    std::cout << std::endl;

    std::cout << "i + n\n";
    rit3 = rit3 + 1;
    std::cout << *rit3 << std::endl;
    rit3 = rit3 + (-1);
    std::cout << *rit3 << std::endl;
    std::cout << std::endl;

    for (NAMESPACE::vector<int>::reverse_iterator it = copyBuilt.rbegin(); it != copyBuilt.rend(); it++)
        std::cout << *it << " ";
    std::cout << std::endl;


    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||          copy-construct        ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    NAMESPACE::vector<std::string>    originalVector;
    originalVector.push_back("hello");
    originalVector.push_back("1");
    originalVector.push_back("2");
    originalVector.push_back("3");
    std::cout << "Before copy-construct:" << std::endl;
    std::cout << "size = " << originalVector.size() << " - capacity = " << originalVector.capacity() << " - content = ";
    for (NAMESPACE::vector<std::string>::iterator iter = originalVector.begin(); iter != originalVector.end(); iter++)
        std::cout << *iter << " ";
    std::cout << std::endl;
    NAMESPACE::vector<std::string>    copyConstruct(originalVector);
    while (!originalVector.empty())
    {
        std::cout << originalVector.back() << " - " << originalVector.size() << std::endl;
        originalVector.pop_back();
    }
    std::cout << std::endl;
    while (!copyConstruct.empty())
    {
        std::cout << copyConstruct.back() << " - " << copyConstruct.size() << std::endl;
        copyConstruct.pop_back();
    }
    std::cout << std::endl;
    return (0);
}


int main()
{
	test_vector_a();
    test_vector_m();
    return (0);
}
