/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/08 09:20:24 by alouis            #+#    #+#             */
/*   Updated: 2021/08/23 16:01:40 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VECTOR_HPP
# define VECTOR_HPP

# include <string>
# include <iostream>
# include <stdexcept>
# include <cstddef>
# include "../iterators/Random_access_iterator.hpp"
# include "../iterators/Reverse_iterator.hpp"
# include "../utils/enable_if.hpp"
# include "../utils/compare.hpp"

namespace ft
{
    template < class T, class Alloc = std::allocator<T> >
    class vector
    {
        public:
            typedef             T                                               value_type;
            typedef             Alloc                                           allocator_type;
            typedef typename    allocator_type::reference                       reference;          //T&
            typedef typename    allocator_type::const_reference                 const_reference;    //const T&
            typedef typename    allocator_type::pointer                         pointer;            //T*
            typedef typename    allocator_type::const_pointer                   const_pointer;      //const T*
            typedef             ft::random_access_iterator<T>                   iterator;
            typedef             ft::random_access_iterator<T const>             const_iterator;
            typedef             ft::reverse_iterator<iterator, const_iterator>  reverse_iterator;
            typedef             ft::const_reverse_iterator<const_iterator>      const_reverse_iterator;
            typedef             size_t                                          size_type;          //size_t
            typedef typename    Iterator_traits<iterator>::difference_type      difference_type;    //ptrdiff_t

            explicit vector (const allocator_type& alloc = allocator_type()) : _alloc(alloc), _capacity(0), _start(NULL), _end(NULL) {
            };

            explicit vector (size_type n, const value_type& val = value_type(), const allocator_type& alloc = allocator_type()) : _alloc(alloc), _capacity(n) {
                _start = _alloc.allocate(n);
                _end = _start + n;
                while (n)
                {
                    n--;
                    _alloc.construct(_start + n, val);
                }
            };

            template <class InputIterator>
            vector (typename ft::enable_if< ft::is_iterator<InputIterator>::value, InputIterator>::type first,
                        InputIterator last, const allocator_type& alloc = allocator_type()) : _alloc(alloc) {
                size_type n;
                
                n = distance(last, first);
                _capacity = n;
                _start = _alloc.allocate(n);
                _end = _start + n;
                while (n)
                {
                    n--;
                    last--;
                    _alloc.construct(_start + n, *last);
                }
            };

            vector (const vector& x) : _alloc(x._alloc), _capacity(x.size()) {
                    size_type   n;

                    n = _capacity;
                    _start = _alloc.allocate(n);
                    _end = _start + n;
                    while (n)
                    {
                        n--;
                        _alloc.construct(_start + n,  x[n]);
                    }
            };

            virtual ~vector() {
                if (_start)
                {
                    while (size())
                        pop_back();
                    _alloc.deallocate(_start, _capacity);
                }
            };

            vector& operator= (const vector& x) {
                clear();
                if (_start)
                    _alloc.deallocate(_start, _capacity);
                _capacity = x.capacity();
                _start = _alloc.allocate(_capacity);
                _end = _start;
                assign(x.begin(), x.end());
                return (*this);
            };
            
            iterator                begin() {
                return (_start);
            };
            const_iterator          begin() const {
                return (_start);
            };
            iterator                end() {
                return (_end);
            };
            const_iterator          end() const {
                return (_end);
            };
            reverse_iterator        rbegin() {
                return reverse_iterator(end());
            };
            const_reverse_iterator  rbegin() const {
                return const_reverse_iterator(end());
            };
            reverse_iterator        rend() {
                return reverse_iterator(begin());
            };
            const_reverse_iterator  rend() const {
                return const_reverse_iterator(begin());
            };

            size_type size() const {
                return (_end - _start);
            };

            size_type max_size() const {
                size_type   max_size;

                max_size = 0;
                try 
                {
                    max_size = _alloc.max_size();
                }
                catch (std::exception &e)
                {
                    std::cout << "Error in vector.max_size() :" << e.what() << std::endl;
                }
                return (max_size);
            };

            void resize (size_type n, value_type val = value_type()) {
                while (n < size())
                    pop_back();
                if (n > _capacity)
                    reserve(n * 2);
                while (n > size())
                    push_back(val);
            };

            size_type capacity() const {
                return (_capacity);
            };

            bool empty() const {
                return (_end - _start == 0);
            };

            void reserve (size_type n) {
                if (n >= max_size())
                    throw std::length_error("allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size");
                else if (n > _capacity)
                {
                    pointer     new_start;
                    size_type   i;
                    
                    new_start = _alloc.allocate(n);
                    for (i = 0; i < size(); ++i)
                        _alloc.construct(new_start + i, _start[i]);
                    while (size())
                    {
                        _end--;
                        _alloc.destroy(_end);
                    }
                    if (_capacity != 0)
                        _alloc.deallocate(_start, _capacity);
                    _start = new_start;
                    _end = _start + i;
                    _capacity = n;
                }
            };

            reference operator[] (size_type n) {
                return (_start[n]);
            };

            const_reference operator[] (size_type n) const {
                return (_start[n]);
            };
            
            reference at (size_type n) {
                    if (n < size())
                        return (_start[n]);
                    else
                        throw std::out_of_range("Out of Range error: vector::_M_range_check");
            };

            const_reference at (size_type n) const {
                    if (n < size())
                        return (_start[n]);
                    else
                        throw std::out_of_range("Out of Range error: vector::_M_range_check");
            };

            reference front() {
                return (*_start);
            };

            const_reference front() const {
                return (*_start);
            };

            reference back() {
                return (*(_end - 1));
            };

            const_reference back() const {
                return (*(_end - 1));
            };

            template <class InputIterator>
            void assign (typename ft::enable_if< ft::is_iterator<InputIterator>::value, InputIterator>::type first, InputIterator last) {
                size_type n;

                n = distance(last, first);
                while (size())
                    pop_back();
                if (n > _capacity)
                    reserve(n);
                while (first != last)
                {
                   push_back(*first);
                   first++;
                }
            };
            
            void assign (size_type n, const value_type& val) {
                while (size())
                    pop_back();
                if (n > _capacity)
                    reserve(n);
                while (size() < n)
                    push_back(val);
            };

            void push_back (const value_type& val) {
                if (_capacity == 0)
                {
                    _start = _alloc.allocate(1);
                    _end = _start;
                    _capacity = 1;
                    _alloc.construct(_end, val);
                    _end += 1;
                    return ;
                }
                if (size() + 1 > _capacity)
                    reserve(_capacity * 2);
                _alloc.construct(_end, val);
                _end += 1;
            };

            void pop_back() {
                if (size())
                {
                    _alloc.destroy(_end - 1);
                    _end -= 1;
                }
            };

            iterator insert (iterator position, const value_type& val) {
                difference_type     n;

                n = distance(position, begin());
                insert(position, 1, val);
                return (begin() + n);
            };  


            void insert (iterator position, size_type n, const value_type& val) {
                if (size() + n > _capacity)
                {
                    vector<T>   tmp(begin(), position);
                    
                    tmp.reserve((size() + n) * 2);
                    while (n--)
                        tmp.push_back(val);
                    while (position != end())
                    {
                        tmp.push_back(*position);
                        position++;
                    }
                    assign(tmp.begin(), tmp.end());
                    reserve(tmp.capacity());
                }
                else
                {
                    difference_type dist;

                    dist = distance(position, _start);
                    for (iterator it = end() - 1; it != position - 1; it--)
                    {
					    _alloc.construct(&*(it + n), *it);
                        _alloc.destroy(&*it);
                    }
                    _end += n;
                    while (n)
                    {
                        _alloc.construct(_start + dist, val);
                        dist++;
                        n--;
                    }
                }
            };

            template <class InputIterator>
            void insert (iterator position, typename ft::enable_if< ft::is_iterator<InputIterator>::value, InputIterator>::type first, InputIterator last) {
                difference_type n;

                n = distance(last, first);
                if (size() + n > _capacity)
                {
                    vector<T>   tmp(begin(), position);
                    
                    tmp.reserve((size() + n) * 2);
                    while (first != last)
                    {
                        tmp.push_back(*first);
                        last++;
                    }
                    while (position != end())
                    {
                        tmp.push_back(*position);
                        position++;
                    }
                    assign(tmp.begin(), tmp.end());
                    reserve(tmp.capacity());
                }
                else
                {
                    difference_type dist;

                    dist = distance(position, _start);
                    for (iterator it = end() - 1; it != position - 1; it--)
                    {
					    _alloc.construct(&*(it + n), *it);
                        _alloc.destroy(&*it);
                    }
                    _end += n;
                    while (first != last)
                    {
                        _alloc.construct(_start + dist, *first);
                        dist++;
                        first++;;
                    }
                }
            };

            iterator erase (iterator position) {
                iterator    it;

                it = position;
                if (position == end())
                    return (end());
                if (position == end() - 1)
                {
                    pop_back();
                    return (end());
                }
                else
                {
                    _alloc.destroy(&*position);
                    while (++position != end())
                    {
                        _alloc.construct(&*(position - 1), *position);
                        _alloc.destroy(&*position);
                    }
                    _end -= 1;
                }
                return (it);
            };

            iterator erase (iterator first, iterator last) {
                difference_type n;
                iterator        it(first);

                n = distance(last, first);
                if (last == end())
                {
                    while (n--)
                        pop_back();
                    return (end());
                }
                while (last != end())
                {
                    _alloc.construct(&*(first), *last);
                    _alloc.destroy(&*(first));
                    last++;
                    first++;
                }
                while (first != end())
                {
                    _alloc.destroy(&*(first));
                    first++;
                }
                _end -= n;
                return (it);
            };

            void swap (vector& x) {
                vector tmp;

                tmp = *this;
                *this = x;
                x = tmp;
            };

            void clear() {
                while (size())
                    pop_back();
            };

            allocator_type get_allocator() const { return (_alloc); };
            
        private:
            allocator_type  _alloc;
            size_type       _capacity;
            pointer         _start;
            pointer         _end;
    };

    template <class T, class Alloc>
    bool operator== (const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs) {
        typename ft::vector<T>::const_iterator  lit, rit;
        
        if (lhs.size() != rhs.size())
            return false;
        for (lit = lhs.begin(), rit = rhs.begin(); lit != lhs.end(); lit++, rit++)
        {
            if (*lit != *rit)
                return false;
        }
        return true;
    };

    template <class T, class Alloc>
    bool operator!= (const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs) {
        return !(lhs == rhs);
    };

    template <class T, class Alloc>
    bool operator<  (const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs) {
        return (ft::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end()));
    };

    template <class T, class Alloc>
    bool operator<= (const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs) {
        return !(rhs < lhs);
    };

    template <class T, class Alloc>
    bool operator>  (const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs) {
        return (rhs < lhs);
    };

    template <class T, class Alloc>
    bool operator>= (const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs) {
        return !(lhs < rhs);
    };

    template <class T, class Alloc>
    void swap (vector<T,Alloc>& x, vector<T,Alloc>& y) {
        return (x.swap(y));
    };
};

#endif