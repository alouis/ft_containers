/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   List.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/20 14:44:20 by alouis            #+#    #+#             */
/*   Updated: 2021/07/02 14:32:03 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIST_HPP
# define LIST_HPP

# include <memory>
# include <iostream>
# include <string>
# include "ConstListIterator.hpp"
# include "ReverseListIterator.hpp"

namespace ft
{
    template < class T, class Alloc >
    class Node
    {
        public:
            typedef             Alloc                       allocator_type;

            // ------- Constructors --------
            Node(const allocator_type& alloc = allocator_type()) : _alloc(alloc), _elem(0), _prev_node(0), _next_node(0) {}; 
            Node(const T &val, const allocator_type& alloc = allocator_type()) : _alloc(alloc), _prev_node(0), _next_node(0) 
            {
                _elem = _alloc.allocate(1);
                _alloc.construct(_elem, val);
            };
            Node(Node<T> *prev, Node<T> *next, const allocator_type& alloc = allocator_type()) : _alloc(alloc), _prev_node(prev), _next_node(next)
            {
                _elem = _alloc.allocate(1);
                _alloc.construct(_elem, 0);
            };
            Node (const Node &old_obj) 
            {
                *this = old_obj;
            };

            Node& operator=(const Node &old_obj)
            {
                if (this != &old_obj)
                {
                    _elem = old_obj._elem;
                    _prev_node = old_obj._prev_node;
                    _next_node = old_obj._next_node;
                }
                return (*this);
            };

            ~Node() 
            {
                if (_elem)
                {
                    _alloc.destroy(_elem); 
                    _alloc.deallocate(_elem, 1);
                }
                _elem = 0;
            };

            // ------- Getters --------
            Node    *getNode() { return (this); };
            T       *getElem() const { return (_elem); };
            Node    *getPreviousNode() const { return (_prev_node); };
            Node    *getNextNode() const { return (_next_node); };

            // ------- Setters --------

            void            setElem(T& val) 
            {
                *_elem = val;
            };

            void            setPrevNode(Node* node)
            {
                _prev_node = node;
            };

            void            setNextNode(Node* node)
            {
                _next_node = node;
            };

        private:
            Alloc       _alloc;
            T           *_elem;
            Node        *_prev_node;
            Node        *_next_node;
    };

    template < class T >
    std::ostream& operator<<(std::ostream& os, Node<T>& n)
    {
        os << "current Node is at address: " << n.getNode() << " and its element is at addess : " << n.getElem();
        os << "\nprev: " << n.getPreviousNode() << " and next: " << n.getNextNode();
        if (n.getElem() == NULL)
            os << "\nNO VALUE";
        else 
            os << "\nVALUE = " << *n.getElem();
        return os;
    }

    template < class T, class Alloc = std::allocator<T> >
    class list
    {
        public:
            // ------- Member types ------- 
            typedef             T                                   value_type;
            typedef             Alloc                               allocator_type;
            typedef typename    allocator_type::reference           reference;
            typedef typename    allocator_type::const_reference     const_reference;
            typedef typename    allocator_type::pointer             pointer;
            typedef typename    allocator_type::const_pointer       const_pointer;
            typedef typename    ft::listIterator<T>                 iterator;
            typedef typename    ft::constListIterator<T>            const_iterator;
            typedef typename    std::ptrdiff_t                      difference_type;
            typedef typename    std::size_t                         size_type;
            
            // ------- Constructors --------
            explicit list (const allocator_type& alloc = allocator_type()) : _alloc(alloc), _count(0), _first_node(0), _last_node(0) { _init_list(); };
            list (size_type n, const value_type& val = value_type(), const allocator_type& alloc = allocator_type()) : _alloc(alloc), _count(0)
            {
                _init_list();
                while (n > 0)
                {
                    push_back(val);
                    n--;
                }
            };
            template <class InputIterator>
            list (InputIterator first, InputIterator last, const allocator_type& alloc = allocator_type()) : _alloc(alloc), _count(0)
            {
                _init_list();
                assign(first, last);
            };
            list (const list& x)
            {
                _init_list();
                assign(x.begin(), x.end());
            };

            // ------- Destructor -------
            ~list() 
            { 
                clear();
                delete _first_node;
                delete _last_node;
            };


            list&       operator= (const list& x)
            {
                assign(x.begin(), x.end());
                return (*this);
            };

            // ------- Capacity -------

            bool        empty() const {return ((_count == 0) ? true : false); };
            size_type   size() const { return (_count); };
            size_type   max_size() const { return (std::numeric_limits<size_type>::max() / sizeof(Node<T>)); };
            
            // ------- Iterators --------
            iterator    begin() const { return (iterator(_first_node->getNextNode())); };
            iterator    end() const { return (iterator(_last_node)); };           

            // ------- Element access --------
            reference       front()
            {
                // std::cout << "non const front" << std::endl;
                return (*_first_node->getNextNode()->getElem());
            };

            const_reference front() const
            {
                const_reference ref;

                std::cout << "const front" << std::endl;
                ref = front();
                return (ref);
            };
            
            reference       back()
            {
                // std::cout << "non const back" << std::endl;
                return (*_last_node->getPreviousNode()->getElem());
            };

            const_reference back() const
            {
                const_reference ref;

                std::cout << "const back" << std::endl;
                ref = back();
                return (ref);
            };

            // ------- Modifiers --------
            template <class InputIterator>
            void assign (InputIterator first, InputIterator last)
            {
                clear();
                while (first != last)
                {
                    push_back(*first->getElem());
                    first++;
                }
            };

            void assign (size_type n, const value_type& val)            // /!\ Doesn't go through there but through previous assign() method if variable wan't declared w/ correct type
            {
                clear();
                while (n > 0)
                {
                    push_back(val);
                    n--;
                }
            };

            void push_front (const value_type& val)
            {
                Node<T> *new_node = new Node<T>(val);

                _count++;
                new_node->setPrevNode(_first_node);
                new_node->setNextNode(_first_node->getNextNode());
                _first_node->getNextNode()->setPrevNode(new_node);
                _first_node->setNextNode(new_node);
            };

            void pop_front()
            {
                Node<T> *tmp;

                if (_count > 0)
                {
                    tmp = _first_node->getNextNode();
                    _first_node->setNextNode(tmp->getNextNode());
                    _first_node->getNextNode()->setPrevNode(_first_node);
                    delete tmp;
                    _count--;
                }
            };

            void push_back (const value_type& val)
            {
                Node<T> *new_node = new Node<T>(val);
                
                _count++;
                new_node->setPrevNode(_last_node->getPreviousNode());
                new_node->setNextNode(_last_node);
                _last_node->getPreviousNode()->setNextNode(new_node);
                _last_node->setPrevNode(new_node);
            };

            void pop_back()
            {
                Node<T> *tmp;

                if (_count > 0)
                {
                    tmp = _last_node->getPreviousNode();
                    _last_node->setPrevNode(tmp->getPreviousNode());
                    _last_node->getPreviousNode()->setNextNode(_last_node);
                    delete tmp;
                   _count--;
                }
            };

            iterator insert (iterator position, const value_type& val)
            {
                if (_count == 0)
                {
                    push_back(val);
                    return (begin());
                }
                else
                {
                    Node<T> *new_node = new Node<T>(val);
                    position->getPreviousNode()->setNextNode(new_node);
                    new_node->setPrevNode(position->getPreviousNode());
                    position->setPrevNode(new_node);
                    new_node->setNextNode(position->getNode());
                    _count++;
                    return (new_node);
                }
            };
            
            void insert (iterator position, size_type n, const value_type& val)
            {
                while (n != 0)
                {
                    position = insert(position, val);
                    n--;
                }
            };

            template <class InputIterator>
            void insert (iterator position, InputIterator first, InputIterator last)
            {
                while (last != first)
                {
                    position = insert(position, *(last->getPreviousNode()->getElem()));
                    last--;
                }
            };
            
            iterator erase (iterator position)
            {
                typename ft::list<T>::iterator it;

                if (position == end())                          //Not sure protection is necessary
                {
                    std::cout << " Malloc error " << std::endl;
                    return NULL;
                }
                else
                {
                    it = begin();
                    while (it != position)
                        it++;
                    it++;
                    position->getNextNode()->setPrevNode(position->getPreviousNode());
                    position->getPreviousNode()->setNextNode(position->getNextNode());
                    delete position->getNode();
                    _count--;
                    return (it);
                }
            };
            
            iterator erase (iterator first, iterator last)
            {
                while (first != last)
                    first = erase(first);
                return (first);
            };

            void swap (list& x)
            {
                size_t  count;
                Node<T> *tmp;

                count = _count;
                _count = x._count;
                x._count = count;
                tmp = _first_node;
                _first_node = x._first_node;
                x._first_node = tmp;
                tmp = _last_node;
                _last_node = x._last_node;
                x._last_node = tmp;
            };

            void resize (size_type n, value_type val = value_type())
            {
                while (_count > n)
                    pop_back();
                while (_count < n)
                    push_back(val);
            };

            void clear()
            {
                Node<T> *tmp;

                while (_last_node->getPreviousNode() != _first_node)
                    pop_back();
                _count = 0;
            };
            

            // ------- Operations --------
            void splice (iterator position, list& x)
            {
                splice(position, x, x.begin(), x.end());
            };

            void splice (iterator position, list& x, iterator i)
            {
                Node<T> *pos(position.operator->());
                Node<T> *x_i(i.operator->());
                
                if (!x_i->getNextNode())
                    return ;
                if (&x == this && position == i)
                    return ;
                x_i->getPreviousNode()->setNextNode(x_i->getNextNode());
                x_i->getNextNode()->setPrevNode(x_i->getPreviousNode());
                pos->getPreviousNode()->setNextNode(x_i);
                x_i->setPrevNode(pos->getPreviousNode());
                pos->setPrevNode(x_i);
                x_i->setNextNode(pos);
                _count += 1;
                x._count -= 1;
            };

            void splice (iterator position, list& x, iterator first, iterator last)
            {
                Node<T> *pos(position.operator->());
                Node<T> *ppos(pos->getPreviousNode());
                Node<T> *x_first(first.operator->());
                Node<T> *x_last(last.operator->());

                if (x._count == 0)
                    return ;
                if (&x == this)
                {
                    typename ft::list<T>::iterator  tmp(first);
                    while (tmp != last)
                    {
                        if (position == tmp)
                            return ;
                        tmp++;
                    }
                }
                x_first->getPreviousNode()->setNextNode(x_last);
                pos->getPreviousNode()->setNextNode(x_first);
                pos->setPrevNode(x_last->getPreviousNode());
                x_last->getPreviousNode()->setNextNode(pos);  
                x_last->setPrevNode(x_first->getPreviousNode());
                x_first->setPrevNode(ppos);  
                _count += x._count;
                x._count = 0;
            };

            void remove (const value_type& val)
            {
                typename ft::list<T>::iterator it;

                for (it = begin(); it != end(); ++it)
                {
                    if (*it == val)
                        erase(it);
                }
            };

            template <class Predicate>
            void remove_if (Predicate pred)
            {
                typename ft::list<T>::iterator    it;
                
                for (it = begin(); it != end(); ++it)
                {
                    if (pred(*it) == true)
                        erase(it);
                }
            };

            void unique()
            {
                typename ft::list<T>::iterator    it;

                for (it = begin(); it != end(); ++it)
                {
                    if (*it == *(it->getNextNode()->getElem()))
                        erase(it);
                }
            };
        
            template <class BinaryPredicate>
            void unique (BinaryPredicate binary_pred)
            {
                typename ft::list<T>::iterator    it;

                for (it = begin(); it != end(); ++it)
                {
                    if (binary_pred(*it, *(it->getPreviousNode()->getElem())))
                        erase(it);
                }
            };

            void merge (list& x)
            {
                splice(end(), x);
                sort();
            };
            
            template <class Compare>
            void merge (list& x, Compare comp)
            {
                splice(end(), x);
                sort(comp);
            };

            void sort()
            {
                typename ft::list<T>::iterator  node;
                typename ft::list<T>::iterator  next;

                node = begin();
                while (node != end())
                {
                    next = node + 1;
                    while (next != end())
                    {
                        if (*next->getElem() < *node->getElem())
                            swap_values(node, next);
                        next++;
                    }
                    node++;
                }
            };

            template <class Compare>
            void sort (Compare comp)
            {
                typename ft::list<T>::iterator  node;
                typename ft::list<T>::iterator  next;

                node = begin();
                while (node != end())
                {
                    next = node + 1;
                    while (next != end())
                    {
                        if (comp(*next->getElem(), *node->getElem()))
                            swap_values(node, next);
                        next++;
                    }
                    node++;
                }
            };

            void    reverse()
            {
                typename ft::list<T>::iterator  first;
                typename ft::list<T>::iterator  last;
                int                             i;

                i = 0;
                first = begin();
                last = end();
                last--;
                while (i < _count / 2)
                {
                    swap_values(first, last);
                    first++;
                    last--;
                    i++;
                }
            }
            
        private:
            Alloc       _alloc;
            size_t      _count;
            Node<T>     *_first_node;
            Node<T>     *_last_node;

            void _init_list(void) 
            {
                _first_node = new Node<T>(0, 0);
                _last_node = new Node<T>(_first_node, 0);
                _first_node->setNextNode(_last_node);
                _last_node->setNextNode(_first_node);
            };
            
            // ------- Getters --------
            size_t         getCount() const { return (_count); };
            Node<T>*       getFirstNode() const { return (_first_node); };
            Node<T>*       getLastNode() const { return (_last_node); };

            void    swap_values(iterator &left_nd, iterator &right_nd)
            { 
                T   elem;

                elem = *left_nd->getElem();
                left_nd->setElem(*right_nd->getElem());
                right_nd->setElem(elem);
            };
    };
    template <class T, class Alloc>
    void swap (list<T,Alloc>& x, list<T,Alloc>& y) { x.swap(y); };
};

#endif