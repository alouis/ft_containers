/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/20 14:44:27 by alouis            #+#    #+#             */
/*   Updated: 2021/07/02 13:24:33 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <list>
#include <vector>
#include <iterator>
#include <cmath>
#include "List.hpp"

// a predicate implemented as a function:
bool single_digit (const int& value) { return (value<10); }

// a predicate implemented as a class:
struct is_odd {
  bool operator() (const int& value) { return (value%2)==1; }
};

// a binary predicate implemented as a function:
bool same_integral_part (double first, double second)
{ return ( int(first)==int(second) ); }

// a binary predicate implemented as a class:
struct is_near {
  bool operator() (double first, double second)
  { return (fabs(first-second)<5.0); }
};

// compare only integral part:
bool mycomparison (double first, double second)
{ return ( int(first)<int(second) ); }

// comparison, not case sensitive.
bool compare_nocase (const std::string& first, const std::string& second)
{
  unsigned int i=0;
  while ( (i<first.length()) && (i<second.length()) )
  {
    if (tolower(first[i])<tolower(second[i])) return true;
    else if (tolower(first[i])>tolower(second[i])) return false;
    ++i;
  }
  return ( first.length() < second.length() );
}

int main(void)
{
    const int n = 100;
    int i = 10;
    size_t size = 4; 
    
    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||          Constructors          ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    ft::list<int>               empty_list;
    std::list<int>              off_empty_list;
        ft::list<int>               list1(size, n);
    ft::list<int>               list2(list1.begin(), list1.end());
    ft::list<int>               list3(list2);
    std::list<int>              stl_list(size, n);
    std::list<int>              stl_list2(stl_list.begin(), stl_list.end());
    ft::list<char>              list_c(5, 'c');
    ft::list<double>            mydouble, mydouble2;
    ft::list<int>::iterator     bgn;
    ft::list<int>::iterator     end;
    std::list<int>::iterator    off_bgn;
    std::list<int>::iterator    off_end;
    // ft::list<std::string>       mystrings;
    ft::list<char>::iterator     ch;

    

    off_bgn = off_empty_list.begin();
    off_end = off_empty_list.end();
    std::cout << "it on empty " << *off_bgn << " AND " << *off_end << std::endl;
    bgn = empty_list.begin();
    end = empty_list.end();
    std::cout << "it on empty " << *bgn << " AND " << *end << std::endl;

    // std::cout << "\nConstructor from iterators:" << std::endl;
    // for (ft::list<int>::iterator it = list2.begin(); it != list2.end(); ++it)
    //     std::cout << *it << " ";
    // std::cout << std::endl;
    for (ft::list<int>::iterator it = empty_list.begin(); it != empty_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    
    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||           operator=()          ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;

    empty_list = list1;
    std::cout << "Previously empty list = list1 with size = " << empty_list.size() << std::endl;
    for (ft::list<int>::iterator it = empty_list.begin(); it != empty_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    empty_list.clear();
    list1.clear();
    list1.push_back(100);
    list1.push_back(100);
    list1.push_back(100);
    list1.push_back(100);

    // for (ft::list<int>::iterator it = empty_list.begin(); it != empty_list.end(); ++it)
    //     std::cout << *it << " ";
    // std::cout << std::endl;

    
    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||        begin() - end()         ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    bgn = list1.begin();
    off_bgn = stl_list.begin();
    std::cout << "begin() " << *bgn << " & off: " << *off_bgn << std::endl;
    end = list1.end();
    off_end = stl_list.end();
    std::cout << "end() " << *end << " & off: " << *off_end << std::endl;

    std::cout << "Loop:" << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;

    std::cout << "Loop on empty list:" << std::endl;
    for (ft::list<int>::iterator it = empty_list.begin(); it != empty_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;

    std::cout << "Loop on official empty list:" << std::endl;
    for (std::list<int>::iterator it = off_empty_list.begin(); it != off_empty_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;

    std::cout << std::endl << "==========================================" << std::endl;
    std::cout << "||                                      ||" << std::endl;
    std::cout << "||     empty() - size() - maxsize()     ||" << std::endl;
    std::cout << "||                                      ||" << std::endl;
    std::cout << "==========================================" << std::endl << std::endl;
    
    if (empty_list.empty() == true)
        std::cout << "empty list is empty" << std::endl;
    if (list1.empty() == false)
        std::cout << "not empty list has " << list1.size() << " elements" << std::endl;

    std::cout << "max_size empty = " << empty_list.max_size() << std::endl;
    std::cout << "max_size ints = " << list1.max_size() << std::endl;
    // std::cout << "max_size chars = " << list2.max_size() << std::endl;    
    std::cout << "max_size OFF = " << stl_list.max_size() << std::endl;


    std::cout << std::endl << "==============================================" << std::endl;
    std::cout << "||                                          ||" << std::endl;
    std::cout << "||         resize() - front() - end()       ||" << std::endl;
    std::cout << "||                                          ||" << std::endl;
    std::cout << "==============================================" << std::endl << std::endl;

    std::cout << "resize(n > _count, no val)" << std::endl;
    stl_list.resize(8);
    empty_list.resize(8);
    std::cout << "list size is now: " << empty_list.size() << " with first and last being : " << empty_list.front() << " and " << empty_list.back() << std::endl;
    // for (it = stl_list.begin(); it != stl_list.end(); it++)
    //     std::cout << *it << " ";
    std::cout << std::endl;

    std::cout << "resize(n < _count, no val)" << std::endl;
    stl_list.resize(2);
    empty_list.resize(2);
    std::cout << "list size is now: " << empty_list.size() << " with first and last being : " << empty_list.front() << " and " << empty_list.back() << std::endl;
    // for (it = stl_list.begin(); it != stl_list.end(); it++)
    //     std::cout << *it << " ";
    std::cout << std::endl;

    std::cout << "resize(n > _count, other val)" << std::endl;
    stl_list.resize(4, 5);
    empty_list.resize(4, 5);
    std::cout << "list size is now: " << empty_list.size() << " with first and last being : " << empty_list.front() << " and " << empty_list.back() << std::endl;
    // for (it = stl_list.begin(); it != stl_list.end(); it++)
    //     std::cout << *it << " ";
    std::cout << std::endl;
        
    std::cout << "resize(n < _count, other val)" << std::endl;
    stl_list.resize(2, 5);
    empty_list.resize(0, 5);
    empty_list.resize(1, 5);
    std::cout << "list size is now: " << empty_list.size() << " with first and last being : " << empty_list.front() << " and " << empty_list.back() << std::endl;
    // for (it = stl_list.begin(); it != stl_list.end(); it++)
    //     std::cout << *it << " ";
    std::cout << std::endl;

    empty_list.resize(3);
    std::cout << "list size is now: " << empty_list.size() << " with first and last being : " << empty_list.front() << " and " << empty_list.back() << std::endl;
    
    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||             clear()            ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    empty_list.clear();
    std::cout << "list size after clear: " << empty_list.size() << std::endl;
    empty_list.resize(5, 8);
    std::cout << "list size before clear: " << empty_list.size() << std::endl;
    empty_list.clear();
    std::cout << "list size after clear: " << empty_list.size() << std::endl;

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||            assign()            ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    std::cout << "Before :" << std::endl;
    for (ft::list<int>::iterator it = empty_list.begin(); it != empty_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    empty_list.assign(list1.begin(), list1.end());
    std::cout << "After assign(list1.begin(), list1.end()) :" << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;

    std::cout << "/!\\ Doesn't work with other type than iterators" << std::endl;

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||         push() - pop()         ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
 
    list1.push_front(2);
    list1.pop_front();
    list1.push_back(2);
    list1.pop_back();
    list1.push_back(1);
    list1.push_back(2);
    list1.push_back(3);
    list1.pop_front();
    list1.push_back(4);
    empty_list.pop_front();
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||            insert()            ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    std::cout << "Before inserting at beginning:" << std::endl;
    for (std::list<int>::iterator it = stl_list.begin(); it != stl_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    size = 2;
    list1.assign(size, n);
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;

    stl_list.insert(stl_list.begin(), 27);
    list1.insert(list1.begin(), 27);
    std::cout << "After inserting at beginning:" << std::endl;
    for (std::list<int>::iterator it = stl_list.begin(); it != stl_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;

    off_bgn = stl_list.begin();
    off_bgn++;
    off_bgn++;
    bgn = list1.begin();
    bgn++;
    bgn++;
    stl_list.insert(off_bgn, 3);
    list1.insert(bgn, 3);
    std::cout << "After inserting in middle:" << std::endl;
    for (std::list<int>::iterator it = stl_list.begin(); it != stl_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;

    end = list1.end();
    off_end = stl_list.end();
    stl_list.insert(off_end, 99);     
    list1.insert(end, 99);
    std::cout << "After inserting at end:" << std::endl;
    for (std::list<int>::iterator it = stl_list.begin(); it != stl_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;

    std::cout << "------------------------" << std::endl;

    stl_list.insert(stl_list.begin(), 4, 0);
    list1.insert(list1.begin(), (unsigned int)4, 0);
    std::cout << "After inserting several at beginning:" << std::endl;
    for (std::list<int>::iterator it = stl_list.begin(); it != stl_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;

    end = list1.end();
    off_end = stl_list.end();
    stl_list.insert(off_end, 3, 0);
    list1.insert(end,  (unsigned int)3, 0);
    std::cout << "After inserting several at end:" << std::endl;
    for (std::list<int>::iterator it = stl_list.begin(); it != stl_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;

    off_bgn = stl_list.begin();
    off_bgn++;
    std::vector<int> myvector (2,30);
    myvector.push_back(9);
    std::vector<int>::iterator vit;
    vit = myvector.end();
    // vit--;
    stl_list.insert(off_bgn, myvector.begin(), vit);
    bgn = list1.begin();
    bgn++;
    int j = 30;
    list2.assign(size, j);
    list2.push_back(9);
    ft::list<int>::iterator lit;
    lit = list2.end();
    // lit--;
    list1.insert(bgn, list2.begin(), lit);
    std::cout << "After inserting several iterators:" << std::endl;
    for (std::list<int>::iterator it = stl_list.begin(); it != stl_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    std::cout << "Iterators used: " << *vit << " and " << *lit << std::endl;


    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||             erase()            ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;

    std::cout << "Before erasing:" << std::endl;
    size = 4;
    list1.assign(size, n);
    list1.push_back(7);
    list1.push_back(9);
    list1.push_front(8);
    list1.push_front(6);
    stl_list.assign(size, n);
    stl_list.push_back(7);
    stl_list.push_back(9);
    stl_list.push_front(8);
    stl_list.push_front(6);
    for (std::list<int>::iterator it = stl_list.begin(); it != stl_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    end = list1.end();
    end--;
    end = list1.erase(end);
    off_end = stl_list.end();
    off_end--;
    off_end = stl_list.erase(off_end);
    std::cout << "After erasing last:" << std::endl;
    for (std::list<int>::iterator it = stl_list.begin(); it != stl_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    std::cout << "Iterators used are now: " << *off_end << " and " << *end << std::endl;

    bgn = list1.begin();
    bgn = list1.erase(bgn);
    off_bgn = stl_list.begin();
    off_bgn = stl_list.erase(off_bgn);
    std::cout << "After erasing first:" << std::endl;
    for (std::list<int>::iterator it = stl_list.begin(); it != stl_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    std::cout << "Iterators used are now: " << *off_bgn << " and " <<*bgn << std::endl;

    bgn = list1.begin();
    bgn++;
    end = list1.end();
    end--;
    off_bgn = stl_list.begin();
    off_bgn++;
    off_end = stl_list.end();
    off_end--;
    off_bgn = stl_list.erase(off_bgn, off_end);
    bgn = list1.erase(bgn, end);
    std::cout << "After erasing several in the middle:" << std::endl;
    for (std::list<int>::iterator it = stl_list.begin(); it != stl_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    std::cout << "Iterators used are now: " << *off_bgn << " and " <<*bgn << std::endl;

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||             swap()             ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    std::cout << "Stl containers BEFORE swapping:" << std::endl;
    off_end = stl_list.end();
    off_end--;
    for (std::list<int>::iterator it = stl_list.begin(); it != stl_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (std::list<int>::iterator it = stl_list2.begin(); it != stl_list2.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    std::cout << "Iterator on end of 1st list: " << *off_end << std::endl;

    stl_list.swap(stl_list2);
    std::cout << "Stl containers AFTER swapping:" << std::endl;
    for (std::list<int>::iterator it = stl_list.begin(); it != stl_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (std::list<int>::iterator it = stl_list2.begin(); it != stl_list2.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    std::cout << "Iterator on end of 1st list: " << *off_end << std::endl;
    off_end--;
    std::cout << "after iterating: " << *off_end << std::endl;

    std::cout << "My containers BEFORE swapping:" << std::endl;
    end = list2.end();
    end--;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (ft::list<int>::iterator it = list2.begin(); it != list2.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    std::cout << "Iterator on end of 2nd list: " << *end << std::endl;

    ft::swap(list1, list2);
    std::cout << "My containers AFTER swapping:" << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (ft::list<int>::iterator it = list2.begin(); it != list2.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    std::cout << "Iterator on end of 2nd list: " << *end << std::endl;
    end--;
    std::cout << "after iterating: " << *end << std::endl;


    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||            splice()            ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    off_bgn = stl_list.begin();
    off_bgn++;
    stl_list.splice(off_bgn, stl_list2);
    std::cout << "Lists after splicing:" << std::endl;
    for (std::list<int>::iterator it = stl_list.begin(); it != stl_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (std::list<int>::iterator it = stl_list2.begin(); it != stl_list2.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    stl_list.assign(4, 100);
    stl_list2.assign(1, 8);
    stl_list2.push_back(7);

    std::cout << "Official lists before splicing:" << std::endl;
    for (std::list<int>::iterator it = stl_list.begin(); it != stl_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (std::list<int>::iterator it = stl_list2.begin(); it != stl_list2.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    off_end = stl_list2.end();
    // off_end--;
    stl_list2.splice(off_end, stl_list);
    std::cout << "Official lists after splicing:" << std::endl;
    for (std::list<int>::iterator it = stl_list.begin(); it != stl_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (std::list<int>::iterator it = stl_list2.begin(); it != stl_list2.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    std::cout << std::endl;

    std::cout << "Before splicing lists: " << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (ft::list<int>::iterator it = list2.begin(); it != list2.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    bgn = list1.begin();
    list1.splice(bgn, list2, list2.begin(), list2.end());
    std::cout << "After splicing lists: " << std::endl;
    std::cout << "list1: ";
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    std::cout << "list2: ";
    for (ft::list<int>::iterator it = list2.begin(); it != list2.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    list2.splice(list2.begin(), list1);
    std::cout << "With empty lists: " << std::endl;
    std::cout << "list1: ";
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    std::cout << "list2: ";
    for (ft::list<int>::iterator it = list2.begin(); it != list2.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    list2.splice(list2.begin(), list1);  
    std::cout << std::endl;

    list1.push_back(5);
    list1.push_back(5);
    std::cout << "BEFORE:" << std::endl;
    std::cout << "list1: ";
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    std::cout << "list2: ";
    for (ft::list<int>::iterator it = list2.begin(); it != list2.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;

    end = list1.end();
    end--;
    list1.splice(end, list2);
    std::cout << "AFTER:" << std::endl;
    std::cout << "list1: ";
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    std::cout << "list2: ";
    for (ft::list<int>::iterator it = list2.begin(); it != list2.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;


    list2.assign((size_t)2, (int)1000);
    std::cout << "BEFORE:" << std::endl;
    std::cout << "list1: ";
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    std::cout << "list2: ";
    for (ft::list<int>::iterator it = list2.begin(); it != list2.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    bgn = list2.begin();
    list1.splice(list1.end(), list2, list2.begin());
    std::cout << "AFTER:" << std::endl;
    std::cout << "list1: ";
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    std::cout << "list2: ";
    for (ft::list<int>::iterator it = list2.begin(); it != list2.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    

    // list2.push_back(2);
    // list2.push_back(3);
    // list2.push_back(4);
    // list2.push_back(1000);
    // bgn = list2.begin();
    // bgn += 1;
    // end = list2.end();
    // end -= 1;
    // std::cout << "BEFORE splicing:" << std::endl;
    // std::cout << "list1: ";
    // for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
    //     std::cout << *it << " ";
    // std::cout << std::endl;
    // std::cout << "list2: ";
    // for (ft::list<int>::iterator it = list2.begin(); it != list2.end(); ++it)
    //     std::cout << *it << " ";
    // std::cout << std::endl;
    // std::cout << *bgn << " - " << *end << std::endl;
    // list1.splice(list1.end(), list2, bgn, end);
    // std::cout << *list1.end() << " - " << *bgn << " - " << *end << std::endl;
    // std::cout << "My lists AFTER splicing:" << std::endl;
    // for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
    //     std::cout << *it << " ";
    // std::cout << std::endl;
    // for (ft::list<int>::iterator it = list2.begin(); it != list2.end(); ++it)
    //     std::cout << *it << " ";
    // std::cout << std::endl;

    std::cout << std::endl;
    std::cout << "Splicing list(list):" << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    list1.splice(list1.begin(), list1, list1.end());
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    bgn = list1.begin();
    while(*bgn != 30)
        bgn++;
    // list1.splice(list1.begin(),git 

    std::cout << std::endl;
    stl_list.push_back(5);
    stl_list.push_back(4);
    stl_list.push_back(3);
    stl_list.push_back(2);
    std::cout << "Error official" << std::endl;
    for (std::list<int>::iterator it = stl_list.begin(); it != stl_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    stl_list.splice(stl_list.begin(), stl_list, stl_list.end());
    for (std::list<int>::iterator it = stl_list.begin(); it != stl_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||            remove()            ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    list1.push_back(1);
    list1.push_back(4);
    list1.push_back(2);
    list1.push_back(3);
    list1.push_back(4);
    std::cout << "Before removing 3 and 4:" << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;

    list1.remove(3);
    list1.remove(4);
    std::cout << "After removing 3 and 4:" << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;

    list1.push_back(3);
    list1.push_back(4);
    list1.remove(1);
    list1.remove(1);
    list1.remove('c');
    std::cout << "After removing 1:" << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;

    
    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||           remove_if()          ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    list1.push_back(1);
    list1.push_back(4);
    list1.push_back(2);
    list1.push_back(3);
    list1.push_back(4);
    list1.push_back(99);
    std::cout << "Before removing_if:" << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;

    list1.remove_if(single_digit);
    std::cout << "After removing all single digits:" << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;

    list1.remove_if(is_odd());
    std::cout << "After removing odd:" << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    
    

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||            unique()            ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    list1.push_front(3);
    list1.push_back(4);
    list1.push_front(2);
    list1.push_front(0);
    list1.push_front(0);
    list1.push_back(4);
    std::cout << "Before unique:" << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    list1.unique();
    std::cout << "After unique:" << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    std::cout << std::endl;

    mydouble.push_back(2.72);
    mydouble.push_back(3.14);
    mydouble.push_back(12.15);
    mydouble.push_back(12.77);
    mydouble.push_back(12.77);
    mydouble.push_back(15.3);
    mydouble.push_back(72.25);
    mydouble.push_back(72.25);
    mydouble.push_back(73.0);
    mydouble.push_back(73.35);
    std::cout << "Before unique and unique_if:" << std::endl;
    for (ft::list<double>::iterator db = mydouble.begin(); db != mydouble.end(); ++db)
        std::cout << *db << " ";
    std::cout << std::endl;
    mydouble.unique();
    std::cout << "After unique():" << std::endl;
    for (ft::list<double>::iterator db = mydouble.begin(); db != mydouble.end(); ++db)
        std::cout << *db << " ";
    std::cout << std::endl;
    mydouble.unique(same_integral_part);
    std::cout << "After removing same integral part:" << std::endl;
    for (ft::list<double>::iterator db = mydouble.begin(); db != mydouble.end(); ++db)
        std::cout << *db << " ";
    std::cout << std::endl;
    mydouble.unique(is_near());
    std::cout << "After isnear:" << std::endl;
    for (ft::list<double>::iterator db = mydouble.begin(); db != mydouble.end(); ++db)
        std::cout << *db << " ";
    std::cout << std::endl;
    
    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||             sort()             ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    list1.push_front(10);
    mydouble.push_back(3.14);
    mydouble.push_back(12.15);
    mydouble.push_back(12.77);
    mydouble.push_back(12.77);
    mydouble.push_back(15.3);
    mydouble.push_back(72.25);
    mydouble.push_back(72.25);
    mydouble.push_back(73.0);
    mydouble.push_back(73.35);
    // mydouble.unique();
    std::cout << "Before being sorted:" << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (ft::list<double>::iterator it = mydouble.begin(); it != mydouble.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    list1.sort(); 
    mydouble.sort();
    std::cout << "After being sorted:" << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (ft::list<double>::iterator it = mydouble.begin(); it != mydouble.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    empty_list.clear();
    std::cout << "Sorting empty list:" << std::endl;
    for (ft::list<int>::iterator it = empty_list.begin(); it != empty_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;

    std::cout << "Before sort(comp):" << std::endl;
    
    // mystrings.push_back ("one");
    // mystrings.push_back ("two");
    // mystrings.push_back ("Three");
    // for (ft::list<std::string>::iterator st = mystrings.begin(); st != mystrings.end(); ++st)
    //     std::cout << *st << " ";
    // std::cout << std::endl;
    // mystrings.sort();
    // for (ft::list<std::string>::iterator st = mystrings.begin(); st != mystrings.end(); ++st)
    //     std::cout << *st << " ";
    // std::cout << std::endl;
    // mystrings.sort(compare_nocase);
    // for (ft::list<std::string>::iterator st = mystrings.begin(); st != mystrings.end(); ++st)
    //     std::cout << *st << " ";
    // std::cout << std::endl;

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||            merge()             ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    stl_list2.sort();
    stl_list = stl_list2;
    std::cout << "Official STL:" << std::endl;
    for (std::list<int>::iterator it = stl_list2.begin(); it != stl_list2.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (std::list<int>::iterator it = stl_list.begin(); it != stl_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    stl_list.merge(stl_list2);
    for (std::list<int>::iterator it = stl_list2.begin(); it != stl_list2.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (std::list<int>::iterator it = stl_list.begin(); it != stl_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    std::cout << std::endl;

    std::cout << "Mine:" << std::endl;
    empty_list = list1;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (ft::list<int>::iterator it = list2.begin(); it != list2.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    
    list1.merge(list2);
    std::cout << "After merging:" << std::endl;
    
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (ft::list<int>::iterator it = list2.begin(); it != list2.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    empty_list.push_back(50);
    empty_list.push_back(5);
    empty_list.push_back(5000);
    empty_list.push_back(-1);
    std::cout << "After merging (50, 5, 5000, -1):" << std::endl;
    list1.merge(empty_list);
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (ft::list<int>::iterator it = empty_list.begin(); it != empty_list.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;

    std::cout << "Before merge:" << std::endl;
    mydouble.clear();
    mydouble.push_back(3.1);
    mydouble.push_back(2.2);
    mydouble.push_back(2.9);
    mydouble2.push_back(3.7);
    mydouble2.push_back(7.1);
    mydouble2.push_back(1.4);
    mydouble.sort();
    mydouble2.sort();
    for (ft::list<double>::iterator db = mydouble.begin(); db != mydouble.end(); ++db)
        std::cout << *db << " ";
    std::cout << std::endl;
    for (ft::list<double>::iterator db = mydouble2.begin(); db != mydouble2.end(); ++db)
        std::cout << *db << " ";
    std::cout << std::endl;
    mydouble.merge(mydouble2);
    std::cout << "Before merge(comp):" << std::endl;
    for (ft::list<double>::iterator db = mydouble.begin(); db != mydouble.end(); ++db)
        std::cout << *db << " ";
    std::cout << std::endl;
    for (ft::list<double>::iterator db = mydouble2.begin(); db != mydouble2.end(); ++db)
        std::cout << *db << " ";
    std::cout << std::endl;
    mydouble2.push_back (2.1);
    mydouble.merge(mydouble2, mycomparison);
    std::cout << "After merge(comp):" << std::endl;
    for (ft::list<double>::iterator db = mydouble.begin(); db != mydouble.end(); ++db)
        std::cout << *db << " ";
    std::cout << std::endl;
    for (ft::list<double>::iterator db = mydouble2.begin(); db != mydouble2.end(); ++db)
        std::cout << *db << " ";
    std::cout << std::endl;

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||           reverse()            ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    std::cout << "Before:" << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (ft::list<double>::iterator db = mydouble.begin(); db != mydouble.end(); ++db)
        std::cout << *db << " ";
    std::cout << std::endl;
    std::cout << std::endl;
    
    list1.reverse();
    mydouble.reverse();
    std::cout << "After:" << std::endl;
    for (ft::list<int>::iterator it = list1.begin(); it != list1.end(); ++it)
        std::cout << *it << " ";
    std::cout << std::endl;
    for (ft::list<double>::iterator db = mydouble.begin(); db != mydouble.end(); ++db)
        std::cout << *db << " ";
    std::cout << std::endl;
    
    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||          listIterators         ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;

    int myints[] = {1, 2, 3, 4};
    stl_list.assign(myints, myints + 4);
    list1.clear();
    list1.push_back(1);
    list1.push_back(2);
    list1.push_back(3);
    list1.push_back(4);
    bgn = list1.begin();
    off_bgn = stl_list.begin();
    end = list1.end();
    off_end = stl_list.end();
    std::cout << "list1 bgn = " << *bgn << " & end = " << *end << std::endl;
    std::cout << "stl_l bgn = " << *off_bgn << " & end = " << *off_end << std::endl;
    std::cout << " - Incrementation:" << std::endl;
    bgn++;
    off_bgn++;
    std::cout << "POST list1 bgn = " << *bgn << " & off = " << *off_bgn << std::endl;
    ++bgn;
    ++off_bgn;
    std::cout << "PRE list1 bgn = " << *bgn << " & off = " << *off_bgn << std::endl;
    std::cout << std::endl;
    std::cout << " - Decrementation:" << std::endl;
    end--;
    off_end--;
    std::cout << "POST list1 bgn = " << *end << " & off = " << *off_end << std::endl;
    --end;
    --off_end;
    std::cout << "PRE list1 bgn = " << *end << " & off = " << *off_end << std::endl;
    std::cout << std::endl;

    std::cout << " - Copy assignable" << std::endl;
    bgn = list1.begin();
    end = bgn;
    std::cout << "same: " << *bgn << " - " << *end << std::endl;
    std::cout << std::endl;

    std::cout << " - Copy constructible" << std::endl;
    ft::list<int>::iterator copy(bgn);
    std::cout << "same: " << *bgn << " - " << *copy << std::endl;
    std::cout << std::endl;

    if (bgn == copy)
        std::cout <<  " - operator==() works" << std::endl;
    
    end = list1.end();
    if (bgn != end)
        std::cout <<  " - operator!=() works" << std::endl;
    std::cout << std::endl;
    
    std::cout << "- Dereferencing & incrementing (*it++)" << std::endl;
    bgn = list1.begin();
    off_bgn = stl_list.begin();
    std::cout << *bgn << " & off " << *off_bgn << std::endl;
    *bgn++;
    *off_bgn++;
    std::cout << *bgn << " & off " << *off_bgn << std::endl;
    std::cout << std::endl;

    std::cout << "- Dereferencing & decrementing (*it--)" << std::endl;
    std::cout << *bgn << " & off " << *off_bgn << std::endl;
    *bgn--;
    *off_bgn--;
    std::cout << *bgn << " & off " << *off_bgn << std::endl;
    std::cout << std::endl;

    std::cout << "- *it = i" << std::endl;
    *off_bgn = i;
    *bgn = i;
    std::cout << *bgn << " & off " << *off_bgn << std::endl;
    std::cout << std::endl;

    std::cout << "- Loop for()" << std::endl;
    for (bgn = list1.begin(); bgn != list1.end(); bgn++)
        std::cout << *bgn << " ";
    std::cout << std::endl;

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||        constListIterators      ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    ft::list<int>::const_iterator   cit;
        
    bgn = list3.begin();
    cit = bgn;
    if (cit == bgn)
        std::cout << "operator= and operator== work for const and non-const iterators: " << *bgn << " and " << *cit << std::endl;
    cit++;
    if (cit != bgn)
        std::cout << "operator!= and operator++ work for const and non-const iterators: " << *bgn << " and " << *cit << std::endl;
    if (*cit == *bgn)
        std::cout << "operator* works for const and non-const iterators: " << *bgn << " and " << *cit << std::endl;

    std::cout << "Iterating through container with const iterator:" << std::endl;
    for (cit = list3.begin(); cit != list3.end(); ++cit)
        std::cout << *cit << " ";
    std::cout << std::endl;

    std::cout << *bgn << " and " << *end << std::endl;
    if (*end < *bgn)
        std::cout  << "implement comparison operators" << std::endl;

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||       reverseListIterators     ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;    
    for (int i=0; i<10; i++) stl_list2.push_back(i);

    typedef std::list<int>::iterator iter_type;
    iter_type from (stl_list2.begin());
    iter_type until (stl_list2.end());

    std::reverse_iterator<iter_type> rev_until (from);
    std::reverse_iterator<iter_type> rev_from (until);
    std::cout << "reverse end = " << *rev_until << std::endl;
    rev_until++;
    std::cout << "rend++ = " << *rev_until << std::endl;
    rev_until--;
    rev_until--;
    std::cout << "rend-- = " << *rev_until << std::endl;
    rev_until++;

    std::cout << "reverse begin = " << *rev_from << std::endl;
    rev_from++;
    std::cout << "rend++ = " << *rev_from << std::endl;
    rev_from--;
    rev_from--;
    std::cout << "rend-- = " << *rev_from << std::endl;
    rev_from++;
    std::cout << "reverse begin = " << *rev_from << " & reverse end = " << *rev_until << std::endl;
    std::cout << "stl list:";
    while (rev_from != rev_until)
        std::cout << ' ' << *rev_from++;
    std::cout << '\n';
    return (0);
}