## :building_construction:
This container was the first and is neither finished nor perfect

## To be fixed
 * Freeing twice pointer to element when copying with assigment operator if not using deep copy :ballot_box_with_check: 
 * _alloc.allocate(n) (and others) throwing **exceptions** needs handling
 * Official pop_front() and back() have malloc issues when used on empty containers, keep protection or the issue for an exact replica of STL's containers ?
 * Pre / post-increment iterators, difference not obvious
 * When incrementing/decrementing an iterator, it goes in a loop, firstNode = lastNode => sentinel node
 * list.end() returns a node after the last one, there's an empty node at the end and begining of every list :ballot_box_with_check:
 * constructor(int, int) for size and value [doesn't deduct right](https://en.cppreference.com/w/cpp/language/class_template_argument_deduction) type and send to constructor(iterator, iterator): [implicit conversion issue](https://www.cplusplus.com/doc/tutorial/typecasting/) or [differenciating fill and range constructors failed](https://stackoverflow.com/questions/45847787/how-to-differentiate-fill-constructor-and-range-constructor-in-c11)
 * Post increment and decrement needs a [very specific declaration](https://docs.microsoft.com/en-us/cpp/cpp/increment-and-decrement-operator-overloading-cpp?view=msvc-160) where you add an [int parameter to disambiguate](https://stackoverflow.com/questions/15244094/c-overloading-for-both-pre-and-post-increment) :ballot_box_with_check:
 * Infinite loop unknown when splicing(pos, x, end--, bgn++)
 * Should add  operator() ? (List x();   // Function named x (that returns a List))
 * [How to calculate max_size()](https://stackoverflow.com/questions/7949486/how-is-max-size-calculated-in-the-function-max-size-in-stdlist)
 * Friend overloading operator =, () and [] is forbidden because they're required to be [non-static members](https://en.cppreference.com/w/cpp/language/member_functions). Use [function conversion](https://stackoverflow.com/questions/60767817/c-on-friend-assignment-operator-overloads) instead (for operator=) :ballot_box_with_check:
 * const_it = begin() works even though no overloading of non-const return value member begin(), should add const_iterator begin() const { return (const_iterator(_first_node))} ?


##
[The answer](https://cs.calvin.edu/activities/books/c++/intro/3e/WebItems/Ch14-Web/STL-List-14.4.pdf)
