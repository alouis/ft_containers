/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ListIterator.hpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/02 09:39:42 by alouis            #+#    #+#             */
/*   Updated: 2021/07/02 14:56:05 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LISTITERATOR_HPP
# define LISTITERATOR_HPP

namespace ft 
{
    template < class T, class Alloc = std::allocator<T> >
    class Node;
    
    template < class T, class Distance = ptrdiff_t, class Reference = T& > 
    class constListIterator;

    template < class T,                     // listIterator::value_type
          class Distance = ptrdiff_t,       // listIterator::difference_type
          class Reference = T&              // listIterator::reference
          > 
    class listIterator
    {
        public:
            typedef T                           value_type;
            typedef Node<T>*                    pointer;    
            typedef Distance                    difference_type;
            typedef Reference                   reference;
            typedef ft::constListIterator<T>    const_iterator;
            
            listIterator(void) : _ptr(NULL) {};
            listIterator(const pointer ptr) : _ptr(ptr) {};

            listIterator&   operator=(const listIterator &other)
            {
                _ptr = other._ptr;
                return (*this);
            }

            ~listIterator() {};
            
            bool            operator==(const listIterator &other) const
            {
                return (_ptr == other._ptr);
            }

            bool            operator!=(const listIterator &other) const
            {
                return (_ptr != other._ptr);
            }

            bool            operator<(const listIterator &other) const
            {
                return (*_ptr->getElem() < *other._ptr->getElem());
            }

            bool            operator<=(const listIterator &other) const
            {
                return (*_ptr->getElem() <= *other._ptr->getElem());
            }

            bool            operator>(const listIterator &other) const
            {
                return (*_ptr->getElem() > *other._ptr->getElem());
            }

            bool            operator>=(const listIterator &other) const
            {
                return (*_ptr->getElem() >= *other._ptr->getElem());
            }

            value_type&     operator*() { return (*_ptr->getElem()); };
            value_type&     operator*() const { return (*_ptr->getElem()); };
            pointer         operator->() { return (_ptr); };                // /!\ Not sure
            pointer         operator->() const { return (_ptr); };          // /!\ Not sure
            
            listIterator&   operator++()                                    // pre-incrementation (++it), /!\ original increment in a loop
            {
                _ptr = _ptr->getNextNode(); 
                return (*this); 
            };

            listIterator    operator++(int)                                 // post-incrementation (it++), /!\ original increment in a loop
            {
                listIterator tmp(*this);
                
                operator++(); 
                return tmp;
            };

            listIterator&   operator--()                                    // pre-decrementation (--it), /!\ original decrement in a loop
            {
                _ptr = _ptr->getPreviousNode();
                return (*this); 
            };
 
            listIterator    operator--(int)                                 // post-decrementation (it--), /!\ original decrement in a loop
            {
                listIterator tmp(*this);
                
                operator--(); 
                return tmp;
            };

            listIterator    operator+(int n)
            {
				listIterator tmp(*this);
				tmp += n;
				return (tmp);
            };

            listIterator    operator-(int n)
            {
				listIterator tmp(*this);
				tmp += n;
				return (tmp);
            };

			listIterator    &operator+=(int n)
			{
				while (n < 0)
				{
					(*this)--;
					n++;
				}
				while (n > 0)
				{
					(*this)++;
					n--;
				}
				return (*this);
			};

			listIterator    &operator-=(int n)
			{
				while (n > 0)
				{
					operator--();
					n--;
				}
				while (n < 0)
				{
					operator++();
					n++;
				}
				return (*this);
			};

            operator const_iterator() const { return (_ptr);}

        protected:
            pointer           _ptr;
    };
};

#endif