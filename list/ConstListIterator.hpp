/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ConstListIterator.hpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/02 09:42:32 by alouis            #+#    #+#             */
/*   Updated: 2021/07/02 14:55:49 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONSTLISTITERATOR_HPP
# define CONSTLISTITERATOR_HPP

# include "ListIterator.hpp"

namespace ft
{
    template < class T, class Distance, class Reference > 
    class constListIterator
    {
        public:
            typedef T                       value_type;
            typedef Node<T>*                pointer;    
            typedef Distance                difference_type;
            typedef Reference               reference;
            typedef typename ft::listIterator<T>     iterator;

            constListIterator(void) : _ptr(NULL) {};
            constListIterator(const pointer ptr) : _ptr(ptr) {};
            
            constListIterator&   operator=(const constListIterator &other)
            {
                _ptr = other._ptr;
                return (*this);
            };

            ~constListIterator() {};

            bool    operator==(const constListIterator &other) const
            {
                return (_ptr == other._ptr);
            }

            bool    operator!=(const constListIterator &other) const
            {
                return (_ptr != other._ptr);
            }

            bool            operator<(const constListIterator &other) const
            {
                return (*_ptr->getElem() < *other._ptr->getElem());
            }

            bool            operator<=(const constListIterator &other) const
            {
                return (*_ptr->getElem() <= *other._ptr->getElem());
            }

            bool            operator>(const constListIterator &other) const
            {
                return (*_ptr->getElem() > *other._ptr->getElem());
            }

            bool            operator>=(const constListIterator &other) const
            {
                return (*_ptr->getElem() >= *other._ptr->getElem());
            }

            constListIterator&   operator++()                                    // pre-incrementation (++it), /!\ original increment in a loop
            {
                _ptr = _ptr->getNextNode(); 
                return (*this); 
            };

            constListIterator    operator++(int)                                 // post-incrementation (it++), /!\ original increment in a loop
            {
                constListIterator tmp(*this);
                
                operator++(); 
                return tmp;
            };

            constListIterator&   operator--()                                    // pre-decrementation (--it), /!\ original decrement in a loop
            {
                _ptr = _ptr->getPreviousNode();
                return (*this); 
            };
 
            constListIterator    operator--(int)                                 // post-decrementation (it--), /!\ original decrement in a loop
            {
                constListIterator tmp(*this);
                
                operator--(); 
                return tmp;
            };

            const value_type&     operator*() { return (*_ptr->getElem()); };
            const pointer         operator->() { return (_ptr); };
            
            operator iterator() const { return (_ptr); };

        
        protected:
            pointer           _ptr;

    };
};

#endif