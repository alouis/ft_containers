/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/30 09:25:24 by alouis            #+#    #+#             */
/*   Updated: 2021/08/23 14:05:53 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <map>
#include "map.hpp"
#include <utility>

template <class Key, class Value>
void print_map(NAMESPACE::map<Key, Value> const &m)
{
	if (m.empty())
		return;
	for (typename NAMESPACE::map<Key, Value>::const_iterator it = m.begin(); it != m.end(); it++)
		std::cout << it->first << " - " << it->second << std::endl;
	//std::cout << m.size() << std::endl;
}

void    test_map_m()
{
    std::cout << "constructor / desctructor" << std::endl;
	{
		NAMESPACE::map<std::string, std::string> m;
		for (NAMESPACE::map<std::string, std::string>::iterator it = m.begin(); it != m.end(); it++)
		{
			std::cout << (*it).first << (*it).second << std::endl;
		}
		std::cout << m.size() << std::endl;
	}
    {
		NAMESPACE::map<int, int> m;
		NAMESPACE::map<int, int>::iterator it;
		NAMESPACE::map<int, int>::reverse_iterator rit;

		it = m.insert(NAMESPACE::pair<int, int>(4, 4)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(1, 1)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(8, 8)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(5, 5)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(3, 3)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(2, 2)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(3, 6)).first;
		std::cout << (*it).second << std::endl;

		std::cout << "iterator" << std::endl;
		for (it = m.begin(); it != m.end(); it++)
			std::cout << (*it).first << (*it).second << std::endl;
		std::cout << "reverse iterator" << std::endl;

		for (rit = m.rbegin(); rit != m.rend(); rit++)
			std::cout << (*rit).first << (*rit).second << std::endl;

		NAMESPACE::map<int, int>::const_iterator cit(it);
		for (cit = m.begin(); cit != m.end(); cit++)
			std::cout << (*cit).first << cit->second << std::endl;
		it->second = 1000;
		// Should not compile
		// cit->second = 3;
		std::cout << "operator ->" << std::endl;
		std::cout << it->second << cit->second << std::endl;

        std::cout << "erase by position and erase by key" << std::endl;
		print_map(m);
		it = m.begin();
		it++;
		m.erase(it);
		m.erase(3);
		std::cout << "---" << std::endl;
		print_map(m);

		it = m.insert(NAMESPACE::pair<int, int>(14, 4)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(11, 1)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(18, 8)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(15, 5)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(13, 3)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(12, 2)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(13, 6)).first;
		std::cout << (*it).second << std::endl;

        std::cout << "-1--" << std::endl;
		it = m.begin();
		std::cout << "delete " << it->first << std::endl;
		m.erase(it);
		print_map(m);

		std::cout << "-2--" << std::endl;
		it = m.begin();
		std::cout << "delete " << it->first << std::endl;
		m.erase(it);
		print_map(m);

		std::cout << "-3--" << std::endl;
		it = m.begin();
		std::cout << "delete " << it->first << std::endl;
		m.erase(it);
		print_map(m);

		std::cout << "--4-" << std::endl;
		it = m.begin();
		std::cout << "delete " << it->first << std::endl;
		m.erase(it);
		print_map(m);

		std::cout << "-5--" << std::endl;
		it = m.begin();
		std::cout << "delete " << it->first << std::endl;
		m.erase(it);
		print_map(m);

		std::cout << "-6--" << std::endl;
		it = m.begin();
		std::cout << "delete " << it->first << std::endl;
		m.erase(it);
		print_map(m);

		std::cout << "--7-" << std::endl;
		it = m.begin();
		std::cout << "delete " << it->first << std::endl;
		m.erase(it);
		print_map(m);
		std::cout << "-8--" << std::endl;
		it = m.begin();
		std::cout << "delete " << it->first << std::endl;
		m.erase(it);
		print_map(m);
		std::cout << "--9-" << std::endl;
		it = m.begin();
		m.erase(it);
		print_map(m);

		std::cout << "-10--" << std::endl;
		it = m.begin();
		m.erase(it);
		print_map(m);

        std::cout << "erasing root" << std::endl;
		it = m.insert(NAMESPACE::pair<int, int>(4, 4)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(1, 1)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(8, 8)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(5, 5)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(3, 3)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(2, 2)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(9, 9)).first;
		std::cout << (*it).second << std::endl;
		print_map(m);
		m.erase(5);
		print_map(m);

        std::cout << "erase range" << std::endl;
		it = m.insert(NAMESPACE::pair<int, int>(14, 4)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(11, 1)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(18, 8)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(15, 5)).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<int, int>(13, 3)).first;
		print_map(m);
		{
			NAMESPACE::map<int, int>::iterator itkb = m.begin();
			NAMESPACE::map<int, int>::iterator itke = m.end();
			itkb++;
			itkb++;
			itke--;
			itke--;
			itke--;
			std::cout << "---" << std::endl;
			m.erase(itkb, itke);
			print_map(m);
		}

        std::cout << "bounds" << std::endl;
		std::cout << (*(m.lower_bound(3))).second << std::endl;
		std::cout << (*(m.upper_bound(3))).second << std::endl;
		std::cout << (*(m.equal_range(3).first)).second << std::endl;
		std::cout << m.count(5) << m.count(10) << std::endl;
		std::cout << m[5] << m[10] << std::endl;

		std::cout << "clear" << std::endl;
		{
			NAMESPACE::map<int, int> m2 = m;
			m2.clear();
			print_map(m2);
		}

        std::cout << "swap" << std::endl;
		{
			NAMESPACE::map<int, int> m2;
			it = m2.insert(NAMESPACE::pair<int, int>(8, 8)).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<int, int>(11, 1)).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<int, int>(81, 8)).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<int, int>(51, 5)).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<int, int>(31, 3)).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<int, int>(12, 2)).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<int, int>(19, 9)).first;

			print_map(m);
			print_map(m2);
			m.swap(m2);
			std::cout << "ok" << std::endl;
			print_map(m);
			print_map(m2);
		}

        std::cout << "operator=" << std::endl;
		{
			std::map<char, int> first;
			std::map<char, int> second;

			first['x'] = 8;
			first['y'] = 16;
			first['z'] = 32;

			second = first;				   // second now contains 3 ints
			first = std::map<char, int>(); // and first is now empty

			std::cout << "Size of first: " << first.size() << '\n';
			std::cout << "Size of second: " << second.size() << '\n';
		}
        std::cout << "compare" << std::endl;
		{
			NAMESPACE::map<int, int> m2;
			it = m2.insert(NAMESPACE::pair<int, int>(8, 8)).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<int, int>(11, 1)).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<int, int>(81, 8)).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<int, int>(51, 5)).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<int, int>(31, 3)).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<int, int>(12, 2)).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<int, int>(19, 9)).first;

			NAMESPACE::map<int, int> m1(m2);
			std::cout << "---" << std::endl;
			std::cout << (m1 == m2) << std::endl;
			std::cout << (m1 != m2) << std::endl;
			std::cout << (m1 >= m2) << std::endl;
			std::cout << (m1 <= m2) << std::endl;
			std::cout << (m1 < m2) << std::endl;
			std::cout << (m1 > m2) << std::endl;
		}
		{
			NAMESPACE::map<int, int> m1, m2;
			std::cout << "===comp empty" << std::endl;
			std::cout << (m1 == m2) << std::endl;
			std::cout << (m1 != m2) << std::endl;
			std::cout << (m1 >= m2) << std::endl;
			std::cout << (m1 <= m2) << std::endl;
			std::cout << (m1 < m2) << std::endl;
			std::cout << (m1 > m2) << std::endl;
		}
    }
    std::cout << "=============== with strings =============" << std::endl;
	{
		NAMESPACE::map<std::string, std::string> m;
		NAMESPACE::map<std::string, std::string>::iterator it;
		NAMESPACE::map<std::string, std::string>::reverse_iterator rit;

		it = m.insert(NAMESPACE::pair<std::string, std::string>("4", "4")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("1", "1")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("8", "8")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("5", "5")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("3", "3")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("2", "2")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("3", "6")).first;
		std::cout << (*it).second << std::endl;

		std::cout << "iterator" << std::endl;
		for (it = m.begin(); it != m.end(); it++)
			std::cout << (*it).first << (*it).second << std::endl;
		std::cout << "reverse iterator" << std::endl;

		for (rit = m.rbegin(); rit != m.rend(); rit++)
			std::cout << (*rit).first << (*rit).second << std::endl;

		NAMESPACE::map<std::string, std::string>::const_iterator cit(it);
		for (cit = m.begin(); cit != m.end(); cit++)
			std::cout << (*cit).first << cit->second << std::endl;
		it--;
		it->second = "1000";
		// Should not compile
		// cit->second = 3;
		std::cout << "operator ->" << std::endl;
		cit--;
		std::cout << it->second << cit->second << std::endl;

		std::cout << "erase by position and erase by key" << std::endl;
		print_map(m);
		it = m.begin();
		it++;
		m.erase(it);
		m.erase("3");
		std::cout << "---" << std::endl;
		print_map(m);

        it = m.insert(NAMESPACE::pair<std::string, std::string>("14", "4")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("11", "1")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("18", "8")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("15", "5")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("13", "3")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("12", "2")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("13", "6")).first;
		std::cout << (*it).second << std::endl;

        print_map(m);
		
		std::cout << "-1--" << std::endl;
		it = m.begin();
		std::cout << "delete " << it->first << std::endl;
		m.erase(it);
		print_map(m);
		
		std::cout << "-2--" << std::endl;
		it = m.begin();
		std::cout << "delete " << it->first << std::endl;
		m.erase(it);
		print_map(m);
		
		std::cout << "-3--" << std::endl;
		it = m.begin();
		std::cout << "delete " << it->first << std::endl;
		m.erase(it);
		print_map(m);
		
		std::cout << "--4-" << std::endl;
		it = m.begin();
		std::cout << "delete " << it->first << std::endl;
		m.erase(it);
		print_map(m);
		
		std::cout << "-5--" << std::endl;
		it = m.begin();
		std::cout << "delete " << it->first << std::endl;
		m.erase(it);
		print_map(m);
		
		std::cout << "-6--" << std::endl;
		it = m.begin();
		std::cout << "delete " << it->first << std::endl;
		m.erase(it);
		print_map(m);
		
		std::cout << "--7-" << std::endl;
		it = m.begin();
		std::cout << "delete " << it->first << std::endl;
		m.erase(it);
		print_map(m);
		std::cout << "-8--" << std::endl;
		it = m.begin();
		std::cout << "delete " << it->first << std::endl;
		m.erase(it);
		print_map(m);
		std::cout << "--9-" << std::endl;
		it = m.begin();
		m.erase(it);
		print_map(m);
		
		std::cout << "-10--" << std::endl;
		it = m.begin();
		m.erase(it);
		print_map(m);

        std::cout << "erasing root" << std::endl;
		it = m.insert(NAMESPACE::pair<std::string, std::string>("4", "4")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("1", "1")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("8", "8")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("5", "5")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("3", "3")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("2", "2")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("9", "9")).first;
		std::cout << (*it).second << std::endl;
		print_map(m);
		m.erase("5");
		print_map(m);

		std::cout << "erase range" << std::endl;
		it = m.insert(NAMESPACE::pair<std::string, std::string>("14", "4")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("11", "1")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("18", "8")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("15", "5")).first;
		std::cout << (*it).second << std::endl;
		it = m.insert(NAMESPACE::make_pair<std::string, std::string>("13", "3")).first;
		print_map(m);
		{
			NAMESPACE::map<std::string, std::string>::iterator itkb = m.begin();
			NAMESPACE::map<std::string, std::string>::iterator itke = m.end();
			itkb++;
			itkb++;
			itke--;
			itke--;
			itke--;
			std::cout << "---" << std::endl;
			m.erase(itkb, itke);
			print_map(m);
		}
        std::cout << "bounds" << std::endl;
		std::cout << (*(m.lower_bound("3"))).second << std::endl;
		std::cout << (*(m.upper_bound("3"))).second << std::endl;
		std::cout << (*(m.equal_range("3").first)).second << std::endl;
		std::cout << m.count("5") << m.count("10") << std::endl;
		std::cout << m["5"] << m["10"] << std::endl;

		std::cout << "clear" << std::endl;
		{
			NAMESPACE::map<std::string, std::string> m2 = m;
			m2.clear();
			print_map(m2);
		}

		std::cout << "swap" << std::endl;
		{
			NAMESPACE::map<std::string, std::string> m2;
			it = m2.insert(NAMESPACE::pair<std::string, std::string>("8", "8")).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<std::string, std::string>("11", "1")).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<std::string, std::string>("81", "8")).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<std::string, std::string>("51", "5")).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<std::string, std::string>("31", "3")).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<std::string, std::string>("12", "2")).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<std::string, std::string>("19", "9")).first;

			print_map(m);
			print_map(m2);
			m.swap(m2);
			std::cout << "ok" << std::endl;
			print_map(m);
			print_map(m2);
        }
        std::cout << "operator=" << std::endl;
		{
			std::map<std::string, std::string> first;
			std::map<std::string, std::string> second;

			first["x"] = "8";
			first["y"] = "16";
			first["z"] = "32";

			second = first;								  // second now contains 3 ints
			first = std::map<std::string, std::string>(); // and first is now empty

			std::cout << "Size of first: " << first.size() << '\n';
			std::cout << "Size of second: " << second.size() << '\n';
		}
        std::cout << "compare" << std::endl;
		{
			NAMESPACE::map<std::string, std::string> m2;
			it = m2.insert(NAMESPACE::pair<std::string, std::string>("8", "8")).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<std::string, std::string>("11", "1")).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<std::string, std::string>("81", "8")).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<std::string, std::string>("51", "5")).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<std::string, std::string>("31", "3")).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<std::string, std::string>("12", "2")).first;
			std::cout << (*it).second << std::endl;
			it = m2.insert(NAMESPACE::make_pair<std::string, std::string>("19", "9")).first;

			NAMESPACE::map<std::string, std::string> m1(m2);
			std::cout << "---" << std::endl;
			std::cout << (m1 == m2) << std::endl;
			std::cout << (m1 != m2) << std::endl;
			std::cout << (m1 >= m2) << std::endl;
			std::cout << (m1 <= m2) << std::endl;
			std::cout << (m1 < m2) << std::endl;
			std::cout << (m1 > m2) << std::endl;
		}
		{
			NAMESPACE::map<std::string, std::string> m1, m2;
			std::cout << "===comp empty" << std::endl;
			std::cout << (m1 == m2) << std::endl;
			std::cout << (m1 != m2) << std::endl;
			std::cout << (m1 >= m2) << std::endl;
			std::cout << (m1 <= m2) << std::endl;
			std::cout << (m1 < m2) << std::endl;
			std::cout << (m1 > m2) << std::endl;
		}
    }
    std::cout << "---------- with const -------------" << std::endl;
	std::cout << "constructor / desctructor" << std::endl;
	{
		const NAMESPACE::map<std::string, std::string> m;
		for (NAMESPACE::map<std::string, std::string>::const_iterator it = m.begin(); it != m.end(); it++)
		{
			std::cout << (*it).first << (*it).second << std::endl;
		}
		std::cout << m.size() << std::endl;
	}
    {
		NAMESPACE::map<int, int> m0;
		NAMESPACE::map<int, int>::const_iterator it;
		// NAMESPACE::map<int, int>::const_reverse_iterator rit;

		m0.insert(NAMESPACE::pair<int, int>(4, 4));
		m0.insert(NAMESPACE::make_pair<int, int>(1, 1));
		m0.insert(NAMESPACE::make_pair<int, int>(8, 8));
		m0.insert(NAMESPACE::make_pair<int, int>(5, 5));
		m0.insert(NAMESPACE::make_pair<int, int>(3, 3));
		m0.insert(NAMESPACE::make_pair<int, int>(2, 2));
		m0.insert(NAMESPACE::make_pair<int, int>(3, 6));

		const NAMESPACE::map<int, int> m(m0);

		std::cout << "iterator" << std::endl;
		for (it = m.begin(); it != m.end(); it++)
			std::cout << (*it).first << (*it).second << std::endl;
		std::cout << "iterator" << std::endl;
		for (it = m0.begin(); it != m0.end(); it++)
			std::cout << (*it).first << (*it).second << std::endl;

		// std::cout << "reverse iterator" << std::endl;

		// for (rit = m.rbegin(); rit != m.rend(); rit++)
		// 	std::cout << (*rit).first << (*rit).second << std::endl;
		// for (rit = m0.rbegin(); rit != m0.rend(); rit++)
		// 	std::cout << (*rit).first << (*rit).second << std::endl;

		NAMESPACE::map<int, int>::const_iterator cit(it);
		for (cit = m.begin(); cit != m.end(); cit++)
			std::cout << (*cit).first << cit->second << std::endl;
		for (cit = m0.begin(); cit != m0.end(); cit++)
			std::cout << (*cit).first << cit->second << std::endl;

		std::cout << "operator ->" << std::endl;
		it--;
		cit--;
		std::cout << it->second << cit->second << std::endl;

		std::cout << "bounds" << std::endl;
		std::cout << (*(m.lower_bound(3))).second << std::endl;
		std::cout << (*(m.upper_bound(3))).second << std::endl;
		std::cout << (*(m.equal_range(3).first)).second << std::endl;
		std::cout << m.count(5) << m.count(10) << std::endl;
	}
    std::cout << "compare" << std::endl;
    {
        NAMESPACE::map<int, int> m2;
        m2.insert(NAMESPACE::pair<int, int>(8, 8));
        m2.insert(NAMESPACE::make_pair<int, int>(11, 1));
        m2.insert(NAMESPACE::make_pair<int, int>(81, 8));
        m2.insert(NAMESPACE::make_pair<int, int>(51, 5));
        m2.insert(NAMESPACE::make_pair<int, int>(31, 3));
        m2.insert(NAMESPACE::make_pair<int, int>(12, 2));
        m2.insert(NAMESPACE::make_pair<int, int>(19, 9));

        const NAMESPACE::map<int, int> m1(m2);
        std::cout << "---" << std::endl;
        std::cout << (m1 == m2) << std::endl;
        std::cout << (m1 != m2) << std::endl;
        std::cout << (m1 >= m2) << std::endl;
        std::cout << (m1 <= m2) << std::endl;
        std::cout << (m1 < m2) << std::endl;
        std::cout << (m1 > m2) << std::endl;
    }
    {
        const NAMESPACE::map<int, int> m1, m2;
        std::cout << "===comp empty" << std::endl;
        std::cout << (m1 == m2) << std::endl;
        std::cout << (m1 != m2) << std::endl;
        std::cout << (m1 >= m2) << std::endl;
        std::cout << (m1 <= m2) << std::endl;
        std::cout << (m1 < m2) << std::endl;
        std::cout << (m1 > m2) << std::endl;
    }
}

int test_map_a()
{
    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||           constructors         ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    NAMESPACE::map<int, std::string> emptyMap;
    NAMESPACE::map<int, std::string> copyMap(emptyMap);

    NAMESPACE::map<int, std::string>::iterator    map_it;
    NAMESPACE::map<int, std::string>::const_iterator    const_it;
    map_it = emptyMap.begin();
    map_it = emptyMap.end();
    const_it = emptyMap.begin();
    const_it = emptyMap.end();
    
    NAMESPACE::map<int, char>   myMap;
    NAMESPACE::map<int, char>   myMap2;
    NAMESPACE::map<int, char>::iterator   myMapIt;
    NAMESPACE::pair<int, double>    a(1, 'a');
    NAMESPACE::pair<int, double>    b(2, 'b');
    NAMESPACE::pair<int, double>    c(3, 'c');
    NAMESPACE::pair<int, double>    d(4, 'd');
    NAMESPACE::pair<int, double>    e(5, 'e');
    NAMESPACE::pair<int, double>    f(6, 'f');

    myMap.insert(a);
    myMap.insert(b);
    myMap.insert(c);
    myMap.insert(d);
    myMap.insert(f);

    myMap2 = myMap;
    std::cout << "Items in map:\n";
    for (NAMESPACE::map<int, char>::iterator it = myMap.begin(); it != myMap.end(); ++it)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    for (NAMESPACE::map<int, char>::iterator it = myMap2.begin(); it != myMap2.end(); ++it)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||             empty()            ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    if (copyMap.empty() == true)
        std::cout << "Empty map is empty" << std::endl;
    if (myMap.empty() == false)
        std::cout << "Not empty map is not empty" << std::endl;

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||            insert()            ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    NAMESPACE::pair<int, std::string>   hello;
    NAMESPACE::pair<int, std::string>   thx;
    NAMESPACE::pair<int, std::string>   pls;
    NAMESPACE::pair<NAMESPACE::map<int, std::string>::iterator, bool>   insertIT;
    hello = NAMESPACE::make_pair(1, "hello");
    thx = NAMESPACE::make_pair(2, "thanks");
    pls = NAMESPACE::make_pair(3, "please");
    copyMap.insert(hello);
    insertIT = copyMap.insert(hello);

    NAMESPACE::map<int, std::string>   assignedMap(copyMap.begin(), copyMap.end());
    assignedMap.insert(thx);
    assignedMap.insert(pls);
    std::cout << "Items in maps (2. is a copy of 1. with more elements inserted):\n";
    std::cout << "1. ret = " << insertIT.first->first << std::endl;
    for (NAMESPACE::map<int, std::string>::iterator it = copyMap.begin(); it != copyMap.end(); ++it)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    std::cout << "2.\n";
    for (NAMESPACE::map<int, std::string>::iterator it = assignedMap.begin(); it != assignedMap.end(); ++it)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;

    copyMap.insert(assignedMap.begin(), assignedMap.end());
    std::cout << "Items in maps (1.insert(2.begin(), 2.end())):\n";
    std::cout << "1.\n";
    for (NAMESPACE::map<int, std::string>::iterator it = copyMap.begin(); it != copyMap.end(); ++it)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    std::cout << "2.\n";
    for (NAMESPACE::map<int, std::string>::iterator it = assignedMap.begin(); it != assignedMap.end(); ++it)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;

    NAMESPACE::pair<int, std::string>   good;
    NAMESPACE::pair<int, std::string>   bye;
    good = NAMESPACE::make_pair(5, "good");
    bye = NAMESPACE::make_pair(5, "bye");
    map_it = copyMap.insert(copyMap.begin(), good);
    map_it = copyMap.insert(copyMap.begin(), bye);
    std::cout << "Items in maps (insert(position, new_item)):\n";
    std::cout << "1. ret = [" << map_it->first << "] = " << map_it->second << std::endl;
    for (NAMESPACE::map<int, std::string>::iterator it = copyMap.begin(); it != copyMap.end(); ++it)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    map_it = assignedMap.insert(assignedMap.end(), good);
    map_it = assignedMap.insert(assignedMap.end(), bye);
    std::cout << "2. ret = [" << map_it->first << "] = " << map_it->second << std::endl;
    for (NAMESPACE::map<int, std::string>::iterator it = assignedMap.begin(); it != assignedMap.end(); ++it)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;


    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||           operator[]           ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    assignedMap[5] = "good";
    assignedMap[6] = "bye";
    std::cout << "2." << map_it->second << std::endl;
    for (NAMESPACE::map<int, std::string>::iterator it = assignedMap.begin(); it != assignedMap.end(); ++it)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;


    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||              find()            ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    copyMap.insert(NAMESPACE::make_pair<int, std::string>(0, "O"));
    assignedMap.insert(NAMESPACE::make_pair<int, std::string>(0, "O"));
    myMap2.insert(NAMESPACE::make_pair<int, char>(0, 'o'));
    std::cout << "Empty map:\n";
    for (NAMESPACE::map<int, std::string>::iterator it = emptyMap.begin(); it != emptyMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    std::cout << "Copy map:\n";
    for (NAMESPACE::map<int, std::string>::iterator it = copyMap.begin(); it != copyMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    std::cout << "Assigned map:\n";
    for (NAMESPACE::map<int, std::string>::iterator it = assignedMap.begin(); it != assignedMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    std::cout << "myMap2 map:\n";
    for (NAMESPACE::map<int, char>::iterator it = myMap2.begin(); it != myMap2.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;

    for (int i = 0; i < 7; ++i)
    {
        std::cout << "Element [" << i << "] is:\n";
        
        map_it = emptyMap.find(i);
        if (map_it == emptyMap.end())
            std::cout << "Empty map: Key wasn't found" << std::endl;
        else
            std::cout << "Empty map = " << map_it->second << std::endl;
        
        map_it = copyMap.find(i);   
        if (map_it == copyMap.end())
            std::cout << "Copy map: Key wasn't found" << std::endl;
        else
            std::cout << "Copy map = " << map_it->second << std::endl;
        
        map_it = assignedMap.find(i);
        if (map_it == assignedMap.end())
            std::cout << "Assigned map: Key wasn't found" << std::endl;
        else
            std::cout << "Assigned map = " << map_it->second << std::endl;

        myMapIt = myMap2.find(i);
        if (myMapIt == myMap2.end())
            std::cout << "myMap map: Key wasn't found" << std::endl;
        else
            std::cout << "myMap map = " << myMapIt->second << std::endl;
        std::cout << std::endl;
    }
    
    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||              size()            ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    std::cout << "Empty map's size: " << emptyMap.size() << std::endl;
    std::cout << "Copy map's size: " << copyMap.size() << std::endl;
    std::cout << "Assigned map's size: " << assignedMap.size() << std::endl;
    std::cout << "myMap map's size: " << myMap.size() << std::endl;
    std::cout << "myMap2 map's size: " << myMap2.size() << std::endl;

    std::cout << "Empty map's size: " << emptyMap.max_size() << std::endl;
    std::cout << "Copy map's size: " << copyMap.max_size() << std::endl;
    std::cout << "Assigned map's size: " << assignedMap.max_size() << std::endl;
    std::cout << "myMap map's size: " << myMap.max_size() << std::endl;
    std::cout << "myMap2 map's size: " << myMap2.max_size() << std::endl;

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||          lower_bound()         ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    NAMESPACE::map<int, char>::const_iterator myIt;
    std::cout << "For map containing:\n";
    for (NAMESPACE::map<int, char>::iterator it = myMap2.begin(); it != myMap2.end(); ++it)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    myIt = myMap2.lower_bound(0);
    if (myIt == myMap2.end())
        std::cout << "key = 0 ; lower bound = end()\n";
    else 
        std::cout << "key = 0 ; lower bound = " << myIt->first << std::endl;
    myIt = myMap2.lower_bound(3);
    std::cout << "key = 3 ; lower bound = " << myIt->first << std::endl;
    myIt = myMap2.lower_bound(5);
    std::cout << "key = 5 ; lower bound = " << myIt->first << std::endl;
    myIt = myMap2.lower_bound(6);
    std::cout << "key = 6 ; lower bound = " << myIt->first << std::endl;
    myIt = myMap2.lower_bound(10);
    if (myIt == myMap2.end())
        std::cout << "key = 10 ; lower bound = end()\n";

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||          upper_bound()         ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    std::cout << "For map containing:\n";
    for (NAMESPACE::map<int, char>::iterator it = myMap2.begin(); it != myMap2.end(); ++it)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    myIt = myMap2.upper_bound(0);
    if (myIt == myMap2.end())
        std::cout << "key = 0 ; upper bound = end()\n";
    else 
        std::cout << "key = 0 ; upper bound = " << myIt->first << std::endl;
    myIt = myMap2.upper_bound(3);
    if (myIt == myMap2.end())
        std::cout << "key = 3 ; upper bound = end()\n";
    else 
        std::cout << "key = 3 ; upper bound = " << myIt->first << std::endl;
    myIt = myMap2.upper_bound(5);
    if (myIt == myMap2.end())
        std::cout << "key = 5 ; upper bound = end()\n";
    else 
        std::cout << "key = 5 ; upper bound = " << myIt->first << std::endl;
    myIt = myMap2.upper_bound(6);
    if (myIt == myMap2.end())
        std::cout << "key = 6 ; upper bound = end()\n";
    else 
        std::cout << "key = 6 ; upper bound = " << myIt->first << std::endl;
    myIt = myMap2.upper_bound(10);
    if (myIt == myMap2.end())
        std::cout << "key = 10 ; upper bound = end()\n";
    else 
        std::cout << "key = 10 ; upper bound = " << myIt->first << std::endl;


    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||          equal_range()         ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    NAMESPACE::pair<NAMESPACE::map<int, char>::const_iterator, NAMESPACE::map<int, char>::const_iterator>   range;
    std::cout << "For map containing:\n";
    
    for (NAMESPACE::map<int, char>::iterator it = myMap2.begin(); it != myMap2.end(); ++it)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    
    range = myMap2.equal_range(0);
    std::cout << "key = 0\n";
    std::cout << "lower bound points to: ";
    if (range.first == myMap2.end())
        std::cout << " end()\n";
    else
        std::cout << range.first->first << " => " << range.first->second << '\n';
    std::cout << "upper bound points to: ";
    if (range.second == myMap2.end())
        std::cout << " end()\n" << std::endl;
    else
        std::cout << range.second->first << " => " << range.second->second << '\n';
    std::cout << std::endl;
    

    range = myMap2.equal_range(1);
    std::cout << "key = 1\n";
    std::cout << "lower bound points to: ";
    if (range.first == myMap2.end())
        std::cout << " end()\n ";
    else
        std::cout << range.first->first << " => " << range.first->second << '\n';
    std::cout << "upper bound points to: ";
    if (range.second == myMap2.end())
        std::cout << " end()\n" << std::endl;
    else
        std::cout << range.second->first << " => " << range.second->second << '\n';
    std::cout << std::endl;

    range = myMap2.equal_range(3);
    std::cout << "key = 3\n";
    std::cout << "lower bound points to: ";
    if (range.first == myMap2.end())
        std::cout << " end()\n";
    else
        std::cout << range.first->first << " => " << range.first->second << '\n';
    std::cout << "upper bound points to: ";
    if (range.second == myMap2.end())
        std::cout << " end()\n" << std::endl;
    else
        std::cout << range.second->first << " => " << range.second->second << '\n';
    std::cout << std::endl;

    range = myMap2.equal_range(6);
    std::cout << "key = 6\n";
    std::cout << "lower bound points to: ";
    if (range.first == myMap2.end())
        std::cout << " end()\n";
    else
        std::cout << range.first->first << " => " << range.first->second << '\n';
    std::cout << "upper bound points to: ";
    if (range.second == myMap2.end())
        std::cout << " end()\n" << std::endl;
    else
        std::cout << range.second->first << " => " << range.second->second << '\n';
    std::cout << std::endl;

    range = myMap2.equal_range(10);
    std::cout << "key = 10\n";
    std::cout << "lower bound points to: ";
    if (range.first == myMap2.end())
        std::cout << " end()\n";
    else
        std::cout << range.first->first << " => " << range.first->second << '\n';
    std::cout << "upper bound points to: ";
    if (range.second == myMap2.end())
        std::cout << " end()\n" << std::endl;
    else
        std::cout << range.second->first << " => " << range.second->second << '\n';
    std::cout << std::endl;

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||         erase(position)        ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    NAMESPACE::map<int, int>    one;
    one.insert(NAMESPACE::make_pair<int, int>(1, 123));
    one.insert(NAMESPACE::make_pair<int, int>(3, 123));
    one.insert(NAMESPACE::make_pair<int, int>(2, 123));
    one.insert(NAMESPACE::make_pair<int, int>(7, 123));
    one.insert(NAMESPACE::make_pair<int, int>(8, 123));
    one.insert(NAMESPACE::make_pair<int, int>(4, 123));
    one.insert(NAMESPACE::make_pair<int, int>(5, 123));
    one.insert(NAMESPACE::make_pair<int, int>(6, 123));
    std::cout << "For map containing:\n";
    for (NAMESPACE::map<int, int>::iterator  oneIt = one.begin(); oneIt != one.end(); oneIt++)
        std::cout << "[" << oneIt->first << "] ";
    std::cout << std::endl;
    
    one.insert(NAMESPACE::make_pair<int, int>(3, 123));
    NAMESPACE::map<int, int>::iterator  oneIt;
    oneIt = one.begin();
    oneIt++;
    oneIt++;
    oneIt++;
    oneIt++;
    one.erase(oneIt);
    std::cout << std::endl << std::endl;
    std::cout << "***** Erasing node with only one child: ****" << std::endl;
    for (NAMESPACE::map<int, int>::iterator  oneIt = one.begin(); oneIt != one.end(); oneIt++)
        std::cout << "[" << oneIt->first << "] ";
    std::cout << std::endl;
    
    one.insert(NAMESPACE::make_pair<int, int>(5, 123));
    std::cout << std::endl << std::endl;
    std::cout << "***** Erasing one leave at a time: *****" << std::endl;
    oneIt = one.end();
    oneIt--;
    one.erase(oneIt);
    one.erase(one.begin());
    for (NAMESPACE::map<int, int>::iterator  oneIt = one.begin(); oneIt != one.end(); oneIt++)
        std::cout << "[" << oneIt->first << "] ";
    std::cout << std::endl;
    oneIt = one.begin();
    oneIt++;
    one.erase(oneIt);
    for (NAMESPACE::map<int, int>::iterator  oneIt = one.begin(); oneIt != one.end(); oneIt++)
        std::cout << "[" << oneIt->first << "] ";
    std::cout << std::endl;
    one.erase(one.begin());
    for (NAMESPACE::map<int, int>::iterator  oneIt = one.begin(); oneIt != one.end(); oneIt++)
        std::cout << "[" << oneIt->first << "] ";
    std::cout << std::endl;

    one.insert(NAMESPACE::make_pair<int, int>(1, 123));
    one.insert(NAMESPACE::make_pair<int, int>(3, 123));
    one.insert(NAMESPACE::make_pair<int, int>(2, 123));
    one.insert(NAMESPACE::make_pair<int, int>(8, 123));
    one.insert(NAMESPACE::make_pair<int, int>(9, 123));
    one.insert(NAMESPACE::make_pair<int, int>(10, 123));
    one.insert(NAMESPACE::make_pair<int, int>(11, 123));
    one.insert(NAMESPACE::make_pair<int, int>(12, 123));
    one.insert(NAMESPACE::make_pair<int, int>(13, 123));
    one.insert(NAMESPACE::make_pair<int, int>(14, 123));
    one.insert(NAMESPACE::make_pair<int, int>(15, 123));
    std::cout << "For map containing:\n";
    for (NAMESPACE::map<int, int>::iterator  oneIt = one.begin(); oneIt != one.end(); oneIt++)
        std::cout << "[" << oneIt->first << "] ";
    std::cout << std::endl;
    
    std::cout << std::endl << std::endl;
    std::cout << "***** Erasing one node at a time: *****" << std::endl;
    oneIt = one.begin();
    oneIt++;
    oneIt++;
    oneIt++;
    oneIt++;
    oneIt++;
    oneIt++;
    oneIt++;
    oneIt++;
    oneIt++;
    one.erase(oneIt);
    for (NAMESPACE::map<int, int>::iterator  oneIt = one.begin(); oneIt != one.end(); oneIt++)
        std::cout << "[" << oneIt->first << "] ";
    std::cout << std::endl;
    oneIt = one.end();
    oneIt--;
    oneIt--;
    one.erase(oneIt);
    for (NAMESPACE::map<int, int>::iterator  oneIt = one.begin(); oneIt != one.end(); oneIt++)
        std::cout << "[" << oneIt->first << "] ";
    std::cout << std::endl;

    std::cout << "============================" << std::endl;
    std::cout << "For maps containing:\n";
    std::cout << "Copy map:\n";
    for (NAMESPACE::map<int, std::string>::iterator it = copyMap.begin(); it != copyMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    std::cout << "Assigned map:\n";
    for (NAMESPACE::map<int, std::string>::iterator it = assignedMap.begin(); it != assignedMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    std::cout << "myMap2 map:\n";
    for (NAMESPACE::map<int, char>::iterator it = myMap2.begin(); it != myMap2.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;

    std::cout << std::endl << "----END()----" << std::endl;
    std::cout << "/!\\ trying to erase end() causes segfault\n" << std::endl;

    std::cout << std::endl << "----BEGIN()----" << std::endl;
    copyMap.erase(copyMap.begin());
    std::cout << "Copy map:\n";
    for (NAMESPACE::map<int, std::string>::iterator it = copyMap.begin(); it != copyMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    assignedMap.erase(assignedMap.begin());
    std::cout << "Assigned map:\n";
    for (NAMESPACE::map<int, std::string>::iterator it = assignedMap.begin(); it != assignedMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    myMap2.erase(myMap2.begin());
    std::cout << "myMap2 map:\n";
    for (NAMESPACE::map<int, char>::iterator it = myMap2.begin(); it != myMap2.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;

    std::cout << std::endl << "----LAST ELEMENT----" << std::endl;
    std::cout << "Empty map:\n";
    std::cout << "/!\\ trying to erase a node that doesn't exist causes segfault\n" << std::endl;
    map_it = copyMap.end();
    map_it--;
    copyMap.erase(map_it);
    std::cout << "Copy map:\n";
    for (NAMESPACE::map<int, std::string>::iterator it = copyMap.begin(); it != copyMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    map_it = assignedMap.end();
    map_it--;
    assignedMap.erase(map_it);
    map_it = assignedMap.end();
    std::cout << "Assigned map:\n";
    for (NAMESPACE::map<int, std::string>::iterator it = assignedMap.begin(); it != assignedMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    myMapIt = myMap2.end();
    myMapIt--;
    myMap2.erase(myMapIt);
    std::cout << "myMap2 map:\n";
    for (NAMESPACE::map<int, char>::iterator it = myMap2.begin(); it != myMap2.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;


    std::cout << std::endl << "----RANDOM ELEMENT----" << std::endl;
    std::cout << "Assigned map:\n";
    map_it = assignedMap.begin();
    map_it++;
    map_it++;
    assignedMap.erase(map_it);
    for (NAMESPACE::map<int, std::string>::iterator it = assignedMap.begin(); it != assignedMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    
    std::cout << "myMap2\n";
    myMapIt = myMap2.begin();
    myMapIt++;
    myMapIt++;
    myMap2.erase(myMapIt);
    for (NAMESPACE::map<int, char>::iterator it = myMap2.begin(); it != myMap2.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    
    std::cout << "myMap\n";
    myMapIt = myMap.begin();
    myMapIt++;
    myMapIt++;
    myMap.erase(myMapIt);
    for (NAMESPACE::map<int, char>::iterator it = myMap.begin(); it != myMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;

    
    std::cout << std::endl << "----ROOT----" << std::endl;
    NAMESPACE::map<char, double> letters;
    letters.insert(NAMESPACE::make_pair<char, float>('a', 7.9));
    letters.insert(NAMESPACE::make_pair<char, float>('b', 9.8));
    
    NAMESPACE::map<char, double>::iterator  lIt;
    lIt = letters.begin();
    lIt++;
    letters.erase(lIt);
    for (NAMESPACE::map<char, double>::iterator it = letters.begin(); it != letters.end(); it++)
        std::cout << "[" << it->first << "] ";
    std::cout << std::endl;
    // letters.print_map();

    map_it = copyMap.begin();
    map_it++;
    copyMap.erase(map_it);
    std::cout << "Copy map:\n";
    for (NAMESPACE::map<int, std::string>::iterator it = copyMap.begin(); it != copyMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;

    std::cout << "Assigned map:\n";
    map_it = assignedMap.begin();
    map_it++;
    assignedMap.erase(map_it);
    for (NAMESPACE::map<int, std::string>::iterator it = assignedMap.begin(); it != assignedMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    
    std::cout << "myMap2\n";
    myMapIt = myMap2.begin();
    myMapIt++;
    myMap2.erase(myMapIt);
    for (NAMESPACE::map<int, char>::iterator it = myMap2.begin(); it != myMap2.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    
    std::cout << "myMap\n";
    myMapIt = myMap.begin();
    myMapIt++;
    myMap.erase(myMapIt);
    for (NAMESPACE::map<int, char>::iterator it = myMap.begin(); it != myMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    
    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||           erase(range)         ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    std::cout << "For map containing:\n";
    for (NAMESPACE::map<int, int>::iterator it = one.begin(); it != one.end(); it++)
        std::cout << "[" << it->first << "] ";
    std::cout << std::endl;

    std::cout << std::endl << "Deleting [1, 15) :\n";
    oneIt = one.end();
    oneIt--;
    one.erase(one.begin(), oneIt);
    for (NAMESPACE::map<int, int>::iterator it = one.begin(); it != one.end(); it++)
        std::cout << "[" << it->first << "] ";
    std::cout << std::endl;
    
    one.insert(NAMESPACE::make_pair<int, int>(1, 123));
    one.insert(NAMESPACE::make_pair<int, int>(3, 123));
    one.insert(NAMESPACE::make_pair<int, int>(2, 123));
    one.insert(NAMESPACE::make_pair<int, int>(14, 123));
    one.insert(NAMESPACE::make_pair<int, int>(15, 123));
    one.insert(NAMESPACE::make_pair<int, int>(8, 123));
    one.insert(NAMESPACE::make_pair<int, int>(9, 123));
    one.insert(NAMESPACE::make_pair<int, int>(10, 123));
    one.insert(NAMESPACE::make_pair<int, int>(1, 123));
    one.insert(NAMESPACE::make_pair<int, int>(3, 123));
    one.insert(NAMESPACE::make_pair<int, int>(2, 123));
    one.insert(NAMESPACE::make_pair<int, int>(7, 123));
    one.insert(NAMESPACE::make_pair<int, int>(8, 123));
    one.insert(NAMESPACE::make_pair<int, int>(4, 123));
    one.insert(NAMESPACE::make_pair<int, int>(5, 123));
    one.insert(NAMESPACE::make_pair<int, int>(6, 123));
    one.insert(NAMESPACE::make_pair<int, int>(11, 123));
    one.insert(NAMESPACE::make_pair<int, int>(12, 123));
    one.insert(NAMESPACE::make_pair<int, int>(13, 123));

    std::cout << std::endl << "Deleting [2, end) :\n";
    oneIt = one.begin();
    oneIt++;
    one.erase(oneIt, one.end());
    for (NAMESPACE::map<int, int>::iterator it = one.begin(); it != one.end(); it++)
        std::cout << "[" << it->first << "] ";
    std::cout << std::endl;

    one.insert(NAMESPACE::make_pair<int, int>(1, 123));
    one.insert(NAMESPACE::make_pair<int, int>(3, 123));
    one.insert(NAMESPACE::make_pair<int, int>(2, 123));
    one.insert(NAMESPACE::make_pair<int, int>(14, 123));
    one.insert(NAMESPACE::make_pair<int, int>(15, 123));
    one.insert(NAMESPACE::make_pair<int, int>(8, 123));
    one.insert(NAMESPACE::make_pair<int, int>(9, 123));
    one.insert(NAMESPACE::make_pair<int, int>(10, 123));
    one.insert(NAMESPACE::make_pair<int, int>(1, 123));
    one.insert(NAMESPACE::make_pair<int, int>(3, 123));
    one.insert(NAMESPACE::make_pair<int, int>(2, 123));
    one.insert(NAMESPACE::make_pair<int, int>(7, 123));
    one.insert(NAMESPACE::make_pair<int, int>(8, 123));
    one.insert(NAMESPACE::make_pair<int, int>(4, 123));
    one.insert(NAMESPACE::make_pair<int, int>(5, 123));
    one.insert(NAMESPACE::make_pair<int, int>(6, 123));
    one.insert(NAMESPACE::make_pair<int, int>(11, 123));
    one.insert(NAMESPACE::make_pair<int, int>(12, 123));
    one.insert(NAMESPACE::make_pair<int, int>(13, 123));

    std::cout << std::endl << "Deleting [5, 12) :\n";
    NAMESPACE::map<int, int>::iterator  oneIt2(one.end());
    oneIt = one.begin();
    for (int i = 0; i < 4; i++)
        oneIt++;
    for (int i = 0; i < 4; i++)
        oneIt2--;
    one.erase(oneIt, oneIt2);
    for (NAMESPACE::map<int, int>::iterator it = one.begin(); it != one.end(); it++)
        std::cout << "[" << it->first << "] ";
    std::cout << std::endl;
    
    std::cout << std::endl << "Deleting [first, last) :\n";
    one.erase(one.begin(), one.end());
    for (NAMESPACE::map<int, int>::iterator it = one.begin(); it != one.end(); it++)
        std::cout << "[" << it->first << "] ";
    std::cout << std::endl;

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||             erase(k)           ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    std::cout << "For maps containing:\n";
    std::cout << "Copy map:\n";
    for (NAMESPACE::map<int, std::string>::iterator it = copyMap.begin(); it != copyMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    std::cout << "Assigned map:\n";
    for (NAMESPACE::map<int, std::string>::iterator it = assignedMap.begin(); it != assignedMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    std::cout << "myMap2 map:\n";
    for (NAMESPACE::map<int, char>::iterator it = myMap2.begin(); it != myMap2.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    std::cout << "myMap map:\n";
    for (NAMESPACE::map<int, char>::iterator it = myMap.begin(); it != myMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;

    std::cout << std::endl << "Deleting node with key = 1:\n";

    copyMap.erase(1);
    std::cout << "Copy map:\n";
    for (NAMESPACE::map<int, std::string>::iterator it = copyMap.begin(); it != copyMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;

    assignedMap.erase(1);
    std::cout << "Assigned map:\n";
    for (NAMESPACE::map<int, std::string>::iterator it = assignedMap.begin(); it != assignedMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;

    myMap2.erase(1);
    std::cout << "myMap2 map:\n";
    for (NAMESPACE::map<int, char>::iterator it = myMap2.begin(); it != myMap2.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;

    myMap.erase(1);
    std::cout << "myMap map:\n";
    for (NAMESPACE::map<int, char>::iterator it = myMap.begin(); it != myMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||              swap()            ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    std::cout << "BEFORE swapping:\n";
    
    std::cout << "myMap2:\n";
    for (NAMESPACE::map<int, char>::iterator it = myMap2.begin(); it != myMap2.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << "&\n";
    std::cout << "myMap:\n";
    for (NAMESPACE::map<int, char>::iterator it = myMap.begin(); it != myMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    
    std::cout << "Copy map:\n";
    for (NAMESPACE::map<int, std::string>::iterator it = copyMap.begin(); it != copyMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << "&\n";
    std::cout << "Assigned map:\n";
    for (NAMESPACE::map<int, std::string>::iterator it = assignedMap.begin(); it != assignedMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;

    myMap.swap(myMap2);
    copyMap.swap(assignedMap);
    std::cout << "AFTER swapping:\n";
    std::cout << "myMap2 map:\n";
    for (NAMESPACE::map<int, char>::iterator it = myMap2.begin(); it != myMap2.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << "&\n";
    std::cout << "myMap map:\n";
    for (NAMESPACE::map<int, char>::iterator it = myMap.begin(); it != myMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;
    
    std::cout << "Copy map:\n";
    for (NAMESPACE::map<int, std::string>::iterator it = copyMap.begin(); it != copyMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << "&\n";
    std::cout << "Assigned map:\n";
    for (NAMESPACE::map<int, std::string>::iterator it = assignedMap.begin(); it != assignedMap.end(); it++)
        std::cout << "[" << it->first << "] = " << it->second << std::endl;
    std::cout << std::endl;


    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||       reverse_iterator()       ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    one.insert(NAMESPACE::make_pair<int, int>(10, 123));
    one.insert(NAMESPACE::make_pair<int, int>(1, 123));
    one.insert(NAMESPACE::make_pair<int, int>(3, 123));
    one.insert(NAMESPACE::make_pair<int, int>(2, 123));
    one.insert(NAMESPACE::make_pair<int, int>(7, 123));
    one.insert(NAMESPACE::make_pair<int, int>(8, 123));
    one.insert(NAMESPACE::make_pair<int, int>(4, 123));
    one.insert(NAMESPACE::make_pair<int, int>(5, 123));
    one.insert(NAMESPACE::make_pair<int, int>(6, 123));

    std::cout << "For map containing:\n";
    for (NAMESPACE::map<int, int>::iterator it = one.begin(); it != one.end(); it++)
        std::cout << "[" << it->first << "] ";
    std::cout << std::endl;
    
    std::cout << std::endl;
    std::cout << "Printing backward with reverse iterator:\n";
    for (NAMESPACE::map<int, int>::reverse_iterator rit = one.rbegin(); rit != one.rend(); rit++)
        std::cout << "[" << rit->first << "] ";
    std::cout << std::endl;

    NAMESPACE::map<int, int>::reverse_iterator  rbeg(one.rbegin());
    NAMESPACE::map<int, int>::reverse_iterator  rend(one.rend());

    std::cout << "rbegin = [" << rbeg->first << "] " << rbeg->second << std::endl;
    std::cout << "rend = [" << rend->first << "] " << rend->second << std::endl;

    std::cout << std::endl;
    std::cout << "rbegin++ :\n";
    rbeg++;
    std::cout << "rbegin = [" << rbeg->first << "] " << rbeg->second << std::endl;

    std::cout << std::endl;
    std::cout << "rend-- :\n";
    rend--;
    std::cout << "rend = [" << rend->first << "] " << rend->second << std::endl;
    std::cout << std::endl;


    std::cout << std::endl << "-------------------------------" << std::endl;
    NAMESPACE::map<int, int>::reverse_iterator revIt(one.begin());
    NAMESPACE::map<int, int>::reverse_iterator revIt2(one.end());
    std::cout << "rit(map.begin()) = [" << revIt->first << "] " << revIt->second << std::endl << "rit(map.end()) = [" << revIt2->first << "] " << revIt2->second << std::endl;


    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||       complementary tests      ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    oneIt = one.begin();
    std::cout << "Deleting " << oneIt->first << std::endl;
    one.erase(oneIt);
    for (NAMESPACE::map<int, int>::iterator it = one.begin(); it != one.end(); it++)
        std::cout << "[" << it->first << "] ";
    std::cout << std::endl;
    
    oneIt = one.begin();
    std::cout << "Deleting " << oneIt->first << std::endl;
    one.erase(oneIt);
    for (NAMESPACE::map<int, int>::iterator it = one.begin(); it != one.end(); it++)
        std::cout << "[" << it->first << "] ";
    std::cout << std::endl;

    oneIt = one.begin();
    std::cout << "Deleting " << oneIt->first << std::endl;
    one.erase(oneIt);
    for (NAMESPACE::map<int, int>::iterator it = one.begin(); it != one.end(); it++)
        std::cout << "[" << it->first << "] ";
    std::cout << std::endl;

    oneIt = one.begin();
    std::cout << "Deleting " << oneIt->first << std::endl;
    one.erase(oneIt);
    for (NAMESPACE::map<int, int>::iterator it = one.begin(); it != one.end(); it++)
        std::cout << "[" << it->first << "] ";
    std::cout << std::endl;
    
    oneIt = one.begin();
    std::cout << "Deleting " << oneIt->first << std::endl;
    one.erase(oneIt);
    for (NAMESPACE::map<int, int>::iterator it = one.begin(); it != one.end(); it++)
        std::cout << "[" << it->first << "] ";
    std::cout << std::endl;

    one.clear();

    for (int i = 0; i < 10; i++)
    {
        std::cout << std::endl << "INSERTING " << i;
        one.insert(NAMESPACE::make_pair<int, int>(i, i));
        // one.print_map();
        std::cout << std::endl;
    }

    while (one.empty() == false)
    {
        oneIt = one.end();
        oneIt--;
        std::cout << "Deleting " << oneIt->first << std::endl;
        one.erase(oneIt);
        for (NAMESPACE::map<int, int>::iterator it = one.begin(); it != one.end(); it++)
            std::cout << "[" << it->first << "] ";
        std::cout << std::endl;
    }


    one.insert(NAMESPACE::make_pair<int, int>(1, 123));
    one.insert(NAMESPACE::make_pair<int, int>(2, 123));
    one.insert(NAMESPACE::make_pair<int, int>(3, 123));
    one.insert(NAMESPACE::make_pair<int, int>(10, 123));
    one.insert(NAMESPACE::make_pair<int, int>(9, 123));
    one.insert(NAMESPACE::make_pair<int, int>(4, 123));
    one.insert(NAMESPACE::make_pair<int, int>(5, 123));
    one.insert(NAMESPACE::make_pair<int, int>(6, 123));
    one.insert(NAMESPACE::make_pair<int, int>(7, 123));
    one.insert(NAMESPACE::make_pair<int, int>(8, 123));
    one.insert(NAMESPACE::make_pair<int, int>(15, 123));
    one.insert(NAMESPACE::make_pair<int, int>(11, 123));
    one.insert(NAMESPACE::make_pair<int, int>(12, 123));
    
    std::cout << "For map containing:\n";
    for (NAMESPACE::map<int, int>::iterator it = one.begin(); it != one.end(); it++)
        std::cout << "[" << it->first << "] ";
    std::cout << std::endl;

    while (one.empty() == false)
    {
        oneIt = one.end();
        oneIt--;
        std::cout << "Deleting " << oneIt->first << std::endl;
        one.erase(oneIt);
        for (NAMESPACE::map<int, int>::iterator it = one.begin(); it != one.end(); it++)
            std::cout << "[" << it->first << "] ";
        std::cout << std::endl;
    }

    std::cout << std::endl << "====================================" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "||      relationnal operators     ||" << std::endl;
    std::cout << "||                                ||" << std::endl;
    std::cout << "====================================" << std::endl << std::endl;
    NAMESPACE::map<int, int> m2;
    NAMESPACE::map<int, int> m1(m2);
    std::cout << "---" << std::endl;
    std::cout << (m1 == m2) << std::endl;
    std::cout << (m1 != m2) << std::endl;

    m2.insert(NAMESPACE::make_pair<int, int>(3, 1));
    m1.insert(NAMESPACE::make_pair<int, int>(2, 1));
    std::cout << (m1 < m2) << std::endl;
    std::cout << (m1 >= m2) << std::endl;
    std::cout << (m1 <= m2) << std::endl;
    std::cout << (m1 > m2) << std::endl;
    return (0);
}

int main()
{
    test_map_a();
    test_map_m();

    return (0);
}