/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.hpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/30 09:25:17 by alouis            #+#    #+#             */
/*   Updated: 2021/08/23 14:01:58 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAP_HPP
# define MAP_HPP

# include "../utils/compare.hpp"
# include "../utils/pair.hpp"
# include "../utils/rb_tree.hpp"
# include "../utils/utils.hpp"

namespace ft
{
    template < class Key,
        class T,
        class Compare = ft::less<Key>,
        class Alloc = std::allocator<ft::pair<const Key,T> >
        > 
    class map
    {            
        public:
            typedef             Key                                     key_type;
            typedef             T                                       mapped_type;
            typedef             ft::pair<const Key, T>                  value_type;
            typedef             Compare                                 key_compare;
            typedef             Alloc                                   allocator_type;
            typedef typename    allocator_type::reference               reference;
            typedef typename    allocator_type::const_reference         const_reference;
            typedef typename    allocator_type::pointer                 pointer;
            typedef typename    allocator_type::const_pointer           const_pointer;

        private:
            typedef ft::rb_tree<key_type, value_type, ft::select1st<value_type>, key_compare, allocator_type>   rb_tree_type;
            
        public:
            typedef typename    rb_tree_type::iterator                      iterator;
            typedef typename    rb_tree_type::const_iterator                const_iterator;
            typedef typename    rb_tree_type::reverse_iterator              reverse_iterator;
            typedef typename    rb_tree_type::const_reverse_iterator        const_reverse_iterator;
            typedef typename    allocator_type::size_type                   size_type;
            typedef typename    ft::Iterator_traits< ft::baseIterator<ft::bidirectional_iterator_tag, T> >::difference_type    difference_type;
            
            class value_compare : public std::binary_function<value_type, value_type, bool> // in C++98, it is required to inherit binary_function<value_type,value_type,bool>
            {   
                friend class map;
                protected:
                    Compare comp;
                    value_compare (Compare c) : comp(c) {}
                public:
                    typedef bool        result_type;
                    typedef value_type  first_argument_type;
                    typedef value_type  second_argument_type;
                bool operator() (const value_type& x, const value_type& y) const
                {
                    return comp(x.first, y.first);
                }
            };

            explicit map (const key_compare& comp = key_compare(), const allocator_type& alloc = allocator_type()) :  _alloc(alloc), _comp(comp), _value_comp(_comp) {
            };

            template <class InputIterator>
            map (InputIterator first, InputIterator last,
                const key_compare& comp = key_compare(),
                const allocator_type& alloc = allocator_type()) : _alloc(alloc), _comp(comp), _value_comp(_comp) {
                    while (first != last)
                    {
                        insert(*first);
                        first++;
                    }
            };

            map (const map& x) : _alloc(x.get_allocator()), _comp(x._comp), _value_comp(_comp) {
                _tree = x._tree;
            };

            ~map() {};

            map& operator= (const map& x) {
                _tree = x._tree;
                return (*this);
            };

            iterator begin() {
                return (_tree.begin()); 
            };

            const_iterator begin() const {
                return (_tree.begin());
            };

            iterator end() {
                return (_tree.end());
            };

            const_iterator end() const {
                return (_tree.end());
            };

            reverse_iterator rbegin() {
                return (_tree.rbegin());
            };

            const_reverse_iterator rbegin() const {
                return (_tree.rbegin());
            };

            reverse_iterator rend() {
                return (_tree.rend());
            };

            const_reverse_iterator rend() const {
                return (_tree.rend());
            };

            bool empty() const {
                return (_tree.empty());
            };
            
            size_type size() const {
                return (_tree.size());
            };
            
            size_type max_size() const {
                return (_tree.max_size());
            };
            
            mapped_type& operator[] (const key_type& k) {
                return ((*((this->insert(ft::make_pair(k,mapped_type()))).first)).second);
            };
            
            pair<iterator,bool> insert (const value_type& val) {
                return (_tree.insert_unique(val));
            };
            
            iterator insert (iterator position, const value_type& val) {
                (void)position;
                return ((_tree.insert_unique(val)).first);
            };
            
            template <class InputIterator>
            void insert (InputIterator first, InputIterator last) {
                _tree.insert_range(first, last);
            };
            
            void erase (iterator position) {
                _tree.erase(position);
            };

            size_type erase (const key_type& k) {
                iterator toBeDeleted;

                toBeDeleted = find(k);
                if (toBeDeleted == end())
                    return (0);
                erase(toBeDeleted);
                return (1);
            };

            void erase (iterator first, iterator last) {
                _tree.erase(first, last);
            };

            void swap (map& x) {
                map tmp;

                tmp = *this;
                *this = x;
                x = tmp;
            };
            
            void clear() {
                _tree.clear();
            };

            key_compare key_comp() const {
                return (_comp);
            };
            
            value_compare value_comp() const {
                return (_value_comp);
            };
            
            iterator find (const key_type& k) {
                return (_tree.find(k));
            };

            const_iterator find (const key_type& k) const {
                return (_tree.find(k));
            };
            
            size_type count (const key_type& k) const { // map only contains unique keys
                if (find(k) == end())
                    return (0);
                return (1);
            };
            
            iterator lower_bound (const key_type& k) {
                return (_tree.lower_bound(k));
            };

            const_iterator lower_bound (const key_type& k) const {
                return (_tree.lower_bound(k));
            };

            iterator upper_bound (const key_type& k) {
                return (_tree.upper_bound(k));
            };

            const_iterator upper_bound (const key_type& k) const {
                return (_tree.upper_bound(k));
            };
            
            pair<const_iterator,const_iterator> equal_range (const key_type& k) const {
                const_iterator  lower;
                const_iterator  upper;

                lower = lower_bound(k);
                upper = upper_bound(k);
                return (ft::make_pair<const_iterator, const_iterator>(lower, upper));
            };
            
            pair<iterator,iterator>             equal_range (const key_type& k) {
                iterator  lower;
                iterator  upper;

                lower = lower_bound(k);
                upper = upper_bound(k);
                return (ft::make_pair<iterator, iterator>(lower, upper));
            };

            allocator_type get_allocator() const { return (_alloc); };
        
            void    print_map() {
                return (_tree.print_tree());
            };
            
        private:
            allocator_type  _alloc;
            key_compare     _comp;
            value_compare   _value_comp;
            
            rb_tree_type    _tree;
    };

    template< class Key, class T, class Compare, class Alloc >
    bool operator==( const map<Key,T,Compare,Alloc>& lhs,
                 const map<Key,T,Compare,Alloc>& rhs ) {
        if (lhs.size() != rhs.size())
            return (false);
        return (ft::equal(lhs.begin(), lhs.end(), rhs.begin()));
    };

    template< class Key, class T, class Compare, class Alloc >
    bool operator!=( const map<Key,T,Compare,Alloc>& lhs,
                 const map<Key,T,Compare,Alloc>& rhs ) {
        return !(lhs == rhs);
    };

    template< class Key, class T, class Compare, class Alloc >
    bool operator<( const map<Key,T,Compare,Alloc>& lhs,
                const map<Key,T,Compare,Alloc>& rhs ) {
        return (ft::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end()));
    };

    template< class Key, class T, class Compare, class Alloc >
    bool operator<=( const map<Key,T,Compare,Alloc>& lhs,
                 const map<Key,T,Compare,Alloc>& rhs ) {
        return !(lhs > rhs);
    };

    template< class Key, class T, class Compare, class Alloc >
    bool operator>( const map<Key,T,Compare,Alloc>& lhs,
                const map<Key,T,Compare,Alloc>& rhs ) {
        return (rhs < lhs);
    };

    template< class Key, class T, class Compare, class Alloc >
    bool operator>=( const map<Key,T,Compare,Alloc>& lhs,
                 const map<Key,T,Compare,Alloc>& rhs ) {
        return !(lhs < rhs);
    };
};

#endif