/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/18 15:42:50 by alouis            #+#    #+#             */
/*   Updated: 2021/08/23 13:24:06 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <set>
#include "set.hpp"

int main()
{
    NAMESPACE::set<int> mySet;
    
    for (int i = 1; i < 10; i++)
        mySet.insert(i);
    
    NAMESPACE::set<int> copySet(mySet);
    NAMESPACE::set<int> rangeSet(mySet.begin(), mySet.end());
    NAMESPACE::set<int>::iterator   myIt;
    NAMESPACE::set<int>::iterator   myIt2;
    for (myIt = mySet.begin(); myIt != mySet.end(); myIt++)
        std::cout << *myIt << " ";
    std::cout << std::endl;

    if (mySet.empty() == false)
        std::cout << "mySet is not empty\n";
    else
        std::cout << "mySet is empty\n";
    std::cout << std::endl;
        
    std::cout << "mySet size is: " << mySet.size() << std::endl;

    std::cout << "mySet max_size is: " << mySet.max_size() << std::endl;

    myIt = mySet.begin();
    for (int i = 0; i < 5; i++)
        myIt++;
    myIt2 = mySet.insert(myIt, 33);
    std::cout << "insert(middle, 33): " << *myIt2 << std::endl;
    for (myIt = mySet.begin(); myIt != mySet.end(); myIt++)
        std::cout << *myIt << " ";
    std::cout << std::endl;
    std::cout << std::endl;

    myIt = mySet.end();
    myIt2 = mySet.insert(myIt, 0);
    std::cout << "insert(end, 0): " << *myIt2 << std::endl;
    for (myIt = mySet.begin(); myIt != mySet.end(); myIt++)
        std::cout << *myIt << " ";
    std::cout << std::endl;
    std::cout << std::endl;
    

    NAMESPACE::set<int> mySet2;
    for (int i = 0; i < 6; i++)
        myIt2++;
    mySet2.insert(myIt2, mySet.end());
    std::cout << "insert range [" << *myIt2 << ", end): \n";
    for (myIt = mySet2.begin(); myIt != mySet2.end(); myIt++)
        std::cout << *myIt << " ";
    std::cout << std::endl;
    std::cout << std::endl;
    
    mySet2.insert(mySet.begin(), myIt2);
    std::cout << "insert range [begin, " << *myIt2 << "): \n";
    for (myIt = mySet2.begin(); myIt != mySet2.end(); myIt++)
        std::cout << *myIt << " ";
    std::cout << std::endl;
    std::cout << std::endl;

    mySet2.clear();
    std::cout << "Clear: \n";
    if (mySet2.empty() == true)
        std::cout << "set.clear() worked\n";
    else
        std::cout << " /!\\ set.clear() didn't work /!\\\n";
    mySet2.insert(mySet.begin(), mySet.end());
    std::cout << "insert range [begin, end): \n";
    for (myIt = mySet2.begin(); myIt != mySet2.end(); myIt++)
        std::cout << *myIt << " ";
    std::cout << std::endl;
    std::cout << std::endl;

    myIt = mySet.begin();
    for (int i = 0; i < 7; i++)
        myIt++;
    mySet.erase(myIt);
    std::cout << "erase(" << *myIt << "):\n";
    for (myIt = mySet.begin(); myIt != mySet.end(); myIt++)
        std::cout << *myIt << " ";
    std::cout << std::endl;
    std::cout << std::endl;

    mySet2 = mySet;
    std::cout << "For set containing:\n";
    for (myIt = mySet.begin(); myIt != mySet.end(); myIt++)
        std::cout << *myIt << " ";
    std::cout << std::endl;
    std::cout << "erase odd values:\n";
    mySet.erase(1);
    mySet.erase(3);
    mySet.erase(5);
    mySet.erase(9);
    for (myIt = mySet.begin(); myIt != mySet.end(); myIt++)
        std::cout << *myIt << " ";
    std::cout << std::endl;

    myIt = mySet2.begin();
    for (int i = 0; i < 4; i++)
        myIt++;
    std::cout << "OR erase range [begin, " << *myIt << "):\n";
    mySet2.erase(mySet2.begin(), myIt);
    for (myIt = mySet2.begin(); myIt != mySet2.end(); myIt++)
        std::cout << *myIt << " ";
    std::cout << std::endl;

    myIt = mySet2.end();
    for (int i = 0; i < 5; i++)
        myIt--;
    std::cout << "erase range [" << *myIt << ", end):\n";
    mySet2.erase(myIt, mySet2.end());
    for (myIt = mySet2.begin(); myIt != mySet2.end(); myIt++)
        std::cout << *myIt << " ";
    std::cout << std::endl;
    std::cout << std::endl;

    for (int i = 5; i < 10; i++)
        mySet2.insert(i);
    std::cout << "swapping:\n";
    std::cout << "mySet: ";
    for (myIt = mySet.begin(); myIt != mySet.end(); myIt++)
        std::cout << *myIt << " ";
    std::cout << std::endl;
    std::cout << "AND\nmySet2: ";
    for (myIt = mySet2.begin(); myIt != mySet2.end(); myIt++)
        std::cout << *myIt << " ";
    std::cout << std::endl;
    
    mySet.swap(mySet2);
    std::cout << ">>>\n";
    std::cout << "mySet: ";
    for (myIt = mySet.begin(); myIt != mySet.end(); myIt++)
        std::cout << *myIt << " ";
    std::cout << std::endl;
    std::cout << "AND\nmySet2: ";
    for (myIt = mySet2.begin(); myIt != mySet2.end(); myIt++)
        std::cout << *myIt << " ";
    std::cout << std::endl;
    std::cout << std::endl;

    std::cout << "lower_bound(5):\n";
    myIt = mySet.lower_bound(5);
    std::cout << "mySet -> " << *myIt << " | mySet2 -> ";
    myIt = mySet2.lower_bound(5);
    std::cout << *myIt << std::endl;

    std::cout << "lower_bound(33):\n";
    myIt = mySet.lower_bound(33);
    std::cout << "mySet -> " << *myIt << " | mySet2 -> ";
    myIt = mySet2.lower_bound(33);
    std::cout << *myIt << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;

    std::cout << "upper_bound(6): \n";
    myIt = mySet.upper_bound(6);
    std::cout << "mySet -> " << *myIt << " | mySet2 -> ";
    myIt = mySet2.upper_bound(6);
    std::cout << *myIt << std::endl;
    std::cout << std::endl;
 
    NAMESPACE::pair<NAMESPACE::set<int>::const_iterator, NAMESPACE::set<int>::const_iterator>   range;
    std::cout << "equal_range(8): \n";
    range = mySet.equal_range(8);
    std::cout << "mySet:\n ";
    std::cout << *range.first << " - " << *range.second << '\n';
    range = mySet2.equal_range(8);
    std::cout << "mySet2:\n ";
    std::cout << *range.first << " - " << *range.second << '\n';
    std::cout << std::endl;

    std::cout << "relationnal operators\n";
    NAMESPACE::set<int> m1;
    NAMESPACE::set<int> m2(m1);
    std::cout << "---" << std::endl;
    std::cout << (m1 == m2) << std::endl;
    std::cout << (m1 != m2) << std::endl;

    for (int i = 1; i < 5; i++)
    {
        m1.insert(i);
        m2.insert(i);
    }
    m1.insert(5);
    m2.insert(6);
    std::cout << (m1 == m2) << std::endl;
    std::cout << (m1 != m2) << std::endl;
    std::cout << (m1 < m2) << std::endl;
    std::cout << (m1 >= m2) << std::endl;
    std::cout << (m1 <= m2) << std::endl;
    std::cout << (m1 > m2) << std::endl;
    
    return (0);
}