/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set.hpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/18 15:23:24 by alouis            #+#    #+#             */
/*   Updated: 2021/08/23 13:29:46 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SET_HPP
# define SET_HPP

# include <memory>
# include "../utils/compare.hpp"
# include "../utils/rb_tree.hpp"
# include "../utils/pair.hpp"

namespace ft 
{
    template < class T,
        class Compare = ft::less<T>,
        class Alloc = std::allocator<T> 
        > 
    class set {
        public:
            typedef             T                               key_type;
            typedef             T                               value_type;
            typedef             Compare                         key_compare;
            typedef             Compare                         value_compare;
            typedef             Alloc                           allocator_type;
            typedef typename    allocator_type::reference	    reference; 
            typedef typename    allocator_type::const_reference	const_reference;
            typedef typename    allocator_type::pointer	        pointer;
            typedef typename    allocator_type::const_pointer   const_pointer;
            typedef typename    allocator_type::size_type       size_type;
            typedef typename    ft::Iterator_traits< ft::baseIterator<ft::bidirectional_iterator_tag, T> >::difference_type    difference_type;
            
        private:
            typedef ft::rb_tree<key_type, value_type, key_type, key_compare, allocator_type>   rb_tree_type;
            
        public:
            typedef typename    rb_tree_type::iterator                      iterator;
            typedef typename    rb_tree_type::const_iterator                const_iterator;
            typedef typename    rb_tree_type::reverse_iterator              reverse_iterator;
            typedef typename    rb_tree_type::const_reverse_iterator        const_reverse_iterator;

        explicit set (const key_compare& comp = key_compare(), const allocator_type& alloc = allocator_type()) : _alloc(alloc), _comp(comp) {};

        template <class InputIterator>
        set (InputIterator first, InputIterator last, const key_compare& comp = key_compare(), const allocator_type& alloc = allocator_type()) : _alloc(alloc), _comp(comp) {
            insert(first, last);
        };

        set ( set& x) : _alloc(x._alloc), _comp(x._comp) {
            insert(x.begin(), x.end());
        };

        ~set() {};

        set& operator= (set& x) {
            clear();
            insert(x.begin(), x.end());
            return (*this);
        };

        iterator begin() {
            return (_tree.begin());
        };

        const_iterator begin() const {
            return (_tree.begin());
        };

        iterator end() {
            return (_tree.end());
        };

        const_iterator end() const {
            return (_tree.end());
        };

        reverse_iterator rbegin() {
            return (_tree.rbegin());
        };

        const_reverse_iterator rbegin() const {
            return (_tree.rbegin());
        };

        reverse_iterator rend() {
            return (_tree.rend());
        };

        const_reverse_iterator rend() const {
            return (_tree.rend());
        };

        bool empty() const {
            return (_tree.empty());
        };

        size_type size() const {
            return (_tree.size());
        };

        size_type max_size() const {
            return (_tree.max_size());
        };

        pair<iterator,bool> insert (const value_type& val) {
            return (_tree.insert_unique_set(val));
        };

        iterator insert (iterator position, const value_type& val) {
            (void)position;
            return ((_tree.insert_unique_set(val)).first);
        };

        template <class InputIterator>
        void insert (InputIterator first, InputIterator last) {
            _tree.insert_range_set(first, last);
        };

        void erase (iterator position) {
            _tree.erase(position);
        };

        size_type erase (const key_type& k) {
            iterator toBeDeleted;

            toBeDeleted = find(k);
            if (toBeDeleted == end())
                return (0);
            erase(toBeDeleted);
            return (1);
        };

        void erase (iterator first, iterator last) {
            if (first == begin() && last == end())
                clear();
            else
            {
                iterator tmp = first;
                
                while (first != last)
                {
                    first++;
                    erase(tmp);
                    tmp = first;
                }
            }
        };

        void swap (set& x) {
            set tmp;

            tmp = x;
            x = *this;
            *this = tmp;
        };

        void clear() {
            _tree.clear();
        };

        key_compare key_comp() const {
            return (_comp);
        };

        value_compare value_comp() const {
            return (value_compare());
        };

        iterator find (const value_type& val) {
            return (_tree.find_set(val));
        };

        size_type count (const value_type& val) const {
            (void)val;
            return (1);
        };

        iterator lower_bound (const value_type& val) {
            return (_tree.lower_bound_set(val));
        };

        iterator upper_bound (const value_type& val) {
            return (_tree.upper_bound_set(val));
        };

        pair<iterator,iterator> equal_range (const value_type& val) {
            iterator  lower;
            iterator  upper;

            lower = lower_bound(val);
            upper = upper_bound(val);
            return (ft::make_pair<iterator, iterator>(lower, upper));
        };

        allocator_type get_allocator() const {
            return (_alloc);
        };

        private:
            allocator_type  _alloc;
            key_compare     _comp;

            rb_tree_type    _tree;
    };

    template< class Key, class Compare, class Alloc >
    bool operator==( const set<Key,Compare,Alloc>& lhs,
                 const set<Key,Compare,Alloc>& rhs ) {
        if (lhs.size() != rhs.size())
            return false;
        return (ft::equal(lhs.begin(), lhs.end(), rhs.begin()));
    };

    template< class Key, class Compare, class Alloc >
    bool operator!=( const set<Key,Compare,Alloc>& lhs,
                 const set<Key,Compare,Alloc>& rhs ) {
        return !(lhs == rhs);
    };

    template< class Key, class Compare, class Alloc >
    bool operator<( const set<Key,Compare,Alloc>& lhs,
                 const set<Key,Compare,Alloc>& rhs ) {
        return (ft::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end()));
    };

    template< class Key, class Compare, class Alloc >
    bool operator<=( const set<Key,Compare,Alloc>& lhs,
                 const set<Key,Compare,Alloc>& rhs ) {
        return !(rhs < lhs);
    };

    template< class Key, class Compare, class Alloc >
    bool operator>( const set<Key,Compare,Alloc>& lhs,
                const set<Key,Compare,Alloc>& rhs ) {
        return (rhs < lhs);
    };

    template< class Key, class Compare, class Alloc >
    bool operator>=( const set<Key,Compare,Alloc>& lhs,
                 const set<Key,Compare,Alloc>& rhs ) {
        return !(lhs < rhs);
    };

    template< class Key, class Compare, class Alloc >
    void swap( set<Key,Compare,Alloc>& lhs, 
           set<Key,Compare,Alloc>& rhs ) {
        return (lhs.swap(rhs));
    };
};

#endif