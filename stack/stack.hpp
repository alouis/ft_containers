/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/28 10:23:03 by alouis            #+#    #+#             */
/*   Updated: 2021/07/29 18:52:14 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STACK_HPP
# define STACK_HPP

# include "../vector/vector.hpp"

namespace ft 
{
    template <class T, class Container = ft::vector<T> > 
    class stack
    {
        public:
            typedef             T                               value_type;
            typedef             Container                       container_type;
            typedef typename    container_type::size_type       size_type;
            typedef typename    container_type::reference       reference;
            typedef typename    container_type::const_reference const_reference;

            explicit stack (const container_type& ctnr = container_type()) : _ctnr(ctnr) {};
            stack( const stack& other ) : _ctnr(other._ctnr) {};

            stack& operator=( const stack& other ) {
                _ctnr = other._ctnr;
                return (*this);
            };

            virtual ~stack() {};

            bool empty() const { return (_ctnr.empty()); };

            size_type size() const { return (_ctnr.size()); };

            value_type& top() { return (_ctnr.back()); };
            
            const value_type& top() const { return (_ctnr.back()); };

            void push (const value_type& val) { _ctnr.push_back(val); };

            void pop() { _ctnr.pop_back(); };
            
            friend
            bool operator== (const stack<T,Container>& lhs, const stack<T,Container>& rhs) {
                return (lhs._ctnr == rhs._ctnr);
            };

            friend
            bool operator!= (const stack<T,Container>& lhs, const stack<T,Container>& rhs) {
                return (lhs._ctnr != rhs._ctnr);
            };

            friend
            bool operator<= (const stack<T,Container>& lhs, const stack<T,Container>& rhs) {
                return (lhs._ctnr <= rhs._ctnr);
            };

            friend
            bool operator< (const stack<T,Container>& lhs, const stack<T,Container>& rhs) {
                return (lhs._ctnr < rhs._ctnr);
            };

            friend
            bool operator>= (const stack<T,Container>& lhs, const stack<T,Container>& rhs) {
                return (lhs._ctnr >= rhs._ctnr);
            };

            friend
            bool operator> (const stack<T,Container>& lhs, const stack<T,Container>& rhs) {
                return (lhs._ctnr > rhs._ctnr); 
            };
            
        private:
            container_type  _ctnr;
    };
};

#endif